.. meta::

   :description lang=de: XML-Kurs mit den Themen: DTD, Schema, XSL, XPath
   :keywords: XSL, for-each, OER

:ref:`« XSL-Start <xsl-start>`

.. index:: XSL; for-each

=======================
Wiederholung (for-each)
=======================

.. image:: ../images/schild-beschwerde.jpg
   :width: 0px


.. |a| raw:: html

   <!-- lightbox container hidden with CSS -->
   <a href="#" class="lightbox" id="img1">
   <span style="background-image: url('../../_images/schild-beschwerde.jpg')"></span>
   </a>

.. |b| raw:: html

   <a href="#img1">
   <img width="250px" src="../../_images/schild-beschwerde.jpg">
   </a>

.. sidebar:: Schilder

   |b|

|a|

Lernziel
========

Mit Kontrollstrukturen kann die Ausgabe und Verarbeitung gesteuert
werden. Hier ein Beispiel für eine Wiederholung durch for-each. Wenn
ein Teil des Baumes immer wieder die gleiche Struktur aufweist, können
auch die Anweisungen statt mit einer Template-Rule in einer Schleife
abgearbeitet werden.

Handlungsanweisungen
====================
:Aufgaben:

1. Geben Sie alle Titel Ihrer CD-Datenbank untereinander aus.
2. Geben Sie zusammen mit den Titeln auch die Interpreten in einer Tabelle aus.
3. Fügen Sie der Tabelle nun noch eine Spalte *Bandinformation* hinzu.
   Stellen Sie den entsprechenden Inhalt dar.
4. Geben Sie alle Angabe für die folgende XML-Struktur in einer for-Schleife aus.

   .. code-block:: xml
      :linenos:

      <adressensammlung>
        <!--erster Datensatz-->
        <name>
          <anrede>Herr</anrede>
          <nachname>Zuse</nachname>
          <vorname>Konrad</vorname>
        </name>
        <!-- zweiter Datensatz-->
        <name>
         <anrede>Herr</anrede>
         <nachname>Leibnitz</nachname>
         <vorname>Gottfried Wilhelm </vorname>
        </name>
      </adressensammlung>

Ausgangspunkt für die Transformation
====================================
Mit einer for-Schleife sollen Vor- und Nachname aus der folgenden XML extrahiert werden.

.. code-block:: xml
   :linenos:

   <?xml version="1.0" encoding="UTF-8"?>
   <adresse>
     <person>
       <anrede>Herr</anrede>
       <name>
         <nachname>Becker</nachname>
         <vorname>Heinz</vorname>
       </name>
       <strasse>Schabenstr. 4</strasse>
       <postanschrift>
         <plz>14489</plz>
         <wohnort>Berlin</wohnort>
       </postanschrift>
       <email>becker@heinz.de</email>
     </person>
     <person>
       <anrede>Frau</anrede>
       <name>
         <nachname>Rennt</nachname>
         <vorname>Lola</vorname>
       </name>
       <strasse>Laufsteig 17</strasse>
       <postanschrift>
         <plz>14888</plz>
         <wohnort>Potsdam</wohnort>
       </postanschrift>
       <email>lolo@lola.de</email>
     </person>
   </adresse>


Mehrere Datensätze – XSL
~~~~~~~~~~~~~~~~~~~~~~~~

.. code-block:: xml
   :linenos:

   <xsl:stylesheet version="1.0"
        xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
   <xsl:output method="html"/>

   <xsl:template match="/">

   <h2>Meine Adressen</h2>
   <xsl:for-each select="adressensammlung/adresse/name">
     <p>
     <xsl:value-of select="nachname"/>
     <br/>
     <xsl:value-of select="vorname"/>
     </p>
   </xsl:for-each>
   </xsl:template>
   </xsl:stylesheet>

Mit dieser Schleifenkonstruktion (gleiches Muster wie in anderen Programmiersprachen) 
wird mit dem select-Attribut, nach einem Verschachtelungsmuster gesucht. Wenn das Muster 
passt, wird eine Tabellenzeile in HTML-Syntax erzeugt. Die Pfadangabe folgt den Regeln
im Dateisystem, nur das statt der Ordnernamen die  Elementnamen der
XML-Datei verwendet werden. Es handelt sich um XPath-Ausdrücke
(Zeilen: 8, 10, 12, 14).

Eine Tabelle generieren
~~~~~~~~~~~~~~~~~~~~~~~

.. code-block:: xml
   :linenos:

   <xsl:stylesheet version="1.0"
        xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
   <xsl:output method="html"/>

   <xsl:template match="/">

   <h2>Meine Adressen</h2>

   <table border="1">
     <tr bgcolor="#9acd32">
       <th align="left">Name</th>
       <th align="left">Vorname</th>
      </tr>
      <xsl:for-each select="adressensammlung/adresse/name">
        <tr>
           <td>
              <xsl:value-of select="nachname"/>
           </td>
           <td>
              <xsl:value-of select="vorname"/>
           </td>
     </tr>
      </xsl:for-each>
   </table>
   </xsl:template>
   </xsl:stylesheet>

Angereichert mit HTML-Konstrukten, sollte die Anzeige im Browser, wie weiter oben 
gezeigt und geplant aussehen (Zeilen: 7, 9-13, 15, 16, 18, 19, 21, 22, 24).

.. image:: img/ergebnis-template-rules.png
  :alt: Ergebnis der Transformation
