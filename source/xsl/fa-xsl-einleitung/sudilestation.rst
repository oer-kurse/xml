.. meta::

   :description lang=de: XML-Kurs mit den Themen: DTD, Schema, XSL, XPath
   :keywords: XSL, Einleitung, OER

:ref:`« XSL-Start <xsl-start>`


.. index:: XSL; Einleitung


============
Was ist XSL?
============

.. image:: ../images/tannentrieb2.jpg
   :width: 0px


.. |a| raw:: html

   <!-- lightbox container hidden with CSS -->
   <a href="#" class="lightbox" id="img1">
   <span style="background-image: url('../../_images/tannentrieb2.jpg')"></span>
   </a>

.. |b| raw:: html

   <a href="#img1">
   <img width="250px" src="../../_images/tannentrieb2.jpg">
   </a>

.. sidebar:: Spitze eines Tannentriebs

   |b|


|a|



Lernziel
========

Sie lernen kurz die Bedeutung von XSL kennen.

Handlungsanweisungen
====================

:Aufgaben:

  1. Was ist XSL, finden sie eine Definition im Internet?

Was ist XSL?
~~~~~~~~~~~~

XSL bedeutet *Extensible Stylesheet Language*. Reine XML-Dokumente sind für die
Darstellung nicht geeignet, sie sind eher mit einer Datenbank zu vergleichen.
Für die Darstellung, Extraktion und Konvertierung, benötigen Sie XSL.
XSL erzeugt aus dem XML-Dokument (Eingabe) ein neues Dokument (Ausgabe) in
einem beliebig anderen Format. Ausgaben wie z.B. Text, HTML, Latex als
PDF-Vorstufe, JSON oder eine neue XML-Struktur sind möglich.


Diesen Prozess des Erzeugens und Umwandelns nennt man "Transformation".
Wir haben also eine strikte Trennung von Inhalt und Darstellung.
Damit Sie mit XSL transformieren können, brauchen Sie einen sogenannten XSL-Prozessor,
der Ihre XSL-Anweisungen versteht. Im den meisten Web-Browsern ist ein solcher
Prozessor schon eingebaut.

Um die einzelnen Elemente und Attribute und deren Inhalt in einem XML-Dokument
gezielt finden, extrahieren und verarbeiten zu können wird bei der Transformation 
mit einem weiteren Standard gearbeitet, dem XPATH. Mit XPATH wird der
XML Baum durchsucht, um einzelne Elemente, Knoten oder Attribute zu
finden. Was mit dem Konten geschieht, wenn der durch XPATH gefunden
wurde, wird über XSLT-Anweisungen gesteuert. Damit handelt es sich bei
XPATH und XSLT um ein untrennbares Duo.

Ein Beispiel:
~~~~~~~~~~~~~
Ein XML-Element kann eine HTML-Tabelle sein oder auch ein Tisch für
einen Tischler oder etwas  anderes bedeuten:
::

  <table />

Was es nun wirklich bedeutet, weiß nur der Autor, also Sie, wenn Sie
das XML-Dokument verfasst haben. Ein Browser hat keine Informationen,
wie ein XML-Dokument anzuzeigen ist!

Diese Informationen übergeben Sie dem Browser nach der Abarbeitung mit
den XSLT-Anweisungen. Ob alle table-Elemente oder nur ein Element
verarbeitet werden soll, wird mit einem XPATH-Ausdruck definiert.
