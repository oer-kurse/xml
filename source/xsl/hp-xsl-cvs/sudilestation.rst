.. meta::

   :description lang=de: XML-Kurs mit den Themen: DTD, Schema, XSL, XPath
   :keywords: XSL, Template Rules, CSV, OER

:ref:`« XSL-Start <xsl-start>`

.. index:: CSV-Datei generieren, CSV

===============
Ausgabe als CSV
===============

.. image:: ../images/pfefferlinge.jpg
   :width: 0px


.. |a| raw:: html

   <!-- lightbox container hidden with CSS -->
   <a href="#" class="lightbox" id="img1">
   <span style="background-image: url('../../_images/pfefferlinge.jpg')"></span>
   </a>

.. |b| raw:: html

   <a href="#img1">
   <img width="250px" src="../../_images/pfefferlinge.jpg">
   </a>

.. sidebar:: Pfefferlinge

   |b|

|a|


Lernziel
========

Bisher haben wir HTML als Zielformat gewählt. Hier nun ein Beispiel
wie sie die Daten einer XML-Datei als reinen Text exportieren, um zum
Beispiel eine CSV-Datei zu erzeugen, die danach in eine
Tabellenkalkulation importiert werden kann.

Handlungsanweisungen
====================

:Aufgaben:

Entwickeln Sie bitte eine XSL-Datei, die eine CSV-Datei generieren soll.
Benutzen Sie bitte saxon, so dass Sie die CSV-Datei ohne den im Browser
integriertem Transformators erstellen. Laden Sie dann die CSV-Datei in
Excel ein.

Weitere Hinweise finden Sie unten in dieser Station. Benutzen Sie
folgende XML-Datei.

.. code-block:: xml

    <telefon>
      <eintrag>
        <nummer>0331-2345322</nummer>
        <name>Heinz Becker</name>
        <info>Chef</info>
      </eintrag>
      <eintrag>
        <nummer>0331-2345324</nummer>
        <name>Steffi Acht</name>
        <info>Admin</info>
      </eintrag>
    </telefon>


CSV-Dateien erstellen
~~~~~~~~~~~~~~~~~~~~~

Um CSV-Dateien zu erstellen, müssen Sie folgenden Hinweise berücksichtigen:

**Output-Methode setzen**
::

  <xsl:output method="text" />

Wir müssen die Output-Methode von XSL-Dateien auf *text* setzen.
Die Zeile müssen Sie vor das erste Template setzen.

**Whitespaces (Zeilenumbrüche, Tabstopps usw.) in der XML ignorieren**
::

  <xsl:strip-space elements="*" />

Vorhandene Zeilenumbrüche, Tabstopps usw., die in der XML-Datei zwischen den
Elementen als Strukturierung dienen, sollen nicht in die CSV-Datei übernommen werden.
Diese würden ja die Struktur der CSV-Datei zerstören. Um das zu umgehen, können
Sie in der XSL-Datei vor dem ersten Template angeben, dass diese sog. Whitespaces 
ignoriert werden sollen.

Der Stern bedeutet, dass alle Whitespaces in der XML ignoriert werden.
Wenn Sie nur Whitespaces innerhalb eines Elementes ignorieren wollen, so können
Sie es direkt angeben.

**Zeilenumbruch und Datentrenner (Semikolon, Doppelkreuz, Tabstopps)
in der XSL generieren**

Um die Daten in der CSV voneinander zu trennen, muss zwischen den
Daten ein Datentrenner ausgegeben werden, z.B. ein Semikolon, ein
Tappstopp oder ein Doppelkreuz.

.. index: Tabstopp, Tabulator

Tabulator:
::

   <xsl:text>&#x9;</xsl:text>

.. index: Doppelkreuz, Raute

Doppelkreuz:
::

   <xsl:text>#</xsl:text>

.. index: Leerzeile

Einzelne Datensätze werden durch einen Zeilenumbruch voneinander getrennt:
::

   <xsl:text>&#xA;</xsl:text>

   oder

   <xsl:text>
   </xsl:text>

Die XSL-Datei, mit der Sie beginnen können, die Aufgabe oben zu lösen,
könnte so aussehen:


.. code-block:: xml
   :linenos:

   <?xml version="1.0" encoding="UTF-8"?>
   <xsl:stylesheet version="1.0"
     xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
     <!--Text ausgeben-->
     <xsl:output method="text"/>
     <xsl:strip-space elements="*"/>
     <!--CSV-Datei  generieren-->
     <xsl:template match="telefon">
       <!--HIER GEHT ES LOS-->
     </xsl:template>
   </xsl:stylesheet>

Bedenken Sie, dass Zeilenumbrüche in der XSL-Datei auch Zeilenumbrüche
in der CSV-Datei werden, d.h. Wenn Sie Sie das umgehen wollen, müssen
Sie die XSL-Elemente im entsprechenden Template hintereinander
schreiben (auch wenn es nicht besonders lesbar aussieht).

Letze Zeile der XML-Datei erreicht
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Wann der letzte Eintrag in der XML-Datei erreicht wurde:
::

  <xsl:if test="position()=last()"><!--MACHE ETWAS--></xsl:if>

**Die Lösung, fall sie nicht zum Ziel gelangen:**

.. code-block:: xml
   :linenos:

   <?xml version="1.0" encoding="UTF-8"?>
   <xsl:stylesheet version="1.0"
        xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
     <!--Text ausgeben-->
     <xsl:output method="text"/>
     <xsl:strip-space elements="*"/>
     <!--CSV-Datei mit Saxon generieren-->
     <xsl:template match="telefon">
       <xsl:apply-templates select="eintrag"/>
     </xsl:template>
     <xsl:template match="eintrag"><xsl:value-of select="nummer"/>;
       <xsl:value-of select="name"/>;
       <xsl:value-of select="info"/>;
       <xsl:if test="position()!=last()">
         <xsl:text>&#xA;</xsl:text>
       </xsl:if>
     </xsl:template>
   </xsl:stylesheet>
