.. meta::

   :description lang=de: XML-Kurs mit den Themen: DTD, Schema, XSL, XPath
   :keywords: XSL, Attribute, OER

:ref:`« XSL-Start <xsl-start>`

.. index:: XSL; Attribute

=====================
Zugriff auf Attribute
=====================

.. image:: ../images/frost002.jpg
   :width: 0px


.. |a| raw:: html

   <!-- lightbox container hidden with CSS -->
   <a href="#" class="lightbox" id="img1">
   <span style="background-image: url('../../_images/frost002.jpg')"></span>
   </a>

.. |b| raw:: html

   <a href="#img1">
   <img width="250px" src="../../_images/frost002.jpg">
   </a>

.. sidebar:: Frost

   |b|

|a|

Lernziel
========
Nun lernen Sie, wie Sie auf Attribute zugreifen.

Handlungsanweisungen
====================

:Aufgaben:
  1. Verwenden Sie die Daten/Dateien der letzten Übung.
  2. Geben Sie die Herkunft der in der CD-Datenbank vorhandenen Bands aus.
  3. Erweitern Sie dazu Ihre Tabelle um eine Spalte.

Die XML-Datei
~~~~~~~~~~~~~

Wie können wir nun auf Attribute zugreifen? Schauen Sie sich zunächst
die XML-Datei an. In ihr befindet sich ein Attribut (Zeile 9).

.. code-block:: xml
   :linenos:

   <adressensammlung>
     <adresse>
       <anrede>Herr</anrede>
       <name>
         <nachname>Becker</nachname>
         <vorname>Heinz</vorname>
       </name>
       <strasse>Schwabenstr. 4</strasse>
       <postanschrift land="Deutschland">
         <plz>14488</plz>
         <wohnort>Berlin</wohnort>
       </postanschrift>
       <email>heinz@becker.de</email>
     </adresse>
   </adressensammlung>

Die XSL-Datei
~~~~~~~~~~~~~

Um nun auf das Attribut zuzugreifen, können Sie folgenden
XPATH-Ausdruck für die Selektion in der Template-Rule schreiben:


::

   <xsl:value-of select="adresse/postanschrift/@land"/>

Der Zugriff auf das Attribut erfolgt durch das **@**-Zeichen, gefolgt vom Namen des Attributes. 
