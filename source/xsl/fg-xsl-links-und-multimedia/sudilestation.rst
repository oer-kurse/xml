.. meta::

   :description lang=de: XML-Kurs mit den Themen: DTD, Schema, XSL, XPath
   :keywords: XSL, Attribute, OER

:ref:`« XSL-Start <xsl-start>`

.. index:: XSL; attributes, Attribute erzeugen

===========================
Neue Elemente und Attribute
===========================

.. image:: ../images/apfel.jpg
   :width: 0px


.. |a| raw:: html

   <!-- lightbox container hidden with CSS -->
   <a href="#" class="lightbox" id="img1">
   <span style="background-image: url('../../_images/apfel.jpg')"></span>
   </a>

.. |b| raw:: html

   <a href="#img1">
   <img width="250px" src="../../_images/apfel.jpg">
   </a>

.. sidebar:: Apfel

   |b|

|a|

Lernziel
========

Die Bezeichnungen von Attributen und Links passen nicht immer zum Ausgabeformat, in unserem Fall HTML.
Deshalb kann es vorkommen, dass neue Elemente und Attribute generiert
werden müssen. Wie das funktioniert, wird hier demonstriert.

Handlungsanweisungen
====================
:Aufgaben:

  1. Erstellen Sie eine Linksammlung in XML mit Beschreibung und dem Link.
  2. Der Link sollte ein grafischer Link sein.

.. index: Link erzeugen

Links erzeugen
~~~~~~~~~~~~~~

Wie erzeugen Sie nun ein Bild oder einen Link in XSL? Schauen wir uns
zunächst die XML-Datei an. Angenommen Sie haben die Aufgabe eine
Galerie zu erstellen, die ein Bild, eine Beschreibung und den Künstler
enthält...


.. code-block:: xml
   :linenos:

   <?xml version="1.0" encoding="UTF-8"?>
   <bildersammlung>
     <bild nr="1" quelle="impressionen.png">
       <beschreibung>Ein Tupfertechnik-Werk!</beschreibung>
       <kuenstler>http://www.kuenstler-x.de</kuenstler>
     </bild>
   </bildersammlung>


Sie sehen das Wurzelelement *bildersammlung*, das aus einem
Kindelement *bild* besteht. Die Name der zugehörigen Grafik ist im
Attribut *quelle* gespeichert. Der Link zum Ersteller ist im Element
*kuenstler* zu finden.


.. index: Bildelement (HTML) erzeugen

Die XSL-Datei
~~~~~~~~~~~~~
Ein Bild-Element für HTML aus diversen XML-Elementen zusammensetzten,
die Übersetzung (Transformation) sieht dann wie folgt aus:

.. code-block:: xml
   :linenos:

   <img>
     <xsl:attribute name="src">
       <xsl:value-of select="@quelle"/>
     </xsl:attribute>
   </img>

Hier sehen Sie den Code-Ausschnitt für das Erzeugen eines Links.

.. code-block:: xml
   :linenos:

   <a>
     <xsl:attribute name="href">
       <xsl:value-of select="ersteller"/>
     </xsl:attribute>
     <xsl:value-of select="ersteller"/>
   </a>

Mit den eben gezeigten Anweisungen erzeugen wir ein Attribut für die
umklammernden Elemente. Im ersten Fall also *img*, im zweiten *a*. Für
*img* wählen wir das Attribut *src*, da so die Bildquelle in HTML
definiert wird, für *a* verwenden wie ebenfalls die XSL-Anweisunge
*xsl:atttribute* nur dass das Anker-Element im HTML ein *href*
benötigt. Innerhalb des xsl:attribute-Elementes steht der konkrete
Inhalt des jeweiligen Attributes. Also einmal die Bildquelle (@quelle)
und einmal der Link (Ersteller) -- siehe Zeilen 2 und 5.


.. index: XSL: Variablen

Die Verwendung einer Variablen
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. code-block:: xml
   :linenos:

   <!--Variable definieren-->
   <xsl:variable name="email" select="@email"/>

   <!--Variable in Attribut einfügen-->
   <a href="mailto:{$email}">
	<xsl:value-of select="."/>
   </a>

Die Definition einer Variablen erfolgt über das xsl-Element
*xsl:variable* und deren Wiederverwendung erfolgt mit einem $-Zeichen
vor dem Variablennamen und dem Einschluss in geschweifte Klammern.
