.. meta::

   :description lang=de: XML-Kurs mit den Themen: DTD, Schema, XSL, XPath
   :keywords: XSL, Template Rules, PC-Liste, OER

:ref:`« XSL-Start <xsl-start>`

.. index:: Template Rules; Übung PC-Liste I

=============================================
Demo/Übung PC-Liste (Wiederholung+Vertiefung)
=============================================

.. image:: ../images/baum.jpg
   :width: 0px

.. |a| raw:: html

   <!-- lightbox container hidden with CSS -->
   <a href="#" class="lightbox" id="img1">
   <span style="background-image: url('../../_images/baum.jpg')"></span>
   </a>

.. |b| raw:: html

   <a href="#img1">
   <img width="250px" src="../../_images/baum.jpg">
   </a>

.. sidebar:: Baum

   |b|

|a|


Lernziel
========
Die folgenden Stationen dienen der Vertiefung und Ergänzung. Das
Abarbeiten ist all denen zu empfehlen, die noch unsicher und an einem
weiteren Beispiel interessiert sind. Am Ende der Aufgabenserie
erzeugen Sie aus der XML-Datei eine Tabelle, die etwa so aussieht:

.. image:: img/loesung-pc-liste.png

Sie lernen auch noch weitere XSL- und XPATH-Anweisungen kennen.

Handlungsanweisungen
====================
:Aufgaben:

1. Laden Sie die Dateien herunter, die Ausgangspunkt für alle Transformationen sind.

   :download:`Download pcliste.xml <downloads/pcliste-fuer-saxon.xml>`

2. Legen Sie für jede Übung einen Ordner an und platzieren sie in dem Ordner:
   
   - pcliste.xml (pcliste-fuer-saxon.xml auf den kürzeren Namen umbenennen).
   - pcliste01.xsl (neu erstellen)
   - saxonhe-xx.jar (für die Transformation)
     
3.  Erstellen Sie für jeden Schritt eine neue XLS-Datei
    (z.B. pcliste01.xsl, pdliste02.xsl, ...). Übernehmen Sie jeweils
    den Bearbeitungsstand der vorherigen Übung und fügen Sie die
    neuen Funktionen und Anweisungen hinzu.
4. Machen Sie sich mit dem Inhalt der XML-Datei vertraut.
5. Geben Sie alle Rechner in Form einer Tabelle pro Raum aus.
   Das Ausgabeformat ist HTML, verwenden Sie XSL-Templates-Rules.
6. Erstellen Sie dazu eine erste XSL-Datei und binden Sie diese in die XML-Datei ein.
7. Validieren Sie die Dateien und geben Sie das fertige HTML in eine Datei
   pcliste.html aus.

   Beispiel einer Transformation (Siehe Anleitung für den »Saxon«-Parser:
   :ref:`wie hier beschieben  <saxonhe>`.):
   
:Anmerkung:

   Die Lösung finden Sie in der nächsten Station.
   **Versuchen Sie vorher die Aufgabe selbständig zu lösen!**

.. image:: ../images/peter.png
   :width: 100px


:Zitat:  »Die Neugier steht immer an erster Stelle eines Problems,
         das gelöst werden will.«

         Galileo Galilei (italienischer Physiker und Astronom)

         15.02.1564 - 08.01.1642

