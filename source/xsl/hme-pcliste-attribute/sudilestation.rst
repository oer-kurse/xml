.. meta::

   :description lang=de: XML-Kurs mit den Themen: DTD, Schema, XSL, XPath
   :keywords: XSL, Template Rules, PC-Liste, OER

:ref:`« XSL-Start <xsl-start>`

.. index:: Template Rules; Übung PC-Liste IV (Hervorhebungen)


=========================
PC-Liste: Hervorhebung II
=========================

.. image:: ../images/bluete006.jpg
   :width: 0px


.. |a| raw:: html

   <!-- lightbox container hidden with CSS -->
   <a href="#" class="lightbox" id="img1">
   <span style="background-image: url('../../_images/bluete006.jpg')"></span>
   </a>

.. |b| raw:: html

   <a href="#img1">
   <img width="250px" src="../../_images/bluete006.jpg">
   </a>

.. sidebar:: Blüte

   |b|

|a|

Lernziel
========
Die folgenden Stationen dienen der Vertiefung und Ergänzung. Das
Abarbeiten ist all denen zu empfehlen, die noch unsicher und an einem
weiteren Beispiel interessiert sind. Am Ende der Aufgabenserie
erzeugen Sie aus der XML-Datei eine Tabelle, die etwa so aussieht:

.. image:: ../images/loesung-pc-liste.png

Sie lernen auch noch weitere XSL- und XPATH-Anweisungen kennen.

Handlungsanweisungen
====================

:Neue Aufgaben: | Geben Sie die Zeilen der Tabelle, im Wechsel mit
                | unterschiedlichen Farben, aus.

.. image:: ../images/peter.png
   :height: 100px


:Zitat: | »Es gibt Dinge, die man bereut, ehe man sie tut.
        |  Und man tut sie doch.«  -- Friedrich Hebbel

Lösung zu den Aufgaben:
=======================

1. Erstellen Sie eine neue Spalte, die den Status eines Rechners ausgibt.
2. Geben Sie diesmal den Rechnern mit dem Status *schulungrechner*
   (HINWEIS: den Tippfehler beachten) die Farbe grün und allen anderen
   Rechnern die Hintergrundfarbe orange.

Siehe Zeilen: 28, 42, 45, 49, 52

.. code-block:: xml
   :linenos:

   <xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
   <xsl:output method="html"/>
   <xsl:template match="/">

    <html>
       <head>
         <title>Rechneruebersicht</title>
         <link rel="stylesheet" type="text/css" href="formate.css"/>
       </head>
       <body>
       <h2>Die Liste aller Rechner</h2>
       <!--[weitere Regeln abarbeiten] -->
       <xsl:apply-templates />
       </body>
     </html>

   </xsl:template>

   <xsl:template match="pcliste/raum">
     <h3>
       <xsl:value-of select="@bezeichnung"/>
     </h3>

     <table border="0" class="kopf">
       <tbody>
       <tr>
         <th>Nummer</th>
         <th>Status</th>
         <th>Anmerkung</th>
         <th>Rechner</th>
         <th>IP</th>
       </tr>
       <!--[weitere Regeln abarbeiten] -->
       <xsl:apply-templates />
       </tbody>
     </table>

   </xsl:template>

   <xsl:template match="rechner">

       <xsl:if  test="anmerkung/@status='Schulungrechner'">
       <tr class="wichtig">
         <td><xsl:number format="I" count="rechner" /></td>
         <td>Schulungsrechner</td>
         <xsl:apply-templates />
       </tr>
       </xsl:if>
       <xsl:if test="anmerkung/@status!='Schulungrechner'">
       <tr class="schulungsrechner">
         <td><xsl:number format="I" count="rechner" /></td>
         <td><xsl:value-of select="anmerkung/@status" /></td>
         <xsl:apply-templates />
       </tr>
       </xsl:if>

   </xsl:template>

   <xsl:template match="anmerkung | rechnerip | rechnername">
         <td>
           <!--[Zugriff auf den Element-Inhalt] -->
	   <xsl:value-of select="." />
         </td>
   </xsl:template>

   </xsl:stylesheet>

Im ersten Schritt legen wir für den Tabellenkopf eine neue Spalte an.
Der Vergleichstest bezieht sich diesmal auf ein Attribut. Im ersten Fall,
es handelt sich um einen Schulungsrechner, geben wir die Zeichenfolge
„Schulungsrechner“ aus und verschleiern nach außen den kleinen Tippfehler.

Wenn das status-Attribut etwas anderes enthält, wollen wir sehen,
was drinsteht und greifen deshalb auf den Attributinhalt zu.

Siehe Zeilen:  33-39

.. code-block:: text
   :linenos:


   body
   {
     background-color: #ffffff;
     color: #000000;
     font-family:arial;
   }

   h2{
     color:#0000ff;
   }

   h3{
     color:#00ff00;
   }

   table {
      border-width:2px;
      border-style:solid;
      border-color:gray;
   }
   .kopf {
     background-color:green;}

   th {
     align:left;
   }

   td {
     border-width:2px;
     border-style:solid;
   }

   .wichtig{
     background-color:green;
   }

   .schulungsrechner {
     background-color:orange;
   }
