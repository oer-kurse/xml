.. meta::

   :description lang=de: XML-Kurs mit den Themen: DTD, Schema, XSL, XPath
   :keywords: XSL, OER

:ref:`« Kursstart <xml-kurs-start>`

.. _xsl-start:

.. index:: XSL; Übersicht

.. image:: ./images/ccc2014.jpg
   :width: 0px


.. |a| raw:: html

   <!-- lightbox container hidden with CSS -->
   <a href="#" class="lightbox" id="img1">
   <span style="background-image: url('../_images/ccc2014.jpg')"></span>
   </a>

.. |b| raw:: html

   <a href="#img1">
   <img width="250px" src="../_images/ccc2014.jpg">
   </a>

.. sidebar:: CCC - 2014

   |b|

   Berlin

|a|

===============================
XPath/Transformation (XSL/XSLT)
===============================

.. toctree::
   :maxdepth: 1
   :glob:

   */*
