.. meta::

   :description lang=de: XML-Kurs mit den Themen: DTD, Schema, XSL, XPath
   :keywords: XSL, if, choose, OER

:ref:`« XSL-Start <xsl-start>`

.. index:: XSL; if, XSL; choose

=======================
Bedingungen (if,choose)
=======================

.. image:: ../images/lucas-cranach-adam-eva.jpg
   :width: 0px


.. |a| raw:: html

   <!-- lightbox container hidden with CSS -->
   <a href="#" class="lightbox" id="img1">
   <span style="background-image: url('../../_images/lucas-cranach-adam-eva.jpg')"></span>
   </a>

.. |b| raw:: html

   <a href="#img1">
   <img width="250px" src="../../_images/lucas-cranach-adam-eva.jpg">
   </a>

.. sidebar:: Adam & Eva

   |b|

   Licas Cranach

|a|


Lernziel
========

Sie lernen, wie Sie in XSL Programmverzweigungen durch
Fallunterscheidung realisieren.

Handlungsanweisungen
~~~~~~~~~~~~~~~~~~~~

:Aufgaben:
  1. Die CDs, die in Europa entstanden sind, sollen in der Tabelle rot erscheinen, 
     die anderen blau.
  2. Geben Sie nur die Namen aus, die mehr als 200 Punkte haben!
     Gegeben ist folgende XML-Datei:

.. code-block:: xml
   :linenos:

   <?xml version="1.0" encoding="UTF-8"?>
   <?xml-stylesheet type="text/xsl" href="if.xsl" ?>
   <ergebnisse>
     <ergebnis>
       <name>Crystol Freak</name>
       <punkte>453</punkte>
     </ergebnis>
     <ergebnis>
       <name>Demian</name>
       <punkte>199</punkte>
     </ergebnis>
     <ergebnis>
       <name>Ueberflieger</name>
       <punkte>347</punkte>
     </ergebnis>
     <ergebnis>
       <name>CaptainX</name>
       <punkte>106</punkte>
     </ergebnis>
   </ergebnisse>

:Zusatz:
  Geben Sie die CDs Ihrer CD-Datenbank aus, die von deutschen Bands produziert wurden.

.. index: if, Verzweigung

Bedingte Ausgaben mit if
~~~~~~~~~~~~~~~~~~~~~~~~~

Bedingte Ausgaben könne Sie mit if realisieren, ein else ist nicht möglich.

.. code-block:: xml
   :linenos:

   <xsl:if test="Bedingung">
    ... Bedingte Ausgabe...
   </xsl:if>
   <xsl:if test="nachname='Becker'">
    <xsl:value-of select="vorname" />
   </xsl:if>

.. index: choose, otherwise (else)

Choose-Verzweigungen
~~~~~~~~~~~~~~~~~~~~

Verzweigungen können Sie auch mit choose erreichen. Hier haben Sie auch else-Regeln, 
die bei choose „otherwise“ heißen.

.. code-block:: xml
   :linenos:

   <xsl:choose>
   <xsl:when test="Bedingung">
    ....irgendeine Ausgabe....
   </xsl:when>
   <xsl:otherwise>
    ...eine andere Ausgabe...
   </xsl:otherwise>
   </xsl:choose> 

.. index: Vergleichsoperatoren

Vergleichsoperatoren
====================

Welche Vergleichsoperatoren kennt XSL? 
::

  =      ist gleich
  !=     nicht gleich
  &lt;   weniger als
  &gt;   größer als
  &lt;=  kleiner gleich
  &gt;=  größer gleich


Die Operatoren &lt; und &gt; lassen sich jedoch nur wirklich sinnvoll
auf Ganzzahlen anwenden. Die < und > müssen mit &lt; und &gt;
umschrieben werden.

.. index: logische Operatoren

Logische Operatoren
~~~~~~~~~~~~~~~~~~~

Welche logischen Operatoren kennt XSL?
::

  and
  or
  not

**Beispiel**

.. code-block:: xml
   :linenos:

   <xsl:when test="alter &gt; '20' and alter &lt; '30'">
   ...
   </xsl:when>
   <xsl:when test="name='Heinz' or name='Uwe'">
   ...
   </xsl:when>
   <!--Wenn das Element email in der XML-Datei nicht vorhanden ist-->
   <xsl:when test="not(email)">
   ...
   </xsl:when>


:Ziel: Je nach Geschlecht soll bei der Ausgabe eine unterschiedliche
       Hintergrundfarbe verwendet werden.


.. code-block:: xml
   :linenos:

   <?xml version="1.0" encoding="ISO-8859-1"?>
   <xsl:stylesheet version="1.0"
        xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
   <xsl:output method="html"/>

   <xsl:template match="/">

   <xsl:for-each select="adresse/person">
     <xsl:choose>
       <xsl:when test="anrede='Frau'">
         <p bgcolor="pink">
           <xsl:value-of select="name/nachname"/>
           <xsl:value-of select="name/vorname"/>
         </p>
       </xsl:when>
       <xsl:otherwise>
         <p bgcolor="lightblue">
           <xsl:value-of select="name/nachname"/>
           <xsl:value-of select="name/vorname"/>
         </p>
       </xsl:otherwise>
     </xsl:choose>
   </xsl:for-each>

   </xsl:template>

   </xsl:stylesheet>

Für jede Person wird getestet, ob die Anrede *Frau* ist. Ist dem so,
wird ein lila, ansonsten ein hellblauer Hintergrund gewählt. In beiden
Fällen wird der Vor- und Nachname in eine Tabellenzeile geschrieben.
