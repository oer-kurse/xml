.. meta::

   :description lang=de: XML-Kurs mit den Themen: DTD, Schema, XSL, XPath
   :keywords: XSL, Template Rules, PC-Liste, OER

:ref:`« XSL-Start <xsl-start>`

.. index:: Template Rules; Übung PC-Liste IV (Nummerierung)
.. index:: TemplateRules; Nummerierung

=====================
PC-Liste: Nummerieren
=====================

.. image:: ../images/bluete003.jpg
   :width: 0px


.. |a| raw:: html

   <!-- lightbox container hidden with CSS -->
   <a href="#" class="lightbox" id="img1">
   <span style="background-image: url('../../_images/bluete003.jpg')"></span>
   </a>

.. |b| raw:: html

   <a href="#img1">
   <img width="250px" src="../../_images/bluete003.jpg">
   </a>

.. sidebar:: Blüte

   |b|

|a|

Lernziel
========
Die folgenden Stationen dienen der Vertiefung und Ergänzung. Das
Abarbeiten ist all denen zu empfehlen, die noch unsicher und an einem
weiteren Beispiel interessiert sind. Am Ende der Aufgabenserie
erzeugen Sie aus der XML-Datei eine Tabelle, die etwa so aussieht:

.. image:: ../images/loesung-pc-liste.png

Sie lernen auch noch weitere XSL- und XPATH-Anweisungen kennen.

Handlungsanweisungen
====================

:Neue Aufgaben: 1. Heben Sie den Rechner mit dem Namen *Feuerbach* farblich hervor.

.. image:: ../images/peter.png
   :height: 100px

:Zitat: »Das Werk eines Meisters riecht nicht nach Schweiß,
        verrät keine Anstrengung und ist von Anfang an fertig.« -- James McNeill Whistler 


Lösung zu den Aufgaben:
=======================

1. Nummerieren Sie die Positionen für jeden Raum durch.
2. Geben Sie die Nummern in einer zusätzlichen  Spalte aus.

Siehe Zeilen 26, 38-43

.. code-block:: xml
   :linenos:

   <xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
   <xsl:output method="html"/>
   <xsl:template match="/">

    <html>
       <head>
         <title>Rechneruebersicht</title>
       </head>
       <body>
       <h2>Die Liste aller Rechner</h2>
       <!--[weitere Regeln abarbeiten] -->
       <xsl:apply-templates />
       </body>
     </html>

   </xsl:template>

   <xsl:template match="pcliste/raum">
     <h3>
       <xsl:value-of select="@bezeichnung"/>
     </h3>

     <table border="1">
       <tbody>
       <tr>
         <th>Nummer</th>
         <th>Anmerkung</th>
         <th>Rechner</th>
         <th>IP</th>
       </tr>
       <!--[weitere Regeln abarbeiten] -->
       <xsl:apply-templates />
       </tbody>
     </table>

   </xsl:template>

   <xsl:template match="rechner">
       <tr>
         <td><xsl:number format="I" count="rechner" /></td>
         <xsl:apply-templates />
       </tr>
   </xsl:template>

   <xsl:template match="anmerkung | rechnerip | rechnername">
         <td>
           <!--[Zugriff auf den Element-Inhalt] -->
           <xsl:value-of select="." />
         </td>
   </xsl:template>

   </xsl:stylesheet>

Weil eine neue Spalte benötigt wird, erstellen wir eine neue Überschrift.

Der XSL-Ausdruck *xsl:number* benötigt zwei Angaben:

- das Format (I = römische Zahlen)
- und einen Elementnamen, der gezählt werden soll.

Variieren Sie das Format und prüfen Sie jeweils die Ausgaben.
