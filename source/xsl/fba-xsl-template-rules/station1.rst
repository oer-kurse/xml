.. meta::

   :description lang=de: XML-Kurs mit den Themen: DTD, Schema, XSL, XPath
   :keywords: XSL, Template Rules, OER


:ref:`« XSL-Start <xsl-start>`

.. index:: Template Rules; Demo I

================
Template Rules I
================

.. image:: ../images/schild-fasse-dich-kurz.jpg
   :width: 0px


.. |a| raw:: html

   <!-- lightbox container hidden with CSS -->
   <a href="#" class="lightbox" id="img1">
   <span style="background-image: url('../../_images/schild-fasse-dich-kurz.jpg')"></span>
   </a>

.. |b| raw:: html

   <a href="#img1">
   <img width="250px" src="../../_images/schild-fasse-dich-kurz.jpg">
   </a>

.. sidebar:: Schilder...

   |b|

|a|


Lernziel
========

Sie lernen Template Rules kennen und können Templates für bestimmte Elemente definieren.

Handlungsanweisungen
====================

:Aufgaben:

Geben Sie den Inhalt aller Elemente der folgenden XML-Datei
durch Template Rules aus:

.. code-block:: xml
   :linenos:

   <adressen_db>
     <adresse nr="1">
       <name>Hubert Taler</name>
       <anschrift>
         <strasse>Weinberg 1</strasse>
         <plz>14777</plz>
         <ort>Berlin</ort>
       </anschrift>
     </adresse>
     <adresse nr="2">
       <name>Uni Potsdam</name>
       <anschrift>
         <strasse>Am Park Babelsberg 3</strasse>
         <plz>14482</plz>
         <ort>Potsdam</ort>
       </anschrift>
     </adresse>
   </adressen_db>




Template Rules – xml
====================

Eine XML-Struktur in eine andere umzuwandeln, kann unterschiedliche Gründe haben:

- ein anderes Ausgabeformat wird benötigt, z.B. HTML, Latex (Vorstufe eines PDF), ...
- ein Teil der vorhandenen Informationen soll gefiltert ausgegeben werden
- das XML-Dokument soll mit einem Anderen Dokument verknüpft werden
- mit der Ausgabe sollen weitere Zusatz-Angaben generiert werden
- ...

Für die Ausgabe werden Regeln benötigt und aufgestellt, die bei der
Verarbeitung angewendet werden. Diesen Prozess nennt man
Transformation.

Ausgangspunkt der Transformation ist eine Datei (Dateinedung XSL) mit
den Regeln für die Transformation, wobei ein Satz von eingebauten
Regeln immer wirksam ist.

Wenn keine passende Regel vorhanden ist, werden alle Kindelemente
durchwandert und für jedes der Wert bzw. der Text im XML-Dokument
ausgegeben.

Durch eine Satz von Regeln wird das Standarverhalten geändert und die
gewünschte Ausgabe generiert. Dies kann in zwei Schritten erfolgen:

1. Finden der interessierenden Knoten (Elemente, Attribute) mit der
   Unterstützung von XPath-Ausdrücken.

2. Ergänzung oder Unterdrückung der gefundenen Inhalte, mit der
   Ausgabe.

Beispiele
=========

:Ziel:

1. Ausgabe aller Vor- und Nachnamen.

Hier die XML-Datei:

.. code-block:: xml
   :linenos:

   <?xml version="1.0" encoding="UTF-8"?>
   <?xml-stylesheet type="text/xsl" href="adressen.xsl"?>
   <adresse>
     <person>
       <anrede>Herr</anrede>
       <name>
         <nachname>Becker</nachname>
         <vorname>Heinz</vorname>
       </name>
       <strasse>Schabenstr. 4</strasse>
       <postanschrift>
        <plz>14489</plz>
        <wohnort>Berlin</wohnort>
       </postanschrift>
       <email>becker@heinz.de</email>
     </person>
     <person>
       <anrede>Frau</anrede>
       <name>
          <nachname>Rennt</nachname>
          <vorname>Lola</vorname>
       </name>
       <strasse>Laufsteig 17</strasse>
       <postanschrift>
          <plz>14888</plz>
          <wohnort>Potsdam</wohnort>
       </postanschrift>
       <email>lolo@lola.de</email>
     </person>
   </adresse>


Template Rules – xsl
====================

Schauen Sie sich zunächst die XSL-Struktur an. Die Erklärungen folgen nach dem Code-Beispiel. 

.. code-block:: xml
   :linenos:

   <?xml version="1.0" encoding="UTF-8" ?>
   <xsl:stylesheet
	version="2.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output method="xml" indent="yes"/>

	<xsl:template match="/">
		<xsl:apply-templates/>
	</xsl:template>

   </xsl:stylesheet>

Transformation mit saxon
========================
Falls noch nicht installiert, finden Sie unter :ref:`Werkzeuge Saxon <saxonhe>`
eine Anleitung.

::

   java -jar saxon9he.jar adressen.xml adressen.xsl

Ausgabe nach der Transformation
===============================
::

   <?xml version="1.0" encoding="UTF-8"?>

		Herr

			Becker
			Heinz

		Schabenstr. 4

			14489
			Berlin

		becker@heinz.de


		Frau

			Rennt
			Lola

		Laufsteig 17

			14888
			Potsdam

		lolo@lola.de

**Erklärungen:**
::

   <xsl:template match="/">

Wir starten die Transformation im Root-Element der XML-Datei durch die
Anwendung des XPath-Ausdrucks »match="/"«.
Wenn der Wurzel-Knoten gefunden wurde, werden alle weiteren
Template-Rules aufgerufen/ausgeführt.

::

   <xsl:apply-templates/>

Weil es aber (noch) *keine* weiteren Template-Rules gibt, greift eine
eingebaute *Standard-Regel* und die lautet:

**Gibt alle Inhalte aller Elemente aus, für die es keine Regel gibt.**

Damit nicht alles ausgegeben wird erweitern wir die XSL-Datei:

.. code-block:: xml
   :linenos:

   <?xml version="1.0" encoding="UTF-8" ?>
   <xsl:stylesheet
	version="2.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output method="xml" indent="yes"/>

	<xsl:template match="/">
		<xsl:apply-templates/>
	</xsl:template>

	<!-- Überschreiben der Standard-Regel und
	     Unterdrückung der Ausgabe: siehe nächster Kommentar
	-->
	<xsl:template match="text()|@*">
		<!--xsl:value-of select="."/-->
	</xsl:template>

   </xsl:stylesheet>

Mit der folgenden Anweisung wird der Wert eines Elementes ausgelesen:

      <xsl:value-of select="."/>

Das haben wir durch einen Kommentar weiter oben verhindert.

Nun können wir gezielt auf einzelne Werte zugreifen und definieren
dafür weitere Regeln:

.. code-block:: xml
   :linenos:

   <?xml version="1.0" encoding="UTF-8" ?>
   <xsl:stylesheet
	version="2.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output method="xml" indent="yes"/>

	<xsl:template match="/">
	  <xsl:apply-templates/>
	</xsl:template>

	<xsl:template match="adresse">
	  Wurzel-Element gefunden
	  <xsl:apply-templates select="person"/>
	</xsl:template>

	<xsl:template match="person">
	  Person gefunden
	  <xsl:apply-templates />
	</xsl:template>

	<!-- Überschreiben der Standard-Regel und
	     Unterdrückung der Ausgabe: siehe nächster Kommentar
	-->
	<xsl:template match="text()|@*">
	  <!--xsl:value-of select="."/-->
	</xsl:template>

   </xsl:stylesheet>

Immer wenn ein XPath-Ausdruck zutrifft, wird die Regel abgearbeitet,
in unserem Fall geben wir eine neue Zeichenkette aus, die bestätigt,
dasss die Regel gefunden und angewendet worden ist.

Hier das vorläufige Ergebnis:

::


   Wurzel-Element gefunden

   Person gefunden

   Person gefunden


Werte statt Hilfstexte
======================

.. code-block:: xml
   :linenos:

   <?xml version="1.0" encoding="UTF-8" ?>
   <xsl:stylesheet version="1.0"
     xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

	<xsl:output method="xml" indent="yes"/>

	<xsl:template match="/">
		<xsl:apply-templates/>
	</xsl:template>

	<xsl:template match="vorname">
	  <xsl:value-of select="."/>
	</xsl:template>

	<xsl:template match="nachname">
	  <xsl:value-of select="."/>
	</xsl:template>

	<!-- Überschreiben der Standard-Regel und
	     Unterdrückung der Ausgabe: siehe nächster Kommentar
	-->

	<xsl:template match="text()|@*">
		  <!--xsl:value-of select="."/-->
	</xsl:template>
   </xsl:stylesheet>

Nun haben wir die Werte, aber die Ausgabe sieht nicht wirklich gut
aus! Wie das geändert werden kann, wird in der nächsten Station
gezeigt.
