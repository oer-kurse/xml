.. meta::

   :description lang=de: XML-Kurs mit den Themen: DTD, Schema, XSL, XPath
   :keywords: XSL, Template Rules, OER


:ref:`« XSL-Start <xsl-start>`

.. _xsl-adressen-transformation:

.. index:: Template Rules; Demo II

=================
Template Rules II
=================

.. image:: ../images/unkraut.jpg
   :width: 0px


.. |a| raw:: html

   <!-- lightbox container hidden with CSS -->
   <a href="#" class="lightbox" id="img1">
   <span style="background-image: url('../../_images/unkraut.jpg')"></span>
   </a>

.. |b| raw:: html

   <a href="#img1">
   <img width="250px" src="../../_images/unkraut.jpg">
   </a>

.. sidebar:: Unkraut?

   |b|

|a|

Lernziel
========

Sie lernen Template Rules kennen und können Templates für bestimmte
Elemente definieren.


Handlungsanweisungen
====================
:Aufgaben:

1. Was ist zur vorherigen Variante anders?
2. Ausgabe (Verpackung) der gefunden Daten in ein HTML-Gerüst
   und damit eine Anzeigemöglichkeit für den Browser

Die Verpackung als HTML-Tabelle
===============================
Die Ausgabe soll auch im Browser korrekt dargestellt werden, deshalb
wird in jeder Regel das passende »Verpackungsmaterial« um die
gefundenen Daten gelegt und wir erhalten eine korrekt formatierte
HTML-Tabelle:

.. code-block:: xml
   :linenos:
   :emphasize-lines: 9-13, 15-16, 21, 23, 27, 29


   <?xml version="1.0" encoding="UTF-8" ?>
   <xsl:stylesheet version="1.0"
     xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

	<xsl:output method="xml" indent="yes"/>

	<xsl:template match="/">

	<html>
	<head>
	<title>Adressen von XML zu HTML</title>
	</head>
	<body>
	<table border="1">
	  <xsl:apply-templates/>
	</table>
	</body>
	</html>

      </xsl:template>

	<xsl:template match="name">
	  <tr>
	    <xsl:apply-templates/>
	   </tr>
	</xsl:template>

	<xsl:template match="vorname">
	<td>
	  <xsl:value-of select="."/>
	</td>

	</xsl:template>

	<xsl:template match="nachname">
	<td>
	   <xsl:value-of select="."/>
	</td>
	</xsl:template>

	<!-- Überschreiben der Standard-Regel und
	     Unterdrückung der Ausgabe: siehe nächster Kommentar
	-->

	<xsl:template match="text()|@*">
		  <!--xsl:value-of select="."/-->
	</xsl:template>
   </xsl:stylesheet>

Wie an den Hervorhebungen zu erkennen ist, wurde für das Wurzellement,
das HTML-Gerüst eingefügt und dort wo der Inhalt der Website
erscheinen soll, die nächsten Template-Rules aufgerufen.

Zusätzlich wurde für das Elemente »name« eine Regel erstellt, um eine
Tabellenzeile erzeugen zu könnne. das HTML-Element »tr« bildet wie das
XML-Element »name« eine Klammer oder ist ein Container für die Elemente
»vorname« und »nachname«.


Beispiel einer Transformation (die Dateinamen sind anzupassen):
::

   java -jar ./saxon9he.jar adressen.xml adressen-to-html.xsl

Ergebnis der Transformation
~~~~~~~~~~~~~~~~~~~~~~~~~~~

::

   <?xml version="1.0" encoding="UTF-8"?>
   <html>
      <head>
         <title>Adressen von XML zu HTML</title>
      </head>
      <body>
         <tr>
            <td>Becker</td>
            <td>Heinz</td>
         </tr>
         <tr>
            <td>Rennt</td>
            <td>Lola</td>
         </tr>
      </body>
   </html>
