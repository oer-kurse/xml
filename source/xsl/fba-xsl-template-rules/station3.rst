.. meta::

   :description lang=de: XML-Kurs mit den Themen: DTD, Schema, XSL, XPath
   :keywords: XSL, Template Rules, Aufgabe, Übung, OER


:ref:`« XSL-Start <xsl-start>`

.. _xsl-spruechesammlung-transformation:

.. index:: Template Rules; Aufgabe

=========================
Template Rules -- Aufgabe
=========================

.. image:: ../images/polnisch-russische-grenze.jpg
   :width: 0px


.. |a| raw:: html

   <!-- lightbox container hidden with CSS -->
   <a href="#" class="lightbox" id="img1">
   <span style="background-image: url('../../_images/polnisch-russische-grenze.jpg')"></span>
   </a>

.. |b| raw:: html

   <a href="#img1">
   <img width="250px" src="../../_images/polnisch-russische-grenze.jpg">
   </a>

.. sidebar:: Polnisch-Russische Grenze

   |b|

|a|


Lernziel
========

Grau ist alle Theorie! Sie haben das Prinzip der Template Rules
verstanden? Wenn ja, dann können Sie die folgende Herausforderung
annehmen!


Aufgaben
~~~~~~~~

1. Schreiben Sie eine XSL-Datei mit Style-Attributen, um den Inhalt der unten
   gezeigten XML-Datei als HTML anzeigen zu lassen.
2. Fügen Sie drei weitere Sprüche zur Sprüchesammlung hinzu.
3. Zusatz für Designer: Die rümpfen die Nase, über die Style-Angaben
   am DIV-Element, denn Auszeichnungen für
   CSS gehören in eine separate Datei. Wie kann das realisiert werden.
   Verwenden Sie Ihre Lieblingssuchmaschine um eine Lösung zu finden.
   Haben Sie schon einmal die Suchmaschine "DuckDuckGo.com" ausprobiert?

**Ausgangsdaten: Sprüche im XML-Format**

.. code-block:: xml
   :linenos:

   <?xml version="1.0" encoding="UTF-8"?>
   <!DOCTYPE spruechesammlung [
     <!ELEMENT spruechesammlung (spruch)+>
     <!ELEMENT spruch (kategorie, text, autor, gelebt-von-bis)>
     <!ELEMENT kategorie (#PCDATA)>
     <!ELEMENT text (#PCDATA)>
     <!ELEMENT autor (#PCDATA)>
     <!ELEMENT gelebt-von-bis (#PCDATA)>
   ]>
   <spruechesammlung>
     <!--erster Datensatz-->
     <spruch>
       <kategorie>Erziehung</kategorie>
       <text>
       Bester Beweis einer guten Erziehung ist die Pünktlichkeit.
       </text>
       <autor>Gotthold Ephraim Lessing </autor>
       <gelebt-von-bis>1729-1781</gelebt-von-bis>
     </spruch>
     <!-- zweiter Datensatz-->
     <spruch>
       <kategorie>Ordnung</kategorie>
       <text>
        Bewahre deine Papiere, deine Schlüssel und alles so,
        daß du jedes einzelne Stück auch noch im Dunkeln finden
        kannst. Verfahre noch ordentlicher mit fremden Sachen.
       </text>
       <autor>Adolph Freiherr von Knigge</autor>
       <gelebt-von-bis>1752-1796</gelebt-von-bis>
     </spruch>
     <!-- dritter Datensatz-->
     <spruch>
       <kategorie>Liebe</kategorie>
       <text>
       Der Unterschied zwischen einer Liaison und der
       ewigen Liebe besteht darin, daß die Liaison im
       allgemeinen länger dauert.
       </text>
       <autor>Karl Schönböck</autor>
       <gelebt-von-bis>1909-2001</gelebt-von-bis>
     </spruch>
     </spruechesammlung>


**Gewünschtes Ergebnis als HTML-Ausgabe**

.. image:: img/spruechesammlung.png
   :alt: Sprüchesammlung

:Zusatz: Wer sich mit CSS nicht so auskennt, hier ein paar Varianten:

.. code-block:: xml
   :linenos:

   <div style="font-size:18pt;color:#444444;">
     hier die Kategorie ...
   </div>
   </xsl:template>

   <div style="font-size:12pt;color:green;">
     hier der Text ...
   </div>

   <div style="font-size:12pt;color:orange;padding-left:20px;">
     hier der Autor
   </div>

   <div style="font-size:12pt;color:black;padding-left:50px;">
     hier die Jahreszahlen
   </div>
