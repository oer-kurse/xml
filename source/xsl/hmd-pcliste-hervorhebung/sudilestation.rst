.. meta::

   :description lang=de: XML-Kurs mit den Themen: DTD, Schema, XSL, XPath
   :keywords: XSL, Template Rules, PC-Liste, OER

:ref:`« XSL-Start <xsl-start>`

.. index:: Template Rules; Übung PC-Liste V (Hervorhebungen)

========================
PC-Liste: Hervorhebung I
========================

.. image:: ../images/bluete004.jpg
   :width: 0px


.. |a| raw:: html

   <!-- lightbox container hidden with CSS -->
   <a href="#" class="lightbox" id="img1">
   <span style="background-image: url('../../_images/bluete004.jpg')"></span>
   </a>

.. |b| raw:: html

   <a href="#img1">
   <img width="250px" src="../../_images/bluete004.jpg">
   </a>

.. sidebar:: Blüte

   |b|

|a|

Lernziel
========
Die folgenden Stationen dienen der Vertiefung und Ergänzung. Das
Abarbeiten ist all denen zu empfehlen, die noch unsicher und an einem
weiteren Beispiel interessiert sind. Am Ende der Aufgabenserie
erzeugen Sie aus der XML-Datei eine Tabelle, die etwa so aussieht:

.. image:: ../images/loesung-pc-liste.png

Sie lernen auch noch weitere XSL- und XPATH-Anweisungen kennen.

Handlungsanweisungen
====================

:Neue Aufgaben: 1. Erstellen Sie eine neue Spalte, die den Status eines Rechners ausgibt.
                2. Geben Sie diesmal den Rechnern mit dem Status *schulungrechner* 
                   (HINWEIS: den Tippfehler beachten) die Farbe grün und allen anderen 
                   Rechnern die Hintergrundfarbe orange.

.. image::  ../images/peter.png
   :height: 100px

:Zitat: | »Nichts bewahrt uns so gründlich vor Illusionen
        | wie ein Blick in den Spiegel. «  -- Aldous Huxley

Lösung zu den Aufgaben:
=======================

1. Heben Sie den Rechner mit dem Namen *Feuerbach* farblich hervor.
   Siehe Zeilen: 38 - 45

.. code-block:: xml
   :linenos:

   <xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
   <xsl:output method="html"/>
   <xsl:template match="/">

    <html>
       <head>
         <title>Rechneruebersicht</title>
         <link rel="stylesheet" type="text/css" href="formate.css"/>
       </head>
       <body>
       <h2>Die Liste aller Rechner</h2>
       <!--[weitere Regeln abarbeiten] -->
       <xsl:apply-templates />
       </body>
     </html>

   </xsl:template>

   <xsl:template match="pcliste/raum">
     <h3>
       <xsl:value-of select="@bezeichnung"/>
     </h3>

     <table border="0" class="kopf">
       <tbody>
       <tr>
         <th>Nummer</th>
         <th>Anmerkung</th>
         <th>Rechner</th>
         <th>IP</th>
       </tr>
       <!--[weitere Regeln abarbeiten] -->
       <xsl:apply-templates />
       </tbody>
     </table>

   </xsl:template>

   <xsl:template match="rechner">

       <xsl:if  test="rechnername='Feuerbach' ">
       <tr class="wichtig">
         <td><xsl:number format="I" count="rechner" /></td>
         <xsl:apply-templates />
       </tr>
       </xsl:if>
       <xsl:if test="rechnername!='Feuerbach'">
       <tr class="schulungsrechner">
         <td><xsl:number format="I" count="rechner" /></td>
         <xsl:apply-templates />
       </tr>
       </xsl:if>

   </xsl:template>

   <xsl:template match="anmerkung | rechnerip | rechnername">
         <td>
           <!--[Zugriff auf den Element-Inhalt] -->
	   <xsl:value-of select="." />
         </td>
   </xsl:template>

   </xsl:stylesheet>

Erweiterung
~~~~~~~~~~~
::

  <link rel="stylesheet" type="text/css" href="formate.css"/>

Diese Zeile hat nichts mit XML zu tun, sondern importiert ein
Stylesheet für eine HTML-Seite, die wir mit der Transformation
erzeugen. Wenn die HTML-Seite erzeugt oder geladen wird, findet der
Browser diese Anweisung und holt die Datei mit dem Namen formate.css.
Diese wiederum enthält alle Format-Anweisungen für die
HTML-Elemente, z. B. Farbe, Schrift, ...

Hier nun der Inhalt der Datei formate.css:

.. code-block:: css
   :linenos:

   body
   {
     background-color: #ffffff;
     color: #000000;
     font-family:arial;
   }

   h2{
     color:#0000ff;
   }

   h3{
     color:#00ff00;
   }

   table {
      border-width:2px;
      border-style:solid;
      border-color:gray;
   }
   .kopf {
     background-color:green;}

   th {
     align:left;
   }

   td {
     border-width:2px;
     border-style:solid;
   }

   .wichtig{
     background-color:orange;
   }

   .schulungsrechner {
     background-color:lightgrey;
   }


Legen Sie diese Datei an. Wenn Sie sich auf diesem Gebiet noch nicht auskennen,
finden sie unter folgender Adresse mehr zu diesem Thema:

http://de.selfhtml.org/css/index.htm
