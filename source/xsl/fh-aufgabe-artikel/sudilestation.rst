.. meta::

   :description lang=de: XML-Kurs mit den Themen: DTD, Schema, XSL, XPath
   :keywords: XSL, Transformation, Übung, Aufgabe, OER

:ref:`« XSL-Start <xsl-start>`

.. index:: XSL; Aufgabe

===============================
Aufgabe: Artikel-Transformation
===============================

.. image:: ../images/alf.jpg
   :width: 0px


.. |a| raw:: html

   <!-- lightbox container hidden with CSS -->
   <a href="#" class="lightbox" id="img1">
   <span style="background-image: url('../../_images/alf.jpg')"></span>
   </a>

.. |b| raw:: html

   <a href="#img1">
   <img width="250px" src="../../_images/alf.jpg">
   </a>

.. sidebar:: Alf

   |b|

|a|

Lernziel
========

Alle bisher beschriebenen Techniken können nun in einer komplexen
Übung noch einmal angewendet werden.

Viel Spass beim Knobeln.


Handlungsanweisungen
====================
:Aufgaben:

   Konvertieren Sie die XML nach HTML!

Die XML-Datei als Ausgangsbasis:

.. code-block:: xml
   :linenos:

   <artikel>
     <ueberschrift>XML als Datenaustauschformat</ueberschrift>
     <autor email="info@meising.org">Stefan Meising
       <bild quelle="meising.jpg"/>
     </autor>
     <abschnitt> Sie haben sicher schon viel über <fett>XML</fett> gelesen?!
       <unterstrichen>Aber:</unterstrichen> Können Sie sich auch  ein Bild
       machen von <br/> den Möglichkeiten des XML-Standards? Was kann XML?
     </abschnitt>
     <abschnitt>
       <fett>
         <kursiv>Eine Übersicht:</kursiv>
       </fett>
       <br/>
       <liste type="sortiert">
         <element>XML ist <rot>nur Text</rot> und kann nichts</element>
         <element>XML ist ein Regelwerk zur Beschreibung von Daten</element>
         <element>XML ist hierachisch aufbebaut</element>
         <element>XML ist von Menschen und Programmen lesbar</element>
       </liste>
     </abschnitt>
   </artikel>

Hier noch einige Hinweise für die Entsprechungen der XML-Elemente in HTML:
::

   <h2></h2>    => Überschrift
   <p></p>   => Abschnitt
   <b></b>  =>  fett
   <u></u>  =>  unterstrichen
   <i></i>   =>  kursiv
   <span style="color:red"></span>   =>  rot
   <ul>   =>  Liste
   <li>   =>   Element
   <br/>   =>   <br/>

   Link zur E-Mail:

   <a href="mailto:info@meising.org">E-Mail an Meising</a>

   Grafik:

   <img src="bild.png" />


Ein erste Blick auf das gewünschte Ergebnis:

.. image:: ./artikel-transform-vorschau.png
