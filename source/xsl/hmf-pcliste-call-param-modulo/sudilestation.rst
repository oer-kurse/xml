.. meta::

   :description lang=de: XML-Kurs mit den Themen: DTD, Schema, XSL, XPath
   :keywords: XSL, Template Rules, PC-Liste, OER

:ref:`« XSL-Start <xsl-start>`

.. index:: Template Rules; Übung PC-Liste VII (Modulo)
.. index:: Template Rules; Modulo, Modulo

================
PC-Liste: Modulo
================

.. image:: ../images/bonsai001.jpg
   :width: 0px


.. |a| raw:: html

   <!-- lightbox container hidden with CSS -->
   <a href="#" class="lightbox" id="img1">
   <span style="background-image: url('../../_images/bonsai001.jpg')"></span>
   </a>

.. |b| raw:: html

   <a href="#img1">
   <img width="250px" src="../../_images/bonsai001.jpg">
   </a>

.. sidebar:: Bonsai

   |b|

|a|


Lernziel
========
:Ende: | Die Aufgaben-Serie ist hier erst einmal beendet.
       | Damit sollten Sie die komplexe Aufgabe
       | der nächsten Station lösen können.

.. figure::  ../images/peter.png
   :height: 100px

:Zitat: | »Es ist nicht genug, zu wissen, man muß auch anwenden;
        | es ist nicht genug, zu wollen, man muß auch tun.«
        |
        | Johann Wolfgang von Goethe

Lösung zu den Aufgaben:
=======================
Geben Sie die Zeilen in der Tabelle, im Wechsel mit
unterschiedlichen Farben, aus (Zeilen 41-65).

.. code-block:: xml
   :linenos:

   <xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
   <xsl:output method="html"/>
   <xsl:template match="/">

    <html>
       <head>
         <title>Rechneruebersicht</title>
         <link rel="stylesheet" type="text/css" href="formate.css"/>
       </head>
       <body>
       <h2>Die Liste aller Rechner</h2>
       <!--[weitere Regeln abarbeiten] -->
       <xsl:apply-templates />
       </body>
     </html>

   </xsl:template>

   <xsl:template match="pcliste/raum">
     <h3>
       <xsl:value-of select="@bezeichnung"/>
     </h3>

     <table border="0" class="kopf">
       <tbody>
       <tr>
         <th>Nummer</th>
         <th>Status</th>
         <th>Anmerkung</th>
         <th>Rechner</th>
         <th>IP</th>
       </tr>
       <!--[weitere Regeln abarbeiten] -->
       <xsl:apply-templates />
       </tbody>
     </table>

   </xsl:template>


   <xsl:template match="rechner[position() mod 2 =1]">
       <xsl:call-template name="zeile"/>
   </xsl:template>

   <xsl:template match="rechner[position() mod 2 = 0]">
       <xsl:call-template name="zeile">
         <xsl:with-param name="klasse">gerade</xsl:with-param>
       </xsl:call-template>
    </xsl:template>

   <xsl:template name="zeile">
     <xsl:param name="klasse">ungerade</xsl:param>
     <tr  class="{$klasse}">
       <td><xsl:number format="01" count="rechner"/></td>
       <xsl:if  test="anmerkung/@status='Schulungrechner'">
       <td>Schulungsrechner</td>
       </xsl:if>
       <xsl:if test="anmerkung/@status!='Schulungrechner'">
       <td><xsl:value-of select="anmerkung/@status" /></td>
       </xsl:if>
       <td><xsl:value-of select="anmerkung"/></td>
       <td><xsl:value-of select="rechnername"/></td>
       <td><xsl:value-of select="rechnerip"/></td>
     </tr>
   </xsl:template>

   </xsl:stylesheet>

Kommentar zu den Zeilen 41-49
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Diese Varianten hat einen generellen Umbau erfahren und nutzt einige trickreiche 
Techniken. Fangen wir mit dem match-Attribut an. Wir wollen die Zeilen abwechselnd 
unterschiedlich einfärben.
Mathematisch gesehen, sind es gerade und ungerade Zahlen, wenn wir die Zeilen einer 
Tabelle durchnummerieren. Die Aussage ob eine Zahl gerade oder ungerade ist kann mit 
dem Modulo-Operator beantwortet werden und kennt für die Ziffer 2 nur zwei
Ergebnisse 0 oder 1. Diese Aussage nutzen wir hier und reagieren entsprechend.
Der erste Test prüft auf ungerade Zeilennummern. Wenn dies zutrifft rufen wir
eine Template-Rule, die den Namen *zeile* hat.
Weil sie diesen Namen bekommen hat, wird sie auch nur dann ausgeführt,
wenn sie mit dem Namen aufgerufen wird. Für den Aufruf gibt es auch eine spezielle 
XSL-Anweisung: *xsl:call-template*

Kommentar zu den Zeilen 51-65
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

An Hand des name-Attributes findet der Parser die richtige Template-Rule und
führt diese aus. Wenn der Aufruf ohne Parameter erfolgt, wird der Parameter
*klasse* auf den Wert *ungerade* gesetzt. Wird der Parameter mit dem Aufruf
übergeben (siehe call-template mit with-param), wird dieser Wert in *klasse*
eingesetzt. Einmal definiert, kann der Parameter mit  *{$klasse}* ausgelesen
werden und erscheint im  Ergebnis der Transformation.
Auf diese Art definieren wir zwei CSS-Klassen ( class="gerade" bzw. class="ungerade"). 
Was diese bei der Anzeige tun sollen definieren wir im letzten Schritt
in der Datei formate.css (Zeilen 41-46).


.. code-block:: xml
   :linenos:

   body
   {
     background-color: #ffffff;
     color: #000000;
     font-family:arial;
   }

   h2{
     color:#0000ff;
   }

   h3{
     color:#00ff00;
   }

   table {
      border-width:2px;
      border-style:solid;
      border-color:gray;
   }
   .kopf {
     background-color:green;}

   th {
     align:left;
   }

   td {
     border-width:2px;
     border-style:solid;
   }

   .wichtig{
     background-color:orange;
   }

   .schulungsrechner {
     background-color:lightgrey;
   }

   .gerade {
     background-color:lightgreen;
   }
   .ungerade {
     background-color:lightgrey;
   }

Damit ist auch diese Aufgabe gelöst.
