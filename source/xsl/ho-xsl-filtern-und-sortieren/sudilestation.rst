.. meta::

   :description lang=de: XML-Kurs mit den Themen: DTD, Schema, XSL, XPath
   :keywords: XSL, Template Rules, PC-Liste, OER

:ref:`« XSL-Start <xsl-start>`

.. index:: XSL; sort, XSL; Bedingungen

=====================
Filtern und Sortieren
=====================

.. image:: ../images/fernbedienungen.jpg
   :width: 0px

.. |a| raw:: html

   <!-- lightbox container hidden with CSS -->
   <a href="#" class="lightbox" id="img1">
   <span style="background-image: url('../../_images/fernbedienungen.jpg')"></span>
   </a>

.. |b| raw:: html

   <a href="#img1">
   <img width="250px" src="../../_images/fernbedienungen.jpg">
   </a>

.. sidebar:: Fernbedienungen

   |b|

   2013

|a|

Lernziel
========

In dieser Station lernen Sie, wie Sie Informationen filtern und
sortieren.


Handlungsanweisungen
====================

:Aufgaben:

   1. Geben Sie alle CDs sortiert nach dem Bandnamen aus.

.. index:: Filtern

XSL: Filtern
~~~~~~~~~~~~

Neben den Bedingungen, die Sie schon kennengelernt haben, können Sie
auch Informationen durch XPath, also durch die Pfadangaben,
filtern. Hier ein Beispiel:

.. code-block:: xml
   :linenos:

   <xsl:for-each select="person[postanschrift/wohnort='Berlin']">
   ...
   </xsl:for-each>

In diesem Fall werden nur solche Datensätze ausgewählt, die als
Wohnort „Berlin“ beinhalten.


.. index:: Sortieren

XSL: Sortieren
~~~~~~~~~~~~~~

Manchmal ist es nützlich, wenn Sie die Ausgabe nach den Inhalt eines
bestimmten Elementes sortieren. Die Syntax sieht so aus:

.. code-block:: xml
   :linenos:

   <xsl:sort select="irgendein_tag" order="ascending"/>

Der Befehl für das Sortieren lautet: xsl:sort. Durch das Attribut
*select*, wird das Element angegeben, nach dem Sortiert werden
soll. Das Attribut *order* steht für die Sortierreihenfolge.


Mögliche Attribut-Werte:
::

  ascending = Aufsteigend
  descending = Absteigend

Außerdem gibt es auch noch ein Attribut *case-order*, das die Optionen
::

  upper-first = Großbuchstaben zuerst
  lower-first = Kleinbuchstaben zuerst

**Beispiel:**

.. code-block:: xml
   :linenos:

   <table border="1">
   <xsl:for-each select="geburtstage/person">
     <xsl:sort select="gebdatum" order="ascending"/>
     <tr>
       <td><b><xsl:value-of select="name" /></b></td>
       <td><b><xsl:value-of select="gebdatum" /></b></td>
       <td><b><xsl:value-of select="@gebort" /></b></td>
     </tr>
    </xsl:for-each>
    </table>
