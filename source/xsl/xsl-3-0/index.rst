.. meta::

   :description lang=de: XML-Kurs mit den Themen: DTD, Schema, XSL, XPath
   :keywords: XSL 3.0, OER

:ref:`« XSL-Start <xsl-start>`

.. index:: XSL 3.0

==================
XSL 3.0 Video (en)
==================

.. image:: ../images/modellbausatz.jpg
   :width: 0px


.. |a| raw:: html

   <!-- lightbox container hidden with CSS -->
   <a href="#" class="lightbox" id="img1">
   <span style="background-image: url('../../_images/modellbausatz.jpg')"></span>
   </a>

.. |b| raw:: html

   <a href="#img1">
   <img width="250px" src="../../_images/modellbausatz.jpg">
   </a>

.. sidebar:: Modellbausatz

   |b|

|a|

Lernziel
========

Ein interessanter Vortrag in englischer Sprache, der einen Überblick
zur bisherigen Entwicklung und die Zukunft des XSL-Standards gibt.

Handlungsanweisungen
====================

:Aufgaben:  Anschauen und/oder Folien ansehen

   Interessante Folien finden finden sich an folgenden Stellen:

   - 3:12 Stand von XSLT 2.0
   - 6:49 Motivation
   - 7:31 Paketierung
   - 8:40 Paketierung
   - 10:35 Warum Pakete (wie groß sind diverse Projekte (LOC)
   - 13:48 Paket-Beispiel
   - 15:15 Streaming
   - 22:40 Demo (Streaming)
   - 28:50 itereate, accumulators, maps
   - 32:30 other goodies
   - 34:00 other goodies

.. raw:: html

   <iframe width="560" height="315"
           src="https://www.youtube.com/embed/182g7ql2DEM"
	   frameborder="0"
	   allowfullscreen>
   </iframe>


.. INDEX:: XSL; Funktioniert so nicht
.. INDEX:: Funktioniert so nicht; XSL


Funktioniert so nicht
=====================
Vieles ist möglich, aber nicht alles, kann umgesetzt werden.
Was mit XSL nicht möglich ist, listet die folgende Website auf:

https://www.delightfulcomputing.com/xslfaq/xsl/sect2/nono.html
