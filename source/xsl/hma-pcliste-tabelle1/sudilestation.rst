.. meta::

   :description lang=de: XML-Kurs mit den Themen: DTD, Schema, XSL, XPath
   :keywords: XSL, Template Rules, PC-Liste, OER

:ref:`« XSL-Start <xsl-start>`

.. index:: Template Rules; Übung PC-Liste II (Tabellen)

===========================
PC-Liste: Tabellen erzeugen
===========================

.. image:: ../images/bluete001.jpg
   :width: 0px


.. |a| raw:: html

   <!-- lightbox container hidden with CSS -->
   <a href="#" class="lightbox" id="img1">
   <span style="background-image: url('../../_images/bluete001.jpg')"></span>
   </a>

.. |b| raw:: html

   <a href="#img1">
   <img width="250px" src="../../_images/bluete001.jpg">
   </a>

.. sidebar:: Blüte

   |b|

|a|

Lernziel
========
Die folgenden Stationen dienen der Vertiefung und Ergänzung. Das
Abarbeiten ist all denen zu empfehlen, die noch unsicher und an einem
weiteren Beispiel interessiert sind. Am Ende der Aufgabenserie
erzeugen Sie aus der XML-Datei eine Tabelle, die etwa so aussieht:

.. image:: img/loesung-pc-liste.png

Sie lernen auch noch weitere XSL- und XPATH-Anweisungen kennen.

Handlungsanweisungen
====================
:Aufgaben: Wenn Sie die vier Aufgaben gelöst haben, besitzen sie eine erste Version. 
           Verbessern Sie nun die vorhandene Version und lösen Sie die folgenden 
           Aufgaben.
           Die Lösung dazu finden Sie wieder auf der nächsten Seite.

           1. Jede Tabelle soll als Überschrift die Raumbezeichnung enthalten.
           2. Platzieren Sie am Anfang eine Gesamtüberschrift.

.. image::  ../images/peter.png
   :height: 100px

:Zitat: »Reich wird man erst durch Dinge, die man nicht begehrt.« -- Mahatma Gandhi 

Lösung zur den vorherigen Aufgaben
==================================
:Lösungen: zu folgenden Aufgaben wird hier eine Schritt für Schritt Anleitung gezeigt:

  1. Geben Sie alle Rechner in Form einer Tabelle pro Raum aus.
  2. Das Ausgabeformat ist HTML, verwenden Sie XSL-Templates-Rules.
  3. Erstellen Sie dazu eine erste XSL-Datei und binden Sie diese in
     die XML-Datei ein.
  4. Validieren Sie die Dateien und geben Sie das fertige HTML in eine
     Datei pcliste.html aus.

Das Grundgerüst der XSL-Datei
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. code-block:: xml
   :linenos:


   <xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
   <xsl:output method="html"/>
   <xsl:template match="/">
     Hier kommt das Suchergebnis rein...
   </xsl:template>
   </xsl:stylesheet>


Das Attribut match "/" gibt das Wurzelelement zurück. Wenn es
existiert, können wir das HTML-Gerüst erzeugen (Zeile 3).

HTML-Grundgerüst erzeugen...
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. code-block:: xml
   :linenos:

   <xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
   <xsl:output method="html"/>
   <xsl:template match="/">

    <html>
       <head>
         <title>Rechneruebersicht</title>
       </head>
       <body>
        Hier geht es mit dem Inhalt weiter...
       </body>
     </html>

   </xsl:template>
   </xsl:stylesheet>

Wenn wir dieses minimalistische Stylesheet auf die XML-Datei anwenden,
bekommen wir eine fast leere HTML-Seite (Zeilen 5-12).

Erste Transformation
~~~~~~~~~~~~~~~~~~~~
::

   java  -jar saxon9.jar   pcliste.xml pcliste3.xsl

Beispiel für den Aufruf des javabasierten Parser *saxon*. Passen Sie die
Pfade und Namen an Ihre Gegebenheiten an oder verwenden Sie einen
Parser Ihrer Wahl.

XSL einbinden
~~~~~~~~~~~~~

.. code-block:: xml
   :linenos:

   <xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
   <?xml-stylesheet href="pcliste3.xsl" type="text/xsl" ?>
   <xsl:output method="html"/>
   <xsl:template match="/">

    <html>
       <head>
         <title>Rechneruebersicht</title>
       </head>
       <body>
        Hier geht es mit dem Inhalt weiter...
       </body>
     </html>

   </xsl:template>
   </xsl:stylesheet>

Für die direkte Betrachtung im Browser muß die XML-Datei mit der
XSL-Datei verknüpft werden. Laden Sie jetzt die XML-Datei in den
Browser (Zeile 2).


Ein Element "raum" suchen
~~~~~~~~~~~~~~~~~~~~~~~~~

.. code-block:: xml
   :linenos:

   <xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
   <xsl:output method="html"/>
   <xsl:template match="/">

    <html>
       <head>
         <title>Rechneruebersicht</title>
       </head>
       <body>
       <!--[weitere Regeln abarbeiten] -->
       <xsl:apply-templates />
       </body>
     </html>

   </xsl:template>

   <xsl:template match="pcliste/raum">
      <p>Das ist eine neue Regel die nur zutrifft,
          wenn der XPATH-Ausdruck "pcliste/raum" gefunden wird.
      </p>
   </xsl:template>

   </xsl:stylesheet>

Wenn keine spezielle Regel genannt wird, werden alle Regeln
geprüft. Wenn die Regel zutrifft (XPATH im match-Attribut), wird mit
den Anweisungen in der Template-Rule weitergemacht. Wir geben hier zu
Testzwecken einen Satz aus und sollten sehen, das die Regel vier mal
zutrifft. Laden Sie zu diesem Zweck die Datei in den Browser
(Zeilen 10, 11, 17-21).

Tabellenkopf
~~~~~~~~~~~~

.. code-block:: xml
   :linenos:

   <xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
   <xsl:output method="html"/>
   <xsl:template match="/">

    <html>
       <head>
         <title>Rechneruebersicht</title>
       </head>
       <body>
       <!--[weitere Regeln abarbeiten] -->
       <xsl:apply-templates />
       </body>
     </html>

   </xsl:template>

   <xsl:template match="pcliste/raum">

     <h2>Raum</h2>
     <table border="1">
       <tbody>
       <tr>
         <th>Anmerkung</th>
         <th>Rechner</th>
         <th>IP</th>
       </tr>
       <!--[weitere Regeln abarbeiten] -->
       <!-- xsl:apply-templates /-->
       </tbody>
     </table>

   </xsl:template>

   </xsl:stylesheet>

Wir geben jetzt statt eines Satzes eine Überschrift, gefolgt von einem
Tabellenkopf aus. Testen Sie diese erweiterte Version (Zeilen 17-32).


Tabellezeile für jedes Element "rechner"
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
.. code-block:: xml
   :linenos:

   <xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
   <xsl:output method="html"/>
   <xsl:template match="/">

    <html>
       <head>
         <title>Rechneruebersicht</title>
       </head>
       <body>
       <!--[weitere Regeln abarbeiten] -->
       <xsl:apply-templates />
       </body>
     </html>

   </xsl:template>

   <xsl:template match="pcliste/raum">

     <h2>Raum</h2>
     <table border="1">
       <tbody>
       <tr>
         <th>Anmerkung</th>
         <th>Rechner</th>
         <th>IP</th>
       </tr>
       <!--[weitere Regeln abarbeiten] -->
       <xsl:apply-templates />
       </tbody>
     </table>

   </xsl:template>

   <xsl:template match="rechner">
       <tr>
         <!-- xsl:apply-templates /-->
       </tr>
   </xsl:template>

   </xsl:stylesheet>

Immer wenn der Parser das Element *rechner* findet, soll eine
HTML-Tabellenzeile erzeugt werden. In der Tabellenzeile rufen wir
weitere Regeln ab. Prüfen Sie auch diese Version (Zeilen 34-38).

Zugriff auf den Inhalt
~~~~~~~~~~~~~~~~~~~~~~
.. code-block:: xml
   :linenos:

   <xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
   <xsl:output method="html"/>
   <xsl:template match="/">

    <html>
       <head>
         <title>Rechneruebersicht</title>
       </head>
       <body>
       <!--[weitere Regeln abarbeiten] -->
       <xsl:apply-templates />
       </body>
     </html>

   </xsl:template>

   <xsl:template match="pcliste/raum">

     <h2>Raum</h2>
     <table border="1">
       <tbody>
       <tr>
         <th>Anmerkung</th>
         <th>Rechner</th>
         <th>IP</th>
       </tr>
       <!--[weitere Regeln abarbeiten] -->
       <xsl:apply-templates />
       </tbody>
     </table>

   </xsl:template>

   <xsl:template match="rechner">
     <tr>
       <xsl:apply-templates />
     </tr>
   </xsl:template>

   <xsl:template match="anmerkung | rechnerip | rechnername">
     <td>
       <!--[Zugriff auf den Element-Inhalt] -->
       <xsl:value-of select="." />
     </td>
   </xsl:template>
   </xsl:stylesheet>

Wenn es sich um die Elemente *anmerkung*, *rechnerip* oder
*rechnername* handelt, dann erzeugen wir ein Spalten-Element (td) und
platzieren darin den Inhalt der gefundenen Elemente. Hier bricht die
Abarbeitung ab, weil keine weiteren Regeln angewendet werden (Zeilen 40-45).

Damit ist die erste Fassung unserer Raumverwaltung komplett.
