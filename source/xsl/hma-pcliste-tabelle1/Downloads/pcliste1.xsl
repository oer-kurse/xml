<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
<xsl:output method="html"/>
<xsl:template match="/">
  
 <html>
    <head>
      <title>Rechneruebersicht</title>
    </head>
    <body>
    <!--[weitere Regeln abarbeiten] -->
    <xsl:apply-templates />
    </body>
  </html>

</xsl:template>

<xsl:template match="pcliste/raum">
   Das ist eine neue Regel, die nur zutrifft wenn der XPATH-Ausdruck "pcliste/raum" gefunden wird.
</xsl:template>

</xsl:stylesheet>