<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
<xsl:output method="html"/>
<xsl:template match="/">
  
 <html>
    <head>
      <title>Rechneruebersicht</title>
    </head>
    <body>
    <h2>Die Liste aller Rechner</h2>
    <!--[weitere Regeln abarbeiten] -->
    <xsl:apply-templates />
    </body>
  </html>

</xsl:template>

<xsl:template match="pcliste/raum">

  <h2>Raum</h2>
  <table border="1">
    <tbody>
    <tr>
      <th>Anmerkung</th>
      <th>Rechner</th>
      <th>IP</th>
    </tr>
    <!--[weitere Regeln abarbeiten] -->
    <xsl:apply-templates />
    </tbody>
  </table>

</xsl:template>

<xsl:template match="rechner">
    <tr>
      <xsl:apply-templates />
    </tr>
</xsl:template>

<xsl:template match="anmerkung | rechnerip | rechnername">
      <td>
        <!--[Zugriff auf den Element-Inhalt] -->
	<xsl:value-of select="." />
      </td>
</xsl:template>

</xsl:stylesheet>