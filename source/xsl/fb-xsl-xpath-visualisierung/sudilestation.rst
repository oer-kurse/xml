.. meta::

   :description lang=de: XML-Kurs mit den Themen: DTD, Schema, XSL, XPath
   :keywords: XSL, XPath, OER

:ref:`« XSL-Start <xsl-start>`

.. index:: XPath, XPath (Beispielsammlung)

=====
XPath
=====

.. image:: ../images/nachbar.jpg
   :width: 0px


.. |a| raw:: html

   <!-- lightbox container hidden with CSS -->
   <a href="#" class="lightbox" id="img1">
   <span style="background-image: url('../../_images/nachbar.jpg')"></span>
   </a>

.. |b| raw:: html

   <a href="#img1">
   <img width="250px" src="../../_images/nachbar.jpg">
   </a>

.. sidebar:: Schilder

   |b|

|a|


Lernziel
========

Sie lernen den XPath-Ausdrücke kennen, mit dem Sie Knoten auswählen können.
Dies ist die Voraussetzung für die Anwendung von XSL-Transformationen.

Handlungsanweisungen
~~~~~~~~~~~~~~~~~~~~

:Aufgaben:



  1. Verwenden Sie die folgende Beispiel-Datei:

     .. index:: Download; pcliste.xml

     :download:`Download: Inventurliste <files/pcliste.xml>`

     und entwickeln Sie für jedes Muster, das unter:

     `22 Beispiele für XPATH-Ausdrücke
     <http://www.zvon.org/xxl/XPathTutorial/General_ger/examples.html>`_

     dokumentiert ist, mit den konkreten Daten der Inventurliste eine eigene Variante.

  2. Rufen Sie eine der folgenden Websites auf:

     `http://xpather.com <http://xpather.com>`_

     Und prüfen Sie die XPath-Beispiele!

     :Vortragsvariante:

	- Der Dozent verteilt die 22 Varianten (
	  `22 Beispiele für XPATH-Ausdrücke
	  <http://www.zvon.org/xxl/XPathTutorial/General_ger/examples.html>`_
	  ) auf die Teilnehmer.
        - Diese wenden die Beispiele auf die Inventurliste an.
        - Die Teilnehmer zeigen/demonstrieren die Ergebnisse/Erkenntisse in einem 
          kleinen Vortrag. Alles Diskutieren die Ergebnisse und vergleichen mit
	  Varianten und anderen schon bekannten Regeln.

  3) Laden Sie die in der letzten Station erstellte XML-Datei
     in den Xpath-Visualizer.

     b) Selektieren Sie mit einem XPath-Ausdruck alle Adressen Ihrer XML-Datei.
     c) Wenden Sie einen XPath-Ausdruck an, um den Nachnamen aus der
        letzten Adresse Ihres Datensatzes zurückzubekommen.

  4) Für diese und alles anderen Übungen und Demos können Sie auch
     den Parser »Saxon« verwenden, :ref:`wie hier beschieben  <saxonhe>`.
	
Was ist XPath?
~~~~~~~~~~~~~~
Die XML Path Language (XPath) ist eine vom W3-Konsortium entwickelte
Abfragesprache, um Teile eines XML-Dokumentes zu adressieren und
auszuwerten. XPath dient als Grundlage einer Reihe weiterer Standards
wie XSLT, XPointer und XQuery.
XPath ist derzeit in der Version 3.1 vom 21. März 2017 standardisiert. 

:Quelle: https://de.wikipedia.org/wiki/XPath

Weitere Beispiele und Übersichten finden Sie auf den weiter oben
genannten Seiten wie z.B. Wikipedia, zvon.org, w3schools.com
bzw. in den Dokumenten zum Standard unter https://www.w3.org/TR/xpath-3/
