.. meta::

   :description lang=de: XML-Kurs mit den Themen: DTD, Schema, XSL, XPath
   :keywords: XSL, Template Rules, JSON, OER

:ref:`« XSL-Start <xsl-start>`

.. index:: JSON-Datei generieren, JSON

================
Ausgabe als JSON
================

.. image:: ../images/caputh_bank.jpg
   :width: 0px


.. |a| raw:: html

   <!-- lightbox container hidden with CSS -->
   <a href="#" class="lightbox" id="img1">
   <span style="background-image: url('../../_images/caputh_bank.jpg')"></span>
   </a>

.. |b| raw:: html

   <a href="#img1">
   <img width="250px" src="../../_images/caputh_bank.jpg">
   </a>

.. sidebar:: Bank

   |b|

   in Caputh

|a|

Lernziel
========

Bisher haben wir HTML als Zielformat gewählt. Hier nun ein Beispiel
wie sie die Daten einer XML-Datei in eine JSON-Struktur konvertieren.

Handlungsanweisungen
====================

:Aufgaben:

Entwickeln Sie bitte eine XSL-Datei, die eine JSON-Datei generieren soll.
Benutzen Sie bitte saxon, so dass Sie die JSON-Datei ohne den im Browser
integriertem Transformators erstellen. Laden Sie dann die JSON-Datei in
einen `Online-Validator <JSON Validator>`_, um die Struktur auf Gültigkeit
zu prüfen.

Weitere Hinweise finden Sie unten in dieser Station. Benutzen Sie
folgende XML-Datei.

.. code-block:: xml

    <telefon>
      <eintrag>
        <nummer>0331-2345322</nummer>
        <name>Heinz Becker</name>
        <info>Chef</info>
      </eintrag>
      <eintrag>
        <nummer>0331-2345324</nummer>
        <name>Steffi Acht</name>
        <info>Admin</info>
      </eintrag>
    </telefon>


JSON-Dateien erstellen
~~~~~~~~~~~~~~~~~~~~~~

Um JSON-Strukturen zu erstellen, müssen Sie folgenden Hinweise berücksichtigen:

**Output-Methode setzen**
::

  <xsl:output method="text" />

Wir müssen die Output-Methode von XSL-Dateien auf *text* setzen.
Die Zeile müssen Sie vor das erste Template setzen.



Eine erste Version  mit der Sie beginnen können, realisiert die
Ausgabe für die PC-Liste. Beachen Sie, dass auch die DTD zur XML
vorhanden ist. Was passiert bei der Transformation, wenn Sie die
Verbindung zur DTD, in der XML-Datei deaktivieren?

.. code-block:: xml
   :linenos:

   <?xml version="1.0" encoding="UTF-8"?>
   <xsl:stylesheet version="2.0"
        xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

     <xsl:output method="text"/>
     <xsl:template match="pcliste">
     {
       <xsl:apply-templates />
     }
   </xsl:template>

   <xsl:template match="raum">
     "<xsl:value-of select="./@bezeichnung"/>":
     [<xsl:apply-templates select="rechner"/>]
     <xsl:if test="position() != last()">,</xsl:if>
   </xsl:template>

   <xsl:template match="rechner">
     [
       "<xsl:value-of select="rechnerip"/>",
       "<xsl:value-of select="rechnername"/>",
       "<xsl:value-of select="anmerkung"/>"
    ]
     <xsl:if test="position() != last()">,</xsl:if>
   </xsl:template>
   </xsl:stylesheet>


.. _JSON Validator: https://jsonformatter.curiousconcept.com
