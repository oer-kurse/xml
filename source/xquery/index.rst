.. meta::
    
   :description lang=de: XML-Kurs mit den Themen: DTD, Schema, XSL, XPath
   :keywords: XSL, OER

:ref:`« Kursstart <xml-kurs-start>`

.. _xquery-start:

.. index:: XQuery; Übersicht

.. image:: ./werbung/ccc2014.jpg
   :width: 0px

.. |a| raw:: html

       <div class='hover_img'>
         <a href='#'>Caos Computer Club (CCC)
         <span>
           <img src='../_images/ccc2014.jpg'
                alt='Caos Computer Club' />
          </span>
         </a>
        </div>

.. sidebar:: XML-Kurs

	     | Statt der Werbung...
	     | |a|
	     | Serie: Kunst

======
XQuery
======

XQuery dient wie XSL(T) der Transformation von XML in andere
Ausgabeformate z.B. HTML, SQL, CSV usw. Beide nutzen XPATH und stehen
im Wettbewerb zueinander bzw. wurden parallel entwickelt bzw. lassen
sich wahlweise einsetzen. Was in diesem Kursmaterial bisher mit
XPATH + XSL realisiert worden ist, wird hier noch einmal mit XPATH +
XQUERY wiederholt.

.. toctree::
   :maxdepth: 1
   :glob:
      
   *
