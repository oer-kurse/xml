.. meta::
    
   :description lang=de: XML-Kurs mit den Themen: DTD, Schema, XSL, XPath
   :keywords: XSL, Template Rules, OER, XQuery


:ref:`« XQuery-Start <xquery-start>`

.. index:: XQuery; Spruechesammlung

===============================
XQuery: Funktion/Aufruf/Ausgabe
===============================

.. image:: ./werbung/tuerklinke.jpg
   :width: 0px

.. |a| raw:: html

       <div class='hover_img'>
         <a href='#'>Türklinke ...
         <span>
           <img src='../_images/tuerklinke.jpg'
                alt='Türklinke im Museum Chemnitz' />
          </span>
         </a>
        </div>

.. sidebar:: XML-Kurs

	     | Statt der Werbung...
	     | |a|
	     | Serie: Dinge


Lernziel
========

Einfache Beispiele, eine kleine Sammlung.

XQuery
======

Kommentare werden mit »(:« begonnen und mit »:)« abgeschlossen.

Beispiele
=========


      
XQuery-Funktion
===============

.. code-block:: xml
   :linenos:

    declare namespace local="local";

    declare function local:minPrice($p as xs:decimal?, $d as xs:decimal?) as xs:decimal? {
      let $disc := ($p * $d) div 100
      return ($p - $disc)
    };

    (:Do something...:)

    <ergebnis>

    { local:minPrice(10,10)}

    </ergebnis>

      
Transformation mit saxon
========================
Falls noch nicht installiert, finden Sie unter :ref:`Werkzeuge Saxon <saxonhe>`
eine Anleitung.

::

   java -cp saxon9he.jar net.sf.saxon.Query -t -q:beispiel1.xql


Das Ergebnis
============

::

   <?xml version="1.0" encoding="UTF-8"?>
   <ergebnis>9</ergebnis>
