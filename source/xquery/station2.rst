.. meta::
    
   :description lang=de: XML-Kurs mit den Themen: DTD, Schema, XSL, XPath
   :keywords: XSL, Template Rules, OER, XQuery


:ref:`« XQuery-Start <xquery-start>`

.. index:: XQuery; Spruechesammlung

=========================
Beispiel: Sprüchesammlung
=========================

.. image:: ./werbung/glas.jpg
   :width: 0px

.. |a| raw:: html

       <div class='hover_img'>
         <a href='#'>Glasbild ...
         <span>
           <img src='../_images/glas.jpg'
                alt='Kustwerk im Museum Chemnitz' />
          </span>
         </a>
        </div>

.. sidebar:: XML-Kurs

	     | Statt der Werbung...
	     | |a|
	     | Serie: Kunst


Lernziel
========

Mit XQuery-Syntax,wie mit XSLT kann das gleiche Ergebnis prodzuiert werden.
Vergleichen Sie die Lösungsansätze hier, mit den Template-Rule Beispielen.

XQuery
======

Kommentare werden mit »(:« begonnen und mit »:)« abgeschlossen.

Beispiele
=========

:Ziel:

1. Transformation der XML-Struktur zu HTML.

Ausgangspunkt ist die folgende XML-Datei: 

.. code-block:: xml
   :linenos:

   <?xml version="1.0" encoding="UTF-8"?>
   <!DOCTYPE spruechesammlung [
   <!ELEMENT spruechesammlung (spruch)+>
   <!ELEMENT spruch (kategorie, text, autor, gelebt-von-bis)>
   <!ELEMENT kategorie (#PCDATA)>
   <!ELEMENT text (#PCDATA)>
   <!ELEMENT autor (#PCDATA)>
   <!ELEMENT gelebt-von-bis (#PCDATA)>
   ]>
   <spruechesammlung>
   <!--erster Datensatz-->
   <spruch>
     <kategorie>Erziehung</kategorie>
     <text>
       Bester Beweis einer guten Erziehung ist die Pünktlichkeit.
     </text>
     <autor>Gotthold Ephraim Lessing </autor>
     <gelebt-von-bis>1729-1781</gelebt-von-bis>
   </spruch>
   <!-- zweiter Datensatz-->
   <spruch>
     <kategorie>Ordnung</kategorie>
     <text>
       Bewahre deine Papiere, deine Schlüssel und alles so,
       daß du jedes einzelne Stück auch noch im Dunkeln finden
       kannst. Verfahre noch ordentlicher mit fremden Sachen.
     </text>
     <autor>Adolph Freiherr von Knigge</autor>
     <gelebt-von-bis>1752-1796</gelebt-von-bis>
   </spruch>
   <!-- dritter Datensatz-->
   <spruch>
     <kategorie>Liebe</kategorie>
     <text>
       Der Unterschied zwischen einer Liaison und der
       ewigen Liebe besteht darin, daß die Liaison im
       allgemeinen länger dauert.
     </text>
     <autor>Karl Schönböck</autor>
     <gelebt-von-bis>1909-2001</gelebt-von-bis>
   </spruch>
   </spruechesammlung>
      
XQuery-Abfrage
==============

Schauen Sie sich zunächst die XQuery-Beispiele an.
Die Erklärungen folgen nach dem Code-Beispiel. 

.. code-block:: xml
   :linenos:

   xquery version "1.0";
   (:  java -cp saxon9he.jar net.sf.saxon.Query -t -q:spruechesammlung.xq :)
   <html>
   <head><title>Spruechesammlung</title></head>
   <body>
   {
     let $doc := doc("/Users/peter/labs/xslt/sprueche.xml")
     for  $x in $doc//spruch 
     return 

     <div>
       <p style="font-size:18pt;color:#444444;">
         {data($x/kategorie)}</p>
       <p style="font-size:12pt;color:green;">
         {data($x/text)}</p>
       <p style="font-size:12pt;color:orange;padding-left:20px;">
         {data($x/autor)}</p>
       <p style="font-size:12pt;color:black;padding-left:50px;">
         {data($x/gelebt-von-bis)}</p>
     </div>
   }
   </body>
   </html>

      
Transformation mit saxon
========================
Falls noch nicht installiert, finden Sie unter :ref:`Werkzeuge Saxon <saxonhe>`
eine Anleitung.

::

   java -cp saxon9he.jar net.sf.saxon.Query -t -q:spruechesammlung.xq

   # oder mit Ausgabeumlenkung...
   
   java -cp saxon9he.jar net.sf.saxon.Query -t -q:spruechesammlung.xq > test.html
   


Das Ergebnis
============

:Siehe auch: :ref:`Aufgabe zur XSL-Transformation <xsl-spruechesammlung-transformation>`


::

  <?xml version="1.0" encoding="UTF-8"?>
  <html>
  <head>
    <title>Spruechesammlung</title>
  </head>
  <body>
    <div>
      <p style="font-size:18pt;color:#444444;">Erziehung</p>
      <p style="font-size:12pt;color:green;">
        Bester Beweis einer guten Erziehung ist die Pünktlichkeit.
    </p>
      <p style="font-size:12pt;color:orange;padding-left:20px;">
        Gotthold Ephraim Lessing
      </p>
      <p style="font-size:12pt;color:black;padding-left:50px;">
        1729-1781
      </p>
    </div>
    <div>
      <p style="font-size:18pt;color:#444444;">Ordnung</p>
      <p style="font-size:12pt;color:green;">
        Bewahre deine Papiere, deine Schlüssel und alles so,
        daß du jedes einzelne Stück auch noch im Dunkeln finden
        kannst. Verfahre noch ordentlicher mit fremden Sachen.
    </p>
      <p style="font-size:12pt;color:orange;padding-left:20px;">
        Adolph Freiherr von Knigge
      </p>
      <p style="font-size:12pt;color:black;padding-left:50px;">
        1752-1796
      </p>
    </div>
    <div>
      <p style="font-size:18pt;color:#444444;">Liebe</p>
      <p style="font-size:12pt;color:green;">
        Der Unterschied zwischen einer Liaison und der
        ewigen Liebe besteht darin, daß die Liaison im
        allgemeinen länger dauert.
    </p>
      <p style="font-size:12pt;color:orange;padding-left:20px;">
        Karl Schönböck
      </p>
      <p style="font-size:12pt;color:black;padding-left:50px;">
        1909-2001
      </p>
    </div>
  </body>
  </html>
