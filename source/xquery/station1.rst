.. meta::
    
   :description lang=de: XML-Kurs mit den Themen: DTD, Schema, XSL, XPath
   :keywords: XSL, Template Rules, OER, XQuery


:ref:`« XQuery-Start <xquery-start>`

.. index:: XQuery; Adressabfrage

====================
 Beispiel: Adressen
====================

.. image:: ./werbung/traum-in-rosa.jpg
   :width: 0px

.. |a| raw:: html

       <div class='hover_img'>
         <a href='#'>Rosa ...
         <span>
           <img src='../_images/traum-in-rosa.jpg'
                alt='Traum in Rosa' />
          </span>
         </a>
        </div>

.. sidebar:: XML-Kurs

	     | Statt der Werbung...
	     | |a|
	     | Serie: Kitsch


Lernziel
========

Mit  XQuery-Syntax kann das gleiche Ergebnis erzielt werden,
wie mit XSLT. Vergleichen Sie die Lösungsansätze hier, mit den
Template-Rule Beispielen.


Handlungsanweisungen
====================

:Aufgaben:
   
Geben Sie den Inhalt aller Elemente der folgenden XML-Datei 
durch XQuery aus:

.. code-block:: xml
   :linenos:

   <adressen_db>
     <adresse nr="1">
       <name>Hubert Taler</name>
       <anschrift>
         <strasse>Weinberg 1</strasse>
         <plz>14777</plz>
         <ort>Berlin</ort>
       </anschrift>
     </adresse>
     <adresse nr="2">
       <name>Uni Potsdam</name>
       <anschrift>
         <strasse>Am Park Babelsberg 3</strasse>
         <plz>14482</plz>
         <ort>Potsdam</ort>
       </anschrift>
     </adresse>
   </adressen_db>




XQuery
======

XQuery orientiert sich mehr in Richtung einer klassischen
Programmiersprache und der Datenbankabfrage-Sprache SQL.

Es gilt das »Flower«-Prinzip: FLWOR

Damit sind die folgenden Schlüsselworte gemeint:

- for
- let
- where
- order
- return


Beispiele
=========

:Ziel:

1. Ausgabe aller Vor- und Nachnamen.

Ausgangspunkt ist die folgende XML-Datei: 

.. code-block:: xml
   :linenos:

   <?xml version="1.0" encoding="UTF-8"?>
   <?xml-stylesheet type="text/xsl" href="adressen.xsl"?>
   <adresse>
     <person>
       <anrede>Herr</anrede>
       <name>
         <nachname>Becker</nachname>
         <vorname>Heinz</vorname>
       </name>
       <strasse>Schabenstr. 4</strasse>
       <postanschrift>
         <plz>14489</plz>
         <wohnort>Berlin</wohnort>
       </postanschrift>
       <email>becker@heinz.de</email>
     </person>
     <person>
       <anrede>Frau</anrede>
       <name>
         <nachname>Rennt</nachname>
         <vorname>Lola</vorname>
       </name>
       <strasse>Laufsteig 17</strasse>
       <postanschrift>
         <plz>14888</plz>
         <wohnort>Potsdam</wohnort>
       </postanschrift>
       <email>lolo@lola.de</email>
     </person>
   </adresse>


XQuery-Abfrage
==============

Schauen Sie sich zunächst die XQuery-Beispiele an.
Die Erklärungen folgen nach dem Code-Beispiel. 

.. code-block:: xml
   :linenos:

   xquery version "1.0";

   let $doc := doc("/Users/peter/labs/xslt/adressen.xml")
   for  $x in $doc//name
   return ($x/vorname/text(), $x/nachname/text())

oder alternativ

.. code-block:: xml
   :linenos:

   xquery version "1.0";

   let $doc := doc("/Users/peter/labs/xslt/adressen.xml")
   for  $x in $doc//name 
   return (data($x/vorname), data($x/nachname))

- Die Angabe der Version ist optional. 
- Mit let wird eine Variable deklariert.
- Variablen haben das das $-Zeichen als Prefix.
- Der Zugriff auf Werte und Elemente erfolgt über XPath-Ausdrücke.
- mit return werden die Ergebnisse zurückgegeben.

Transformation mit saxon
========================
Falls noch nicht installiert, finden Sie unter :ref:`Werkzeuge Saxon <saxonhe>`
eine Anleitung.

::

   java -cp saxon9he.jar net.sf.saxon.Query -t -q:adressen.xq

   # oder mit Ausgabeumlenkung...
   
   java -cp saxon9he.jar net.sf.saxon.Query -t -q:adressen.xq >test.html
   

Transformation mit HTML-Ausgabe
===============================
:siehe auch die:  :ref:`XSL-Transvormation <xsl-adressen-transformation>`


::


   xquery version "1.0";

   <html>
   <head><title>Namen mit XQuery</title></head>
   <body>
     <table>
     {
       let $doc := doc("C:/home/xml/xquery/adressen.xml")
       let $start := "asdfasdf"
       for  $x in $doc//name 
       return 
       
       <tr>
         <td>{data($x/vorname)}</td>
	 <td>{data($x/nachname)}</td>
       </tr>

     }
     </table>
   </body>
   </html>

Das Ergebnis
============

::

   <?xml version="1.0" encoding="UTF-8"?>
   <html>
   <head>
      <title>Namen mit XQuery</title>
   </head>
   <body>
      <table>
         <tr>
            <td>Heinz</td>
            <td>Becker</td>
         </tr>
         <tr>
            <td>Lola</td>
            <td>Rennt</td>
         </tr>
      </table>
   </body>
   </html>

Frage
=====

Wie kann die Ausgabe der ersten Zeile, die XML-Deklaration unterdrückt werden?


Antwort
=======
Durch die Verwendung zusätzlicher »serialization parameters«, siehe auch:

https://www.saxonica.com/html/documentation/extensions/output-extras/

Nachfolgend das verbesserte XQuery:

::

   xquery version "1.0";

   declare namespace saxon="http://saxon.sf.net/"; 
   declare option saxon:output "indent=yes"; 
   declare option saxon:output "omit-xml-declaration=yes";

   <html>
   <head><title>Namen mit XQuery</title></head>
   <body>
     <table>
     {
       let $doc := doc("C:/home/xml/xquery/adressen.xml")
       let $start := "asdfasdf"
       for  $x in $doc//name 
       return 
       
       <tr>
         <td>{data($x/vorname)}</td>
	 <td>{data($x/nachname)}</td>
       </tr>

     }
     </table>
   </body>
   </html>

Die Parameter gelten nur für den saxon-Parser (saxon9he.jar), andere Parser verwenden eventuell eine andere Syntax, z.B. ignoriert der im Editor »editix«  eingebaute Parser, die obigen output-Optionen. Über die Konfiguration kann der interne Parser durch einen anderen, z. B. den saxon-Parserersetzt werden (siehe Menü: Options|Install|A custom transformer).


