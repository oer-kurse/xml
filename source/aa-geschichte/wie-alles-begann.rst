.. meta::
   
   :description lang=de: XML-Kurs mit den Themen: DTD, Schema, XSL, XPath
   :keywords: XML Geschichte, Markup, GML, HTML, XSL, OER, DTD, Schema, XPath


:ref:`« Kursstart <xml-kurs-start>`
   
==================
 Wie alles begann
==================

.. image:: ./images/indianer.jpg
   :width: 0px


.. |a| raw:: html

   <!-- lightbox container hidden with CSS -->
   <a href="#" class="lightbox" id="img1"
      alt='Kendall Old Elk' >
   <span style="background-image: url('../_images/indianer.jpg')"></span>
   </a>

.. |b| raw:: html

   <a href="#img1">
   <img width="250px" src="../_images/indianer.jpg">
   </a>



.. sidebar:: XML-Kurs

   |b|

   | Kendall Old Elk
   | `Eldorado Templin <http://www.eldorado-templin.de/Stuntcrew--Indianer>`_

|a|

	     
HTML ist das Ergebnis einer Entwicklung, mit deren Ergebnis inzwischen
jeder schon einmal zu tun hatte. Was vorher geschah und uns noch
erwarten wird, erfahren Sie hier in einer gerafften Version.

.. index:: Auszeichnungssprachen
	   
Was ist eine Auszeichnungssprache?
==================================

1. Muß ich als HTML-Anwender XML beherrschen?
2. Besuchen Sie die Seiten des W3C und ermitteln Sie 3 wichtige XML-Standards. 
3. Welche Art von Aufgaben werden damit gelöst?

.. index:: Auszeichnungssprachen; SGML
   
SGML
====
   
SGML ist die Mutter aller Markup-Sprachen. Was ist nun eine Markup-Sprache?
Diese Frage wird auf Wikipedia wie folgt beantwortet:

::
   
   Eine Auszeichnungssprache (engl. Markup Language, Abk. ML)
   dient zur Beschreibung der Daten und teilweise des Verfahrens,
   das zur Bearbeitung dieser Daten nötig ist. Ursprünglich
   dienten die Auszeichnungen im Text als Anweisungen für die
   Setzer im Drucksatz.

   Bei einer Auszeichnungssprache werden die Eigenschaften,
   Zugehörigkeiten und Verfahren von bestimmten Wörtern, Sätzen
   und Abschnitten (Elementen) eines Textes oder einer Datenmenge
   beschrieben bzw. zugeteilt, meist indem sie mit Tags markiert
   werden ...
   
Quelle: http://de.wikipedia.org/wiki/Auszeichnungssprache

.. image:: ./img/sgml.png

.. index:: Auszeichnungssprachen; HTML
	   
	      
HTML
====
   
Ist aus der Sicht des SGML-Standard eine Dokumentenklasse und
hat den Vorteil, vom Umfang des Regelwerkes betrachtet, im
Vergleich zu SGML recht klein zu sein. Diese Übersichtlichkeit
und Einfachheit ist der Hauptgrund für den Erfolg dieser
Markupsprache.
   
.. image:: ./img/sgml-html.png


.. index:: Auszeichnungssprachen;XML
	   
XML
===
   
Ist der Nachfolger des SGML mit ebenfalls vereinfachtem
Regelwerk. XML selbst ist einfacher und vom Umfang her kleiner
als HTML, dies wird aber wieder durch die Modularisierung
relativiert, die dann wieder eine ganze Sprachfamilie
umfaßt. XML ist wie SGML eine Meta-Sprache. Wichtig ist die
Möglichkeit, Daten im XML-Format vollautomatisch verarbeiten zu
können.

.. image:: img/xml.png      

.. index:: Auszeichnungssprachen; XHTML
	   
XHTML
=====
   
Ein wesentlicher Nachteil von HTML ist die Fehlertoleranz, die
in den Browsern eingebaut ist und eine korrekte Wiedergabe der
Inhalte wie man es vom Druck her kennt, fast unmöglich und die
automatische Verarbeitung durch Programme sehr fehleranfällig
und schwierig ist.

Deshalb entschloß man sich, einen Schritt in Richtung XML zu gehen.

.. image:: img/xhtml.png      

JavaScript und CSS
==================
   
Es handelt sich um zwei Technologien die HTML
ergänzen und die Pflege von HTML-Seiten
erleichtern bzw. dynamische Prozesse bei der
Darstellung und Verarbeitung von Daten möglich
machen.

.. image:: img/css-und-js.png      	       

.. index:: Auszeichnungssprachen; HTML 5
	   
HTML5
=====
   
Ist der neueste Schritt in der Entwicklung und weicht von den
Prinzipien des XML wieder etwas ab. 

.. image:: img/html5.png    	       

.. index:: Auszeichnungssprachen;  Namen und Jahres-Zahlen
	   
Wichtige Daten in der Zeitachse
===============================
	   
.. list-table:: Geschichte der Markupsprachen
   :widths: 15 10 15 30 
   :header-rows: 1

   * - Namen
     - Jahr
     - Bezeichnung
     - Anmerkung

   * - Vannevar Bush
     - 1945
     - Memex
     -
       
   * - Ted Nelson
     - 1950
     - 
     - Hypertext, erstmals als Theorie entwickelt

   * - Douglas Egelbart
     - 1962
     - Augment
     -

   * - Ted Nelson
     - 1965
     - Xanadu
     - 

   * - Wiliam Tunniclifle
     - 1967
     - GCA
     - generic coding

   * - Stanley Rice
     -
     -
     - editorial sturcture tags

   * - Norman Scharpf
     -
     -
     - Direktor GCA GenCode-Komitee
       
   * - Goldfab, Nosher, Lorie
     - 1969
     - GML
     - Generalized Markup Language (IBM)
          
   * - Charles Goldfab
     - 1978
     - ANSI
     -
     
   * -     
     - 1986
     - SGML
     - Standard Generalized Markup Language (ISO 8879)

   * - Tim Berners-Lee
     - 1989
     - HTML
     - Hypertext Markup Language (CERN)
       
   * - Mark Adnreessen
     - 1993
     -
     - HTML-Formulare (Xmosaic) der Firma NCSA

   * - Netscape/Microsoft
     - 1994
     - 
     - HTML-Abweichungen im Kampf um Marktanteile für die Browser.

   * - Dave Raggett
     -
     - 
     - HTML als Standard beim W3C

   * - Hakon Lie
     -
     -
     - CSS
       
   * -
     - 1994
     - HTML 2.0
     - Unter Leitung des W3C als Standard verabschiedet

   * -
     - 1994
     - CSS 1.0
     - Cascading Style Sheet als Ergänzung zu HTML.

   * -
     - 1996
     - HTML 3.2
     - Die Version 3.0 wurde nie verabschiedet.

   * -
     - 1996
     - XML 1.0
     - Als Diskussionsvorschlag vom W3C vorgestellt.

   * - Jon Bosak (Sun), James Clark
     - 1997
     - HTML 4.0
     - Als Richtlinie vom W3C verabschiedet.

   * -
     - 1998
     - CSS 2.0
     -
     
   * -
     - 1998
     - XML 1.0
     - Als Standard verabschiedet.
       
   * -
     - 1998
     - XSL 1.0
     - StyleSheet-Standard für die Verarbeitung von XML-Daten.

   * -
     - 2000
     - XHTML
     -  

   * -
     - 2001
     - XHTML 1.1
     -
     
   * -
     - 2002
     - XHTML 2.0
     - Entwurf: Basiert nicht mehr auf HTML 4.0, eine vollständige
       Trennung von Inhalt und Darstellung wird angestrebt.
       
   * -
     - 2008
     - HTML 5.0
     - Entwurf: Neues Vokabular auf Basis von XHTML und HTML 4.0

       
   * -
     - 2008
     -
     - 1. „Working Draft“ (Arbeitsentwurf)

   * -
     - 2008
     - 
     - 2. Arbeitsentwurf, in den der bis dahin separate
       Web-Forms-2.0-Entwurf eingearbeitet wurde.
     
   * -
     - 2009
     -
     - 3. Arbeitsentwurf
       
   * -
     - 2009
     -
     - 4. Arbeitsentwurf

   * -
     -
     -
     - 5. Arbeitsentwurf

   * -
     - 2010
     -
     - 6. Arbeitsentwurf. HTML Canvas 2D[17] und HTML Microdata
       als eigene Arbeitsentwürfe ausgelagert. Neues Dokument „HTML: The
       Markup Language“.

   * -
     -
     -
     - 7. Arbeitsentwurf und Überarbeitung der verwandten
       Spezifikationen und Dokumente. Neu hinzugefügt wurden die
       unterstützenden Dokumente HTML5: Techniques for providing
       useful text alternatives[20] und Polyglot Markup:
       HTML-Compatible XHTML Documents.[21]
       
   * -
     -
     -
     - 8. Arbeitsentwurf
       1. Arbeitsentwurf von „HTML5 Web Messaging“.

   * -
     - 2011
     -
     - 	9. Arbeitsentwurf
	
   * -
     -
     - 
     -	10. Arbeitsentwurf
	
   * -
     -
     -
     -	11. Arbeitsentwurf

   * -
     - 2012
     - 
     -	12. Arbeitsentwurf
	
   * -
     -
     -
     - 13. Arbeitsentwurf

   * -
     -
     -
     - Candidate Recommendation (Empfehlungs-Kandidat)

   * -
     - 2013
     -
     -	Candidate Recommendation (Empfehlungs-Kandidat)

   * -
     -
     -
     - Candidate Recommendation (Empfehlungs-Kandidat)

   * -
     - 2014
     -
     - Candidate Recommendation (Empfehlungs-Kandidat)

   * -
     -
     -
     - Letzter Arbeitsentwurf
       
   * -
     -
     -
     - Candidate Recommendation (Empfehlungs-Kandidat)

   * -
     -
     -
     - Proposed Recommendation (Empfehlungs-Vorschlag)

   * -
     -
     -
     - Recommendation (Empfehlung)


:Quellangaben:
	  
   - HTML5-Grafik
     
     Von Peter Kröner - https://github.com/SirPepe/SpecGraph, CC BY
     3.0 de,
     
     https://commons.wikimedia.org/w/index.php?curid=19051177

