.. meta::
   
   :description lang=de: XML-Kurs mit den Themen: DTD, Schema, XSL, XPath
   :keywords: XML, Alternativen, XSL, OER, DTD, Schema, XPath

:ref:`« Kursstart <xml-kurs-start>`

.. index:: Alternativen zu XML
	   
=====================
 Alternativen zu XML
=====================

.. image:: ./images/bratislava-man-at-work.jpg
   :width: 0px


.. |a| raw:: html

   <!-- lightbox container hidden with CSS -->
   <a href="#" class="lightbox" id="img1">
   <span style="background-image: url('../_images/bratislava-man-at-work.jpg')"></span>
   </a>

.. |b| raw:: html

   <a href="#img1">
   <img width="250px" src="../_images/bratislava-man-at-work.jpg">
   </a>

.. sidebar:: XML-Kurs

   |b|
   
   | Bratislava:
   | »man at work« 

|a|
  
     
Bevor wir uns weiter mit XML beschäftigen, will ich auf Alternativen
aufmerksam machen und auch einige Pro und Kontra benennen.

.. index:: Alternativen zu XML; JSON
	   
JSON
====
Ist sehr populär, weil es ein kompaktes, einfaches (Ansichtsache)
Format darstellt. Aber ein Vergleich der Parser zeigt, das es ein
schlecht implementierter Standard ist und man den Datenaustausch damit
gut planen sollte. Die Definitiion von Schemata, im XML lange Standard,
wird nun auch für JSON gefordert und implementiert. 

.. index:: Alternativen zu XML; YAML

YAML
====
YAML ist ein rekursives Akronym für „YAML Ain’t Markup Language“
(ursprünglich „Yet Another Markup Language“).

YAML ist eine vereinfachte Auszeichnungssprache (englisch markup
language) zur Datenserialisierung, angelehnt an XML (ursprünglich) und
an die Datenstrukturen in den Sprachen Perl, Python und C sowie dem in
RFC 2822 vorgestellten E-Mail-Format. Die Idee zu YAML stammt von
Clark Evans, die Spezifikation wurde von ihm selbst, Brian Ingerson
und Oren Ben-Kiki erstellt.

:Quelle: https://de.wikipedia.org/wiki/YAML

.. index:: Alternativen zu XML; ProtoBuf

ProtoBuf (Protocol Buffers)
===========================
:Website: https://developers.google.com/protocol-buffers/
	  https://developers.google.com/protocol-buffers/docs/proto3
	  
Weil andere Austauschformate immer noch Schwachstellen aufweisen, wurde
eine allgemeine Austauschvariante entwickelt. Interessant ist die
folgende Anmerkung von der Website über Alternativen zu ProtoBuf:

:Zitat:

Serialize the data to XML. This approach can be very attractive since
XML is (sort of) human readable and there are binding libraries for
lots of languages. This can be a good choice if you want to share data
with other applications/projects. However, XML is notoriously space
intensive, and encoding/decoding it can impose a huge performance
penalty on applications. Also, navigating an XML DOM tree is
considerably more complicated than navigating simple fields in a class
normally would be.

:Quelle: https://developers.google.com/protocol-buffers/docs/pythontutorial


RDF
===


Beispiel: T-Rex

.. code-block:: bash

   curl -L -H "Accept: application/rdf+xml" https://dbpedia.org/resource/Tyrannosaurus
   
   curl -L -H "Accept: text/html" https://dbpedia.org/resource/Tyrannosaurus


	 
Vergleich JSON -- XML
=====================


+---------------------+-----------------------------------+----------------------+-----------------------------------+
| Merkmal             | JSON                              | XML                  | Anmerkung                         |
+---------------------+-----------------------------------+----------------------+-----------------------------------+
| Format              | Text                              |                      |                                   |
+---------------------+-----------------------------------+----------------------+-----------------------------------+
|                     |                                   | Text                 | Untermenge von SGML               |
+---------------------+-----------------------------------+----------------------+-----------------------------------+
| Datei-Erweiterung   | json                              | xml, xsl, xsd...     | Das X in der Dateierweiterung     |
|                     |                                   |                      | ist ein Indiz für XML.            |
|                     |                                   |                      | Komplexe XML-Strukturen werden oft|
|                     |                                   |                      | In zip-Paketen verwaltet.         |
|                     |                                   |                      | Beispiele: odf, docx              |
+---------------------+-----------------------------------+----------------------+-----------------------------------+
| Medientyp           | application/json                  |                      |                                   |
|                     |                                   | application/xml      |                                   |
+---------------------+-----------------------------------+----------------------+-----------------------------------+
| Standard            | als Draft für IETF (Version7)     |                      | Stand: 04/2018                    |
+---------------------+-----------------------------------+----------------------+-----------------------------------+
|                     |                                   | W3C-Standard         | seit: 10. Februar 1998            |
+---------------------+-----------------------------------+----------------------+-----------------------------------+
| Lesbarkeit          | Mensch                            | Mensch               |                                   |
+---------------------+-----------------------------------+----------------------+-----------------------------------+
|                     | Maschine                          | Maschine             |                                   |
+---------------------+-----------------------------------+----------------------+-----------------------------------+
| self describing     | Aber: keine Kommentare            | <!-- Kommentar -->   |                                   |
+---------------------+-----------------------------------+----------------------+-----------------------------------+
| Konvertierung       |                                   |                      |                                   |
+---------------------+-----------------------------------+----------------------+-----------------------------------+
|                     |                                   | andere Textformate   | XML, CSV, JSON, HTML, ...         |
+---------------------+-----------------------------------+----------------------+-----------------------------------+
| Syntax              | Schlüssel-Wert-Paare              |                      |                                   |
+---------------------+-----------------------------------+----------------------+-----------------------------------+
|                     |                                   | Elemente             | Ergänzung durch Attribute als     |
|                     |                                   |                      | Schlüssel-Wert-Paare;             |
+---------------------+-----------------------------------+----------------------+-----------------------------------+
| Type-Definitionen   | {} = Objekt, [] = Array           |                      | direkte Umwandlung in             |
|                     |                                   |                      | JavaScript-Objekte                |
+---------------------+-----------------------------------+----------------------+-----------------------------------+
|                     |                                   | keine                | Typdefinition/Schema (XSD)        |
|                     |                                   |                      | Standard-Typen oder selbst        |
|                     |                                   |                      | definierte Typen                  |
+---------------------+-----------------------------------+----------------------+-----------------------------------+
| Einfaches Beispiel  | {"name":"Peter"}                  | <name>Peter</name>   | oder: <p name="Peter" />          |
+---------------------+-----------------------------------+----------------------+-----------------------------------+
| Programmiersprachen | viele                             | viele                |                                   |
+---------------------+-----------------------------------+----------------------+-----------------------------------+
| Datenbanken         | Key-Value-Stores, Document-Stores |                      | z.B. MongoDB, CouchDB             |
+---------------------+-----------------------------------+----------------------+-----------------------------------+
| Datenbanken         |                                   | XML-Datentyp         | z.B. Oracle, Postgres             |
+---------------------+-----------------------------------+----------------------+-----------------------------------+
| Parser              | JavaScript                        | spezielle XML-Parser | DOM oder SAX-Parser für viele     |
|                     |                                   |                      | Programmiersprachen               | 
+---------------------+-----------------------------------+----------------------+-----------------------------------+
| Umwandlung          | Object -> String: json.stringify()|                      | vergleichbare Methoden werden     |
|                     |                                   |                      | von allen Programmiersprachen     |
|                     | String -> Object: json.parse()    |                      | implementiert                     |
+---------------------+-----------------------------------+----------------------+-----------------------------------+
| Spezialversionen    | JSONP                             |                      |                                   |
+---------------------+-----------------------------------+----------------------+-----------------------------------+
|                     |                                   | OWL, RDF             | viele domainspezifische Varianten |
+---------------------+-----------------------------------+----------------------+-----------------------------------+

Wie ein Standard verwässert werden kann?
========================================

Etwas Ironie sei erlaubt, aber im Kern steckt ja immer ein wenig Wahrheit:

https://blog.fefe.de/?ts=9c84f0e0
