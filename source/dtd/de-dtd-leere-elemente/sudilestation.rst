.. meta::

   :description lang=de: XML-Kurs mit den Themen: DTD, Schema, XSL, XPath
   :keywords: DTD, Empty, leere Elemente, OER

:ref:`« Start: DTD <dtd-start>`

.. index:: DTD; Empty (Elemente ohne Inhalt)
.. index:: Leere Elemente, EMPTY, Leere Elemente

===================
DTD: Leere Elemente
===================


.. image:: ../images/hochsitz.jpg
   :width: 0px


.. |a| raw:: html

   <!-- lightbox container hidden with CSS -->
   <a href="#" class="lightbox" id="img1">
   <span style="background-image: url('../../_images/hochsitz.jpg')"></span>
   </a>

.. |b| raw:: html

   <a href="#img1">
   <img width="250px" src="../../_images/hochsitz.jpg">
   </a>

.. sidebar:: Hochsitz

   |b|


|a|


Lernziel
========
Leere Elemente sind solche, die keinen Inhalt haben, jedoch Attribute
besitzen können. Sie lernen, wie Sie diese in einer DTD erstellen
können.

Handlungsanweisungen
====================
:Aufgaben:

  1. Jeder CD und jedem Buch soll ein Bild hinzugefügt werden. Ändern
     Sie die DTD und  den Inhalt des jeweiligen XML-Dokumentes.
  2. Ein Bild soll für die Transformation zu HTML auch eine kurze
     Beschreibung erhalten. Fügen Sie das Attribut "beschreibung" als
     optionales Angabe hinzu.
  3. Das Beispiel in Kapitel 2 enthält einen Fehler. Finden Sie den
     Fehler und schicken Sie dem Tutor einen Bug-Report.  Benutzen Sie
     dazu das interne Mail-System der Lernplattform.

Definition leerer Elemente:
===========================

Sie kennen Sie vom HTML, einige Beispiele:
::

  <hr />
  <br />
  <img src="bild.png" />


Allen ist gemein, dass sie kein schließendes Element-Teil
benötigen. Der Slash (/) kennzeichnet, dass es sich um ein leeres
Element handelt. Elemente können auch Attribute enthalten,
siehe auch  *src="bild.png"*.

Wie definieren Sie nun leere Elemente in einer DTD?
::

  <!ELEMENT Name_des_Elements EMPTY>

Bei „Name_des_Elements“ notieren Sie Ihren Elementnamen.
Durch *EMPTY* wird gekennzeichnet, dass es keine Inhalte aufnehmen,
sondern nur Attribut-Werte-Paare enthalten soll.

Ein Beispiel
~~~~~~~~~~~~
Betrachten Sie nun folgendes Beispiel, in dem ein Element für ein Bild
definiert wird (Zeile 14-15)


.. code-block:: xml
   :linenos:

   <?xml version="1.0" encoding="UTF-8"?>
   <!DOCTYPE adressensammlung [
   <!ELEMENT adressensammlung (adresse)+>
     <!ELEMENT adresse (anrede, name, strasse, postanschrift, email*, bild)>
     <!ELEMENT anrede (#PCDATA)>
     <!ELEMENT name (nachname, vorname)>
     <!ELEMENT postanschrift (plz, wohnort)>
     <!ELEMENT nachname (#PCDATA)>
     <!ELEMENT vorname (#PCDATA)>
     <!ELEMENT plz (#PCDATA)>
     <!ELEMENT wohnort (#PCDATA)>
     <!ELEMENT strasse (#PCDATA)>
     <!ELEMENT email (#PCDATA)>
     <!ELEMENT bild EMPTY>
     <!ATTLIST bild bezeichnung CDATA #REQUIRED>
     <!ATTLIST postanschrift
               land (Brandenburg | Mecklenburg | Sachsen) #REQUIRED>
   ]>
   <adressensammlung>
     <adresse>
       <anrede/>
       <name>
	 <nachname/>
	 <vorname/>
       </name>
       <strasse/>
       <postanschrift land="Deutschland">
	 <plz/>
	 <wohnort/>
       </postanschrift>
       <email/>
       <bild bezeichnung="../testbild.png"/>
     </adresse>
   </adressensammlung>

:Erklärung: Wir definieren ein Element *bild*, das keinen Inhalt hat,
	    aber ein Attribut *bezeichnung*, das eine beliebige
	    Zeichenfolge enthalten kann (z.B. den Pfad zu einem Bild)
	    und Pflicht ist.
