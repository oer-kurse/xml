.. meta::
   
   :description lang=de: XML-Kurs mit den Themen: DTD, Schema, XSL, XPath
   :keywords: DTD, ELEMENT, PCDATA, CDATA, OER

:ref:`« Start: DTD <dtd-start>`
     
.. index:: DTD; DOCTYPE, DTD; Elemente,
.. index:: DTD; ELEMENT, DTD; PCDATA, DTD; CDATA
	   
=============
DTD erstellen
=============

.. image:: ../images/caputh-himmel001.jpg
   :width: 0px


.. |a| raw:: html

   <!-- lightbox container hidden with CSS -->
   <a href="#" class="lightbox" id="img1">
   <span style="background-image: url('../../_images/caputh-himmel001.jpg')"></span>
   </a>

.. |b| raw:: html

   <a href="#img1">
   <img width="250px" src="../../_images/caputh-himmel001.jpg">
   </a>

.. sidebar:: Himmel über Caputh

   |b|

   

|a|


Lernziel
========

Sie lernen, wie eine DTD aufgebaut ist. Außerdem werden Sie gleich
selbst eine DTD erstellen.

Viel Erfolg!

Handlungsanweisungen
====================
:Aufgaben:
  1. Entwicklen Sie die DTDs für die von Ihnen schon erstellen CD- und
     Buch-Datenbanken. (Benutzen Sie ein "+"-Zeichen hinter dem
     Wurzelelement. Dies ermöglicht, dass mehrere Datensätze in Ihren
     Datenbanken eingetragen seien dürfen. Mehr dazu im nächsten
     Kapitel.)     
  2. Was ist der Nachteil der DTD?
  3. Lageren Sie die DTD der CD-Datenbank in eine externe Datei aus
     und binden Sie diese dann im XML-Dokument ein.
  4. Prüfen Sie die Gültigkeit (Validität) Ihrer XML-Dokumente mit
     einem Validierungstool oder -service!
  5. Was entspricht in einer DTD nicht dem XML-Regeln?

Interne und externe DTDs
~~~~~~~~~~~~~~~~~~~~~~~~

Es gibt zwei Möglichkeiten DTDs zu definieren. Intern und extern: 

.. image:: img/dtd-einbinden.png

Wir schauen uns zunächst die interne DTD an.

DTD der Adressdatenbank

Nun lernen Sie anhand eines Beispiels, wie Sie eine DTD erstellen können. 
Eine DTD ist nach folgender Syntax aufgebaut: 
::

  <!DOCTYPE Name_des_Wurzelelements 
  [
   <!ELEMENT Name_des_Elements (Elementinhalt)> 
  ]>

Schauen wir uns nun die DTD für die Adressdatenbank an: 

.. code-block:: xml
   :linenos:
   
   <?xml version="1.0" encoding="UTF-8"?>
   <!DOCTYPE adresse [
     <!ELEMENT adresse (anrede, name, strasse, postanschrift, email)>
     <!ELEMENT anrede (#PCDATA)>
     <!ELEMENT name (nachname, vorname)>
     <!ELEMENT postanschrift (plz, wohnort)>
     <!ELEMENT nachname (#PCDATA)>
     <!ELEMENT vorname (#PCDATA)>
     <!ELEMENT plz (#PCDATA)>
     <!ELEMENT wohnort (#PCDATA)>
     <!ELEMENT strasse (#PCDATA)>
     <!ELEMENT email (#PCDATA)>
   ]>
   <adresse>
     <anrede>...</anrede>
     <name>
       <nachname>...</nachname>
       <vorname>...</vorname>
     </name>
     <strasse>...</strasse>
     <postanschrift>
       <plz>...</plz>
       <wohnort>...</wohnort>
     </postanschrift>
     <email>...</email>
    </adresse>

Erklärung:
 
.. code-block:: xml
   :linenos:

   <?xml version="1.0" encoding="UTF-8" ?>
   <!DOCTYPE adresse 
   [
    ...
   ]>

Hier beginnt die Definition der DTD für den XML-Inhalt. 
Hinter DOCTYPE wird das Wurzelelement (Zeile 2) angegeben.

Die Definition der im Wurzelelement "adresse" enthaltenen Elemente. 
Das Element adresse soll anrede, name, strasse, postanschrift und email 
in der angegebenen Reihenfolge enthalten. 

.. code-block:: xml
   :linenos:

   <?xml version="1.0" encoding="UTF-8" ?>
   <!DOCTYPE adresse 
   [
   ...
     <!ELEMENT anrede (#PCDATA)>
   ... 
 
   ]>


#PCDATA bedeutet, dass innerhalb des definierten Elements normale Zeichen 
(keine weiteren Elemente) stehen dürfen, also nur Ziffern und Buchstaben. 
Der Inhalt wird später ebenfalls geparsed, deshalb PCDATA = Parsed
Character DATA.
Im Unterschied dazu gibt es auch noch den Typ CDATA, dessen Inhalt keiner 
Prüfung unterzogen wird.

Wohlgeformtheit und Gültigkeit

Sie haben schon gelernt, dass ein XML-Dokument wohlgeformt sein kann,
wenn XML in der richtigen Syntax verfasst ist. Entspricht ein
XML-Dokument darüber hinaus auch noch der DTD, spricht man von
Gültigkeit eines XML- Dokumentes.


.. image:: img/wellformed-valide.png

**Externe DTDs**

Für die Übersichtlichkeit und um sich Tipparbeit zu sparen, ist es
sinnvoll, DTDs in externe Dateien auszulagern und in die XML wieder zu
einzubinden. Wir schauen uns nun ein Beispiel an, mit Zeile 3 wird die
DTD eingebunden:

.. code-block:: xml
   :linenos:

   <?xml version="1.0" encoding="UTF-8"?>
   <!--JETZT BINDEN WIR DIE EXTERNE DTD EIN-->
   <!DOCTYPE adresse SYSTEM "adresse.dtd">
   <adresse>
     <anrede>...</anrede>
     <name>
       <nachname>...</nachname>
       <vorname>...</vorname>
     </name>
     <strasse>...</strasse>
     <postanschrift>
       <plz>...</plz>
       <wohnort>...</wohnort>
     </postanschrift>
     <email>...</email>
   </adresse>

und die externe DTD „adresse.dtd“ sieht so aus:

.. code-block:: xml
   :linenos:

   <!ELEMENT adresse (anrede, name, strasse, postanschrift, email)>
   <!ELEMENT anrede (#PCDATA)>
   <!ELEMENT name (nachname, vorname)>
   <!ELEMENT postanschrift (plz, wohnort)>
   <!ELEMENT nachname (#PCDATA)>
   <!ELEMENT vorname (#PCDATA)>
   <!ELEMENT plz (#PCDATA)>
   <!ELEMENT wohnort (#PCDATA)>
   <!ELEMENT strasse (#PCDATA)>
   <!ELEMENT email (#PCDATA)>
 
Nachteil der DTD
~~~~~~~~~~~~~~~~

Die DTD besitzt einen Nachteil: es ist mit der DTD nicht möglich, den
konkreten Inhalt in einem Element zu unterscheiden. Beispielsweise muß
ein Element, das die Telefonnummern speichert (<Telefon>), in der DTD
als PCDATA deklariert werden:

::

  <!ELEMENT Telefon (#PCDATA)>

Das Problem: PCDATA kann nicht zwischen Zahlen und normalen Buchstaben
unterscheiden. Es ist nicht sinnvoll, in dem Tag <Telefon> Buchstaben
einzugeben. Oder beispielsweise bei einem Tag <Geburtsdatum> kann
dieses auch nur als PCDATA angegeben werden.

Der XML-Parser kann nicht unterscheiden zwischen echten Geburtsdaten 
::

  <geburtsdatum>19.01.1967</geburtsdatum>

und falschen Daten z.B.:
::

 <geburtsdatum>ab.cd.19ef</geburtsdatum>). 

Dieser Nachteil wurde auch vom W3-Konsortium erkannt. Dies führte zur
Weiterentwicklung der DTD, die unter dem Namen XML Schema geführt
wird. XML Schema bietet die Möglichkeit, den Datentyp eines
XML-Elements genau anzugeben. Dabei kann man zwischen normalen
Buchstaben, Zahlen und sogar zwischen verschiedenen Wertebereichen der
Zahlen unterscheiden.

Ein weiterer Vorteil der neuen XML-Schema-Sprache ist die XML-Form
selbst, denn die alte DTD-Variante ist selbst nicht XML-Konform, auch
wenn es wie XML aussieht.

 

