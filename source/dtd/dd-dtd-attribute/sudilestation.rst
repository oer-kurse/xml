.. meta::

   :description lang=de: XML-Kurs inclusive DTD, Schema, XSL, XPath
   :keywords: DTD, Attribute, OER

:ref:`« Start: DTD <dtd-start>`

.. index:: DTD; Attribute

==============
DTD: Attribute
==============


.. image:: ../images/bobbycar.jpg
   :width: 0px


.. |a| raw:: html

   <!-- lightbox container hidden with CSS -->
   <a href="#" class="lightbox" id="img1">
   <span style="background-image: url('../../_images/bobbycar.jpg')"></span>
   </a>

.. |b| raw:: html

   <a href="#img1">
   <img width="250px" src="../../_images/bobbycar.jpg">
   </a>

.. sidebar:: Autosicherung

   |b|


|a|


Lernziel
========

Nach der Station wissen Sie, was Attribute sind und wie Sie diese definieren.

Handlungsanweisungen
====================
:Aufgaben:
  1. Fügen Sie per Attribut eine Nummerierung der Datensätze in Ihrer
     CD-Datenbank durch. Ändern Sie den jeweiligen XML-Content und die
     jeweilige DTD. Die Nummerierung sollte für alle Datensätze
     Pflicht sein.

  2. Fügen Sie in Ihrer CD-Datenbank zu dem Element Bandinformation
     ein Attribut  "Herkunft" hinzu. Dieses Attribut soll folgende
     Zustände annehmen können: "Europa", "Nordamerika", "Südamerika",
     "Afrika", "Asien". Die Angabe der Herkunft ist optional.

:Zusatz:
  3. Fügen Sie jedem Buch ein Attribut "Format" hinzu. Folgende
     Angaben sind erlaubt: "a3","a4","a5". Die Angabe ist
     Pflicht. Ändern Sie dementsprechend den XML-Inhalt.


Attribute
~~~~~~~~~

Was sind Attibute?

Attribute sind zusätzliche Informationen zu einem Element.
Aus der HTML-Welt kennen Sie sicherlich folgendes Element mit dem Attribut:
::

  <img src="mein-bild.png" />

wobie *src* hier das Attribut für das img-Element ist.

Auch in XML können Sie Attribute verwenden.
Wie Sie diese in der DTD definieren, lernen Sie nun.

Attributregeln
~~~~~~~~~~~~~~

Die Syntax, um ein Attribut zu definieren, sieht so aus:
::

  <!ATTLIST Elementname Attributname (Attributwerte) Defaultdeklaration >

:Elementname: Welchem Element ist das Attribut zuzuordnen?
:Attributname: Wie heißt das Attribut?
:Attributwerte: Hier kann eine von zwei Varianten gewählt werden:

	1. Aufzählung aller zulässigen Werte
	2. CDATA = beliebige Zeichenfolge

:Defaultdeklaration: legt fest, ob ein Wert optional oder die Angabe
		     Pflicht ist.

		     - #IMPLIED = die Eingabe ist optional
		     - #REQUIRED = die Eingabe ist Pflicht

Ein Beispiel:
~~~~~~~~~~~~~
Schauen wir uns nun ein Beispiel an: Sie Definition für das Attribut
"land" als Pflichtangabe für das Element "postanschrift" mit einem
definierten Wertebereich (siehe Zeilen 14-15).

.. code-block:: xml
   :linenos:


   <?xml version="1.0" encoding="UTF-8"?>
   <!DOCTYPE adressensammlung [
   <!ELEMENT adressensammlung (adresse)+>
   <!ELEMENT adresse (anrede, name, strasse, postanschrift, email*)>
   <!ELEMENT anrede (#PCDATA)>
   <!ELEMENT name (nachname, vorname)>
   <!ELEMENT postanschrift (plz, wohnort)>
   <!ELEMENT nachname (#PCDATA)>
   <!ELEMENT vorname (#PCDATA)>
   <!ELEMENT plz (#PCDATA)>
   <!ELEMENT wohnort (#PCDATA)>
   <!ELEMENT strasse (#PCDATA)>
   <!ELEMENT email (#PCDATA)>
   <!ATTLIST postanschrift land (Deutschland | Schweiz | Oesterreich)
             #REQUIRED>
   ]>
  <adressensammlung>
    <adresse>
      <anrede/>
      <name>
	<nachname/>
	<vorname/>
      </name>
      <strasse/>
      <postanschrift land="Deutschland">
	<plz/>
	<wohnort/>
      </postanschrift>
      <email/>
    </adresse>
  </adressensammlung>

Noch ein Beispiel
~~~~~~~~~~~~~~~~~

Schauen Sie sich ein zweites Beispiel an. Wir fügen hier eine
Nummerierung ein, weil mehrere Adressen in der Datenbank stehen. Der
Datentyp ist CDATA, also eine beliebige Zeichenfolge (Zeile 16).
Hier der Code:

.. code-block:: xml
   :linenos:


   <?xml version="1.0" encoding="UTF-8"?>
   <!DOCTYPE adressensammlung [
   <!ELEMENT adressensammlung (adresse)+>
   <!ELEMENT adresse (anrede, name, strasse, postanschrift, email*)>
   <!ELEMENT anrede (#PCDATA)>
   <!ELEMENT name (nachname, vorname)>
   <!ELEMENT postanschrift (plz, wohnort)>
   <!ELEMENT nachname (#PCDATA)>
   <!ELEMENT vorname (#PCDATA)>
   <!ELEMENT plz (#PCDATA)>
   <!ELEMENT wohnort (#PCDATA)>
   <!ELEMENT strasse (#PCDATA)>
   <!ELEMENT email (#PCDATA)>
   <!ATTLIST postanschrift land (Deutschland | Schweiz | Oesterreich)
            #REQUIRED>
   <!ATTLIST adresse nr CDATA #REQUIRED>
   ]>
   <adressensammlung>
     <adresse nr="1">  ... </adresse>
     <adresse nr="2">  ... </adresse>
   </adressensammlung>

In der DTD verwenden wir als Datentyp "CDATA", also eine beliebige
Zeichenfolge. Die Adressen können nun durch ein Attribut nummeriert
werden.

:Links: http://de.selfhtml.org/xml/dtd/attribute.htm
