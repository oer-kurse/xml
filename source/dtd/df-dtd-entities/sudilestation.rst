.. meta::

   :description lang=de: XML-Kurs mit den Themen: DTD, Schema, XSL, XPath
   :keywords: DTD, Abkürzungen, Entity, OER


:ref:`« Start: DTD <dtd-start>`

.. index:: DTD; Abkürzungen (Entities)
.. index:: DTD; Entities (Abkürzungen)

==========================
Abkürzungen in DTD-Dateien
==========================

.. image:: ../images/bobbycar.jpg
   :width: 0px


.. |a| raw:: html

   <!-- lightbox container hidden with CSS -->
   <a href="#" class="lightbox" id="img1">
   <span style="background-image: url('../../_images/bobbycar.jpg')"></span>
   </a>

.. |b| raw:: html

   <a href="#img1">
   <img width="250px" src="../../_images/bobbycar.jpg">
   </a>

.. sidebar:: Wegfahrsperre

   |b|

|a|


Lernziel
========

Mit Abkürzungen kann man sich Tipparbeit sparen.  Außerdem wird der
Quelltext übersichtlicher. Wie man das in einer DTD macht, das
lernen Sie hier. Im Unterschied zu den Textersetzungen im
XML-Dokument, geht es hier um Ersetzungen in einer DTD, die dem
Entwicker Tipparbeit erspart und Wiederholungen von identischen
Konstrukten vermeidet

Handlungsanweisungen
====================
:Aufgaben:

  1. Definieren Sie, ob die Daten *Bandinformationen* und *Preis* aus Ihrer
     CD-Datenbank öffentlich zugänglich sein sollen oder nicht. Benutzen Sie
     dafür Entities. Die Angabe soll optional sein. Ändern Sie zwei Datensätze
     und setzen Sie die Bandinformationen *public*.
  2. Erstellen Sie außerdem eine Entität für das Euro-Zeichen.
  3. Lagern Sie einen Teil der DTD in eine neue DTD aus.
     Laden Sie die neu erstellte DTD in die aktuelle DTD ein
     (siehe ganz unten in dieser Station).

Abkürzungen in einer DTD-Datei
==============================

Wie in Texten wiederholen sich in einer DTD bestimmte Angaben immer wieder.
Solche wiederkehrenden DTD-Konstrukte werden vor den ELEMENT-Angaben definiert
und können dann beliebig oft in der DTD Verwendung finden. Diese Art der
Abkürzung wird Parameter-Entity genannt und unterscheidet sich von einer
Text-Entity durch das Prozentzeichen:
::

  <!ENTITY % Name_der_Entity "Ein gültiges DTD-Konstrukt" >

Beispiel: Adressdaten könnten auch als öffentlich und privat deklariert werden.
Wir definieren diese für die Postanschrift, die Strasse und die E-Mail-Adresse.
Die Angaben wiederholen sich und sind damit Kandidaten für eine
Abkürzung. (Zeilen: 1, 2, 10, 16, 18)

Hier die DTD:

.. code-block:: xml
   :linenos:

   <!--Die Entität muss oben definiert werden-->
   <!ENTITY % sichtbarkeit "einsicht (privat|public) #REQUIRED">
   <!ELEMENT adressensammlung (adresse)+>
   <!ELEMENT adresse (anrede, name, strasse, postanschrift, email*)>
   <!ELEMENT anrede (#PCDATA)>
   <!ELEMENT name (nachname, vorname)>
   <!ELEMENT postanschrift (plz, wohnort)>
   <!ATTLIST postanschrift
     land (Deutschland | Schweiz | Oesterreich) #REQUIRED
     %sichtbarkeit; >
   <!ELEMENT nachname (#PCDATA)>
   <!ELEMENT vorname (#PCDATA)>
   <!ELEMENT plz (#PCDATA)>
   <!ELEMENT wohnort (#PCDATA)>
   <!ELEMENT strasse (#PCDATA)>
   <!ATTLIST strasse  %sichtbarkeit; >
   <!ELEMENT email (#PCDATA)>
   <!ATTLIST email %sichtbarkeit; >


Und die Anwendung der Regeln im XML-Dokument (Zeilen: 10, 11, 15):

.. code-block:: xml
   :linenos:

   <?xml version="1.0" encoding="UTF-8"?>
   <!DOCTYPE adresse SYSTEM "adresse.dtd">
   <adressensammlung>
     <adresse>
       <anrede>Herr</anrede>
       <name>
         <nachname>Becker</nachname>
         <vorname>Heinz</vorname>
       </name>
       <strasse einsicht="public">Schwabenstr. 19</strasse>
       <postanschrift einsicht="public" land="Deutschland">
         <plz>14488</plz>
         <wohnort>Berlin</wohnort>
       </postanschrift>
       <email einsicht="privat">heinz@becker.de</email>
     </adresse>
   </adressensammlung>


:Erklärung: Wir definieren eine Abkürzung mit der Bezeichnung *sichtbarkeit*.
            Wichtig hier ist das Prozentzeichen (%), das besagt, dass es sich
            bei dieser ENTITY-Definition um eine Parameter-Abkürzung handelt.
            Wir definieren dann die Attribut-Listen mit dem Inhalt von
	    der ENTITY *sichtbarkeit*. Die Abkürzung wird  mit
	    *%sichtbarkeit;* an allen Stellen der DTD, die ein
	    Attribut *einsicht* mit den Werten *privat*  oder *public*
	    erlauben soll, eingefügt.

.. note:: Manche Parser verlangen für Abkürzungen eine externe DTD.
          Günstig ist es also, die DTD als extern zu definieren.

DTDs aus verschiedenen Dateien zusammensetzen
=============================================

Ein Teil der DTD ist in der *frag.dtd* ausgelagert, da sie z.B. in
verschiedenen DTDs verwendet wird. Mit *%daten;* wird der Inhalt von
*frag.dtd* in die aktuelle DTD eingefügt (Zeilen: 4, 11).

.. code-block:: xml
   :linenos:

   <?xml version="1.0" encoding="UTF-8"?>
   <!ENTITY % zugang "einsicht (privat|public) #REQUIRED">
   <!ENTITY wo_default "kein wohnort angegeben" >
   <!ENTITY % daten SYSTEM "frag.dtd">

   <!ELEMENT adressen (adresse+)>
   <!ELEMENT adresse (name,str,plz,wohnort,abbildung?)>
   <!ELEMENT name (vn,nn)>
   <!ATTLIST name %zugang;>

   %daten;

   <!ATTLIST plz %zugang;>
