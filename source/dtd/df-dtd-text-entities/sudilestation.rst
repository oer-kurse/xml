.. meta::
   
   :description lang=de: XML-Kurs mit den Themen: DTD, Schema, XSL, XPath
   :keywords: DTD, Abkürzungen, Entity 


:ref:`« Start: DTD <dtd-start>`

.. index:: DTD; Textersetzungen
     
==========================
Abkürzungen in XML-Dateien
==========================

.. image:: ../images/diverse-verbote.jpg
   :width: 0px


.. |a| raw:: html

   <!-- lightbox container hidden with CSS -->
   <a href="#" class="lightbox" id="img1">
   <span style="background-image: url('../../_images/diverse-verbote.jpg')"></span>
   </a>

.. |b| raw:: html

   <a href="#img1">
   <img width="250px" src="../../_images/diverse-verbote.jpg">
   </a>

.. sidebar:: Das ist verboten...

   |b|

|a|

Lernziel
========

Mit Abkürzungen kann man sich Tipparbeit sparen.  
Außerdem wird der Quelltext übersichtlicher. 
Wie man das macht? Das lernen Sie hier.

:Wichtiger Hinweis: Die selbst definierte Ersetzungen funktionieren nur, wenn die
		    DTD-Konstrukte **direkt im XML integriert** sind, wie
		    im Beispiel gezeigt.


Handlungsanweisungen
====================
:Aufgabe: 

  - Fall Sie mit einer externen DTD arbeiten, integrieren Sie die
    DTD-Konstrukte direkt in die XML-Datei.
  - Ist der Titel einer CD noch nicht bekannt, so definieren Sie bitte 
    eine Abkürzung *&kein_titel;*. Dort soll stehen »Titel der CD
    noch nicht bekannt.« Fügen Sie einen neuen Datensatz  ein.
    Achten Sie auf die DTD. 

Eine einfache Abkürzungen
=========================

Entities sind wie in einer Textverarbeitung Textbausteine, welche die 
Eingabe von Textbausteinen beschleunigen. Sie kennen sicherlich einige 
Abkürzungen aus der HTML-Welt, wie z.B. 
::

  &Uuml; &auml; &ouml; &szlig; ...


Mit dem XML-Standard können Sie eigene Abkürzungen definieren:
::

  <!ENTITY Name_der_Entity Textinhalt >

Das Schlüsselwort ist *ENTITY*. Mit diesem geben Sie der Abkürzung einen Namen.
Der eigentliche Inhalt folgt danach.
  
Wie vom HTML bekannt, beginnen bei der Eingabe diese Abkürzungen mit
einem **&** (Und-Zeichen) und werden mit einem  **;** (Semikolon)
abgeschlossen.

Ein Beispiel
------------
Da der Text oft vorkommen kann, ist es sinnvoll, ein Entity dafür zu bauen. 
Würde das Element *email* später ausgelesen werden, führt das
automatisch zu dem Eintrag »Keine E-Mail angegeben!«: Zeilen: 13
(Definition), 26 (Anwendung).

.. code-block:: xml
   :linenos:

   <?xml version="1.0" encoding="UTF-8"?>
   <!DOCTYPE adresse [
     <!ELEMENT adresse (anrede, name, strasse, postanschrift, email*)>
     <!ELEMENT anrede (#PCDATA)>
     <!ELEMENT name (nachname, vorname)>
     <!ELEMENT postanschrift (plz, wohnort)>
     <!ELEMENT nachname (#PCDATA)>
     <!ELEMENT vorname (#PCDATA)>
     <!ELEMENT plz (#PCDATA)>
     <!ELEMENT wohnort (#PCDATA)>
     <!ELEMENT strasse (#PCDATA)>
     <!ELEMENT email (#PCDATA)>
     <!ENTITY email_default "Keine E-Mail angegeben!">
   ]>
   <adresse>
     <anrede>Herr</anrede>
     <name>
       <nachname>Becker</nachname>
       <vorname>Heinz</vorname>
     </name>
     <strasse>Schwabenstr. 7</strasse>
     <postanschrift>
       <plz>14488</plz>
       <wohnort>Berlin</wohnort>
     </postanschrift>
     <email>&email_default;</email>
   </adresse>

