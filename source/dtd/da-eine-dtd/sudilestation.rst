.. meta::
   
   :description lang=de: XML-Kurs mit den Themen: DTD, Schema, XSL, XPath
   :keywords: XML, XSL, OER, DTD, Schema, XPath

:ref:`« Start: DTD <dtd-start>`

.. index:: DTD
     
==============
Was sind DTDs?
==============

.. image:: ../images/elbsandsteingebirge-lokomotive.jpg
   :width: 0px


.. |a| raw:: html

   <!-- lightbox container hidden with CSS -->
   <a href="#" class="lightbox" id="img1">Lokomotive im Elbsandsteingebirge
   <span style="background-image: url('../../_images/elbsandsteingebirge-lokomotive.jpg')"></span>
   </a>

.. |b| raw:: html

   <a href="#img1">
   <img width="250px" src="../../_images/elbsandsteingebirge-lokomotive.jpg">
   </a>

.. sidebar:: Lokomotive im Elbsandsteingebirge

   |b|

|a|
   
Lernziel
========

Sie erfahren in dieser Station, was DTDs sind.

Handlungsanweisungen
====================
:Aufgaben:
  1. Suchen sie eine Definition für **DTD** im Internet
     (siehe auch: `Standards des W3C <https://www.w3.org/TR/>`_).
  2. Gibt es eine DTD für den HTML-5-Standard?


Was ist eine DTD?
~~~~~~~~~~~~~~~~~

Das Regelwerk für eine XML-Datei wird in einer DTD (Document Type Definition) 
festgelegt. Damit kann eine XML-Datei von Programmen geprüft und/oder 
verarbeitet werden. Das Ergebnis einer Überprüfung kann zu dem Prädikat 
*valid* führen, d.h. das XML-Dokument entspricht vom Aufbau her den Regeln 
einer DTD. 

Anhand der DTD können Sie genau festlegen, wie ein XML-Dokument 
auszusehen hat und was es alles enthalten darf. Mit der DTD legen Sie einen 
Standard für XML-Dokumente fest, an die sich andere Programmierer
halten müssen.
Sinnvoll ist das vor allem für Daten, an denen viele Menschen arbeiten, 
wie z.B. bei einem Buchprojekt. Indem Sie z.B. festlegen, wie ein Buch und 
deren Kapitel auszusehen haben, können Sie die von verschiedenen Autoren 
geschriebenen Artikel später bequem zu einem Buch zusammenfügen. 
Einfacher als viele verschiedene Formate zu einem Format zu konvertieren.  

