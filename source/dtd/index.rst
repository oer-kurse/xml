.. meta::
    
   :description lang=de: XML-Kurs mit den Themen: DTD, Schema, XSL, XPath
   :keywords: DTD, Übersicht, OER

.. _dtd-start:

:ref:`« Kursstart <xml-kurs-start>`

.. index:: DTD; Übersicht

===
DTD
===

.. image:: ./images/kaktusbluete05.jpg
   :width: 0px


.. |a| raw:: html

   <!-- lightbox container hidden with CSS -->
   <a href="#" class="lightbox" id="img1">
   <span style="background-image: url('../_images/kaktusbluete05.jpg')"></span>
   </a>

.. |b| raw:: html

   <a href="#img1">
   <img width="250px"
        alt="Kaktus-Büte"
        src="../_images/kaktusbluete05.jpg">
   </a>

.. sidebar:: XML-Kurs

   |b|

   Serie: Pflanzen

|a|
    
.. toctree::
   :maxdepth: 1
   :glob:
      
   */*
