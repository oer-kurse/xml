.. meta::

   :description lang=de: XML-Kurs mit den Themen: DTD, Schema, XSL, XPath
   :keywords: DTD, Gruppenaufgabe


:ref:`« Start: DTD <dtd-start>`

.. index:: DTD; Komplexe Aufgabe (Gruppenarbeit)

===================
DTD: Gruppenprojekt
===================

.. image:: ../images/nashorn.jpg
   :width: 0px

.. |a| raw:: html

   <!-- lightbox container hidden with CSS -->
   <a href="#" class="lightbox" id="img1">
   <span style="background-image: url('../../_images/nashorn.jpg')"></span>
   </a>

.. |b| raw:: html

   <a href="#img1">
   <img width="250px" src="../../_images/nashorn.jpg">
   </a>

.. sidebar:: Ich will spielen

   |b|

|a|


Lernziel
========

.. image:: img/flora-und-fauna.jpg

Sie sollen durch dieses kleine Projekt den Prozess von Standardisierungen im
XML-Bereich nachvollziehen können. Außerdem sollte Ihnen klar werden, welche
Vorteile XML-Standards haben. Das Ziel der Übung ist es, einen einheitlichen
Standard für die Datenerfassung zu entwickeln.

Handlungsanweisungen
====================

:Aufgaben:
   1. **Pflanzen** oder **Tiere** sollen digital erfasst werden, deshalb
      arbeiten mehrere Forscher zusammen. Alle diese Forscher sollen die
      Daten per XML erfassen. Damit keine Konflikte in der Beschreibung der
      Objekte entstehen, soll ein Standard zur Erfassung entwickelt werden.

      *Das ist nun Ihre Aufgabe!*

      Überlegen Sie sich in der Gruppe, Welche Kriterien Sie für
      wichtig halten (Papier und Stift genügen), um ein Lebewesen
      exakt zu beschreiben. Das muss natürlich nicht vollständig sein,
      wie jedes Modell, enthält es nur die wichtigsten Daten.
   2. Erstellen Sie zu dem in der Gruppe gefundenen Standard eine DTD.
   3. Generieren Sie eine XML-Datei, in der einige Pflanzen oder Tiere
      mit Hilfe der DTD aufgenommen werden sollen.

:Anmerkung: Falls Sie Anregungen für Ihre Systematik brauchen, helfen
	    die Suche auf den folgenden Seite vielleicht weiter:

            - http://www.google.de
	    - http://www.wikipedia.de

Nach dieser Übung, können sie sehr viel besser einschätzen, welche Arbeit
`Brehm <http://de.wikipedia.org/wiki/Brehms_Thierleben>`_,
`Linné <http://de.wikipedia.org/wiki/Carl_von_Linn%C3%A9>`_
und viele andere Forscher geleistet haben.
