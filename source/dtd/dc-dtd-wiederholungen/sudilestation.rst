.. meta::

   :description lang=de: XML-Kurs mit den Themen: DTD, Schema, XSL, XPath
   :keywords: DTD, Wiederholungen, OER


:ref:`« Start: DTD <dtd-start>`

.. index:: DTD; Wiederholungen

===================
DTD: Wiederholungen
===================


.. image:: ../images/schmetterlingspuppen.jpg
   :width: 0px


.. |a| raw:: html

   <!-- lightbox container hidden with CSS -->
   <a href="#" class="lightbox" id="img1">
   <span style="background-image: url('../../_images/schmetterlingspuppen.jpg')"></span>
   </a>

.. |b| raw:: html

   <a href="#img1">
   <img width="250px" src="../../_images/schmetterlingspuppen.jpg">
   </a>

.. sidebar:: Schmetterlinge

   |b|

|a|


Lernziel
========

Sie lernen, wie Sie mit einer DTD definieren, wie oft sich ein Element
wiederholen darf.


Handlungsanweisungen
====================
:Aufgaben:

   1. In Ihrer CD-Datenbank muss mindestens eine CD eingetragen sein.
      Der Preis muss nicht unbedingt enthalten sein, wenn dann aber
      höchstens einmal.

   2. Überlegen Sie, was in Ihrer Buchdatenbank optional sein könnte.
      Ändern Sie die Angaben in der DTD sinnvoll.

Wiederholungsregeln
~~~~~~~~~~~~~~~~~~~

Die aktuelle Version der Adressdatenbank hat den Nachteil, dass nicht
definiert ist, wie oft sich ein Element wiederholen darf! Dies
verhindert auch die Eingabe von sich wiederholden Strukturen. Hier
zunächst die Wiederholungsregeln:

+------------------+--------------------------------------------------------+
| Zeichen          | Wirkung                                                |
+==================+========================================================+
| **+**            | Vorkommen des Elementes mindestens 1 x bis beliebig oft|
+------------------+--------------------------------------------------------+
| **\***           | Vorkommen des Elementes 0 x bis beliebig oft           |
+------------------+--------------------------------------------------------+
| **?**            | Vorkommen des Elementes ist optional, d.h. 0 bis 1 x   |
+------------------+--------------------------------------------------------+
| **keine Angabe** | das Element ist genau 1 x vorhanden                    |
+------------------+--------------------------------------------------------+

Beispiel: Adressensammlung
~~~~~~~~~~~~~~~~~~~~~~~~~~

.. code-block:: xml
   :linenos:

   <?xml version="1.0" encoding="UTF-8" ?>
   <!DOCTYPE adressensammlung [
   <!ELEMENT adressensammlung (adresse+)>
   <!ELEMENT adresse (anrede,name,strasse,postanschrift,email?)>
   <!ELEMENT anrede (#PCDATA)>
   <!ELEMENT name (nachname,vorname)>
   <!ELEMENT postanschrift (plz,wohnort)>
   <!ELEMENT nachname (#PCDATA)>
   <!ELEMENT vorname (#PCDATA)>
   <!ELEMENT plz (#PCDATA)>
   <!ELEMENT wohnort (#PCDATA)>
   <!ELEMENT strasse (#PCDATA)>
   <!ELEMENT email (#PCDATA)>
   ]>

Für die Adressensammlung wird die Wiederholung einer Adresse erlaubt
(+). Die Angabe der email ist optional (?).
