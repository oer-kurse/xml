.. meta::

   :description lang=de: XML-Kurs mit den Themen: DTD, Schema, XSL, XPath
   :keywords: DTD, Aufgabe, OER


:ref:`« Start: DTD <dtd-start>`

.. index:: DTD; Komplexe Aufgabe

=====================
DTD: Komplexe Aufgabe
=====================

.. image:: ../images/flugverbot.jpg
   :width: 0px


.. |a| raw:: html

   <!-- lightbox container hidden with CSS -->
   <a href="#" class="lightbox" id="img1">
   <span style="background-image: url('../../_images/flugverbot.jpg')"></span>
   </a>

.. |b| raw:: html

   <a href="#img1">
   <img width="250px" src="../../_images/flugverbot.jpg">
   </a>

.. sidebar:: Flugverbot

   |b|

|a|


Lernziel
========

Hier lernen Sie nun anhand einer komplexen Aufgabe, die beschriebenen
Teilbereiche von DTDs und XML zu verbinden. Nach dem Lösen der Aufgabe
werden Sie sicher den Umgang mit XML und DTDs beherrschen.


Handlungsanweisungen
====================

:Aufgaben: Entwickeln Sie die DTD für den folgenden XML-Content:

           - artikelname
           - anzahl
           - beschreibung
           - wo gelagert (Raum1, Raum2 oder Raum3)
           - preis
           - hersteller
           - hersteller
           - email
           - bild
           - bildbeschreibung
           - Bei *wo gelagert* soll es nur die 3 Möglichkeiten geben.
             Der Preis muss immer angegeben werden. Es soll mindestens
             ein Datensatz vorhanden sein.
           - Entwicklen Sie die DTD und lagere sie diese in einer externen Datei aus. 
           - Tragen Sie mindestens **drei Datensätze** in die Datenbank ein.
           - Prüfen Sie die Gültigkeit (Validität) Ihrer XML-Dokumente mit dem XML-Spy!

             .. code-block:: xml

               <?xml version="1.0" encoding="UTF-8"?>
               <adressen_db>
                 <adresse nr="1">
                   <anrede>Herr</anrede>
                   <name>Hubert Taler</name>
                   <anschrift>
                     <strasse>Weinberg 1</strasse>
                     <plz>14777</plz>
                     <ort>Berlin</ort>
                   </anschrift>
                 </adresse>
                 <adresse nr="2">
                   <name>Uni Potsdam</name>
                   <anschrift>
                     <strasse>Am Park Babelsberg 3</strasse>
                     <plz>14482</plz>
                     <ort>Potsdam</ort>
                   </anschrift>
                 </adresse>
               </adressen_db>


.. note:: Es dürfen keine Leerzeichen bei festen Attributwerten gesetzt werden!
          Demnach kann man bei 'wo_gelagert' kein Leerzeichen zwischen Raum und der 
          Raumnummer setzen.
