==============================
Welche Elemente und Attribute?
==============================


.. index:: Elements

.. index:: Attributes

Frage?
------

Es existiert keine Document Type Definition (dtd) oder eine Schema-Datei (xsd). 
Wie erhalte ich eine Übersicht mit allen Elementen und Attributen?

Antwort
-------

Stylesheet anlegen
~~~~~~~~~~~~~~~~~~

Mit einem Stylesheet können die Angabe ausgelesen und tabellarisch dargestellt werden.

.. code:: xml


    <xsl:stylesheet version="1.0" 
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output method="html" omit-xml-declaration="yes" 
    version="1.0" encoding="utf-8" indent="yes"/>
    <xsl:strip-space elements="*"/>

    <xsl:template match="/">
        <table border="1">
    	<tr>
    	    <th>Element</th>
    	    <th>Attribute</th>
    	    <th>Value</th>
    	</tr>
    	<xsl:apply-templates select="*"/>
        </table>
    </xsl:template>

    <xsl:template match="*">
        <tr>
    	<td>
    	    <xsl:value-of select="local-name()"/>
    	</td>
    	<td/>
    	<td>
    	    <xsl:apply-templates select="text()"/>
    	</td>
        </tr>
        <xsl:apply-templates select="@*|*"/>
    </xsl:template>

    <xsl:template match="@*">
        <tr>
    	<td>
    	    <xsl:value-of select="local-name(..)"/>
    	</td>
    	<td>
    	    <xsl:value-of select="local-name()"/>
    	</td>
    	<td>
    	    <xsl:value-of select="."/>
    	</td>
        </tr>
    </xsl:template>

    </xsl:stylesheet>

XSL mit XML verknüpfen
~~~~~~~~~~~~~~~~~~~~~~

In folgendem Beispiel wird eine einfache Word-Datei analysiert.

.. code:: bash


    java -jar saxon9he.jar -o:struktur.html dokument.xml struktur.xsl

Testdateien
-----------


    :download:`Testdateien zum Download <files/struktur-analyse.zip>`

Das Ergebnis
------------

Ausschnitt der generierten Tabelle:

.. image:: ./files/struktur.png
