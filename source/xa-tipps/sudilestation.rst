.. meta::
    
   :description lang=de: XML-Kurs mit den Themen: DTD, Schema, XSL, XPath
   :keywords: XML, Tipps, Tricks, OER

:ref:`« Kurs-Start <xml-kurs-start>`
     
.. index:: CDATA
.. index:: Tipps und Tricks
	   
================
Tipps und Tricks
================

.. image:: ./werbung/kornfeld.jpg
   :width: 0px

.. |a| raw:: html

       <div class='hover_img'>
         <a href='#'>Kornfeld
         <span>
           <img src='../_images/kornfeld.jpg'
                alt='Kornfeld' />
          </span>
         </a>
        </div>

.. sidebar:: SQL-Kurs

	     | Statt der Werbung...
	     | |a|
	     | Serie: Kunst


Lernziel
========

Was sonst noch nicht erwähnt wurde, finden Sie vielleicht hier wieder.

CDATA Sections
===============

Angenommen, Sie wollen in einer XML-Datei wiederum XML- oder HTML-Quellcode speichern. 
Es läßt sich denken, dass es ohne eine besondere Deklarierung zu Fehlern kommen würde. 
Für solche Fälle gibt es CDATA Section. Das Ziel ist also, beliebige Zeichenfolgen, 
so auch Quellcode, in einer XML-Datei zu speichern. 
Schauen wir uns dazu den Quellcode an   :emphasize-lines: 6, 14:

.. code-block:: xml
   :linenos:

   <?xml version="1.0" encoding="UTF-8"?>
   <codesammlung>
    <beispiel nr="1">
     <beschreibung>Das ist das Grundgerüst einer HTML-Seite!</beschreibung>
       <quellcode>
         <![CDATA[
             <html>
             <head>
                 <Title>Dies ist der Titel</title>
             </head>
             <body>
             </body>
             </html>
            ]]>
      </quellcode>
    </beispiel>
   </codesammlung>
   
   
:Erklärungen:

Die hervorgehobenen Zeilen schließen definieren den  CDATA-Bereich, sie darf 
also selbst nicht im Text vorkommen. Hier kann nun nichtvaliedes XML, wie 
z.B. JavaScript eingefügt werden. 


.. index: DOCTYPE, HTML 5

DOCTYPE-Declaration für HTML 5
==============================
- Viele Beispiele zeigen nur HTML-Fragmente!
- Wer ein valides HTML-5-Dokument erzeugen will, 
  muss im Root-Element das folgende Konstrukt mit
  einbauen (siehe Zeile 9):


.. code-block:: xml
   :linenos:
   :emphasize-lines: 9

   <?xml version="1.0" encoding="UTF-8"?>
   <xsl:stylesheet version="2.0"
        xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
   <xsl:output method="html" />

   <xsl:template match="/">

   <!-- Ausgabe DOTYPE für HTML 5 -->
   <xsl:text disable-output-escaping="yes">&lt;!DOCTYPE html></xsl:text>
     <html>
     <head>
   	  <title>Eine HTML-5-Seite</title>
   	  <meta charset="UTF-8" />
     </head>
     <body>
     ...
     </body>
     </html>
   </xsl:template>
   <!-- weitere TemplateRules ... -->
   </xsl:stylesheet>

.. index:: XML-Deklaration; unterdrücken
   
XML-Declaration unterdrücken
============================
Wie kann die Ausgabe der ersten Zeile mit der XML-Deklaration
verhindert werden?


a) Umstellen der Ausgabe auf html

   .. code-block:: xml
      :emphasize-lines: 1   

      <xsl:output method="html"
                  indent="yes"
		  encoding="UTF-8" />

b) Verwendung von omit-xml-declaration
   
   .. code-block:: xml
      :emphasize-lines: 4   

       <xsl:output method="xm"
                   indent="yes"
	 	   encoding="UTF-8"
	 	   omit-xml-declaration="yes"/>

XPATH
=====

In Postgres XPath anwenden (ein Beispiel):

https://blog.sznapka.pl/using-xml-and-xpath-in-postgresql-database/
		  
   
XSD
===

Zwei XSD mergen
---------------
Wie können zwei XSD kombiniert werden?

- https://stackoverflow.com/questions/20046452/how-to-merge-more-than-one-xsd-file-to-one-xsd-file
