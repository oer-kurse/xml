.. meta::
    
   :description lang=de: XML-Kurs mit den Themen: DTD, Schema, XSL, XPath
   :keywords: XML, Security, OER

:ref:`« Kurs-Start <xml-kurs-start>`

.. index:: Security
     
=================
XSL -- Sicherheit
=================

.. image:: ../werbung/abfall-fastfood.jpg
   :width: 0px

.. |a| raw:: html

       <div class='hover_img'>
         <a href='#'>Fast-Food/Abfall
         <span>
           <img src='../../_images/abfall-fastfood.jpg'
                alt='Abfall -- FastFood' />
          </span>
         </a>
        </div>

.. sidebar:: SQL-Kurs

	     | Statt der Werbung...
	     | |a|
	     | Serie: Dinge
	     
Lernziel
========

Wie mit jeder Programmiersprache und den verwendeten Programmen, muss
man sich über die Angriffsvectoren und die "dunkle Seite der Macht"
gedanken machen, um eigene Daten, Programme und Rechenanlangen nicht
zu gefährden.

Siehe auch folgende Artikel:

- `Server Side Injection`_
- `XML-Security (W3C)`_
- `CVE-2012-5357 (Microsoft)`_
- `CVE-2012-1592 CVE-2012-1592 (Struts)`_
- `CVE-2005-3757 (Google Search Appliance)`_


.. _XML-Security (W3C):
   https://www.w3.org/standards/xml/security
.. _Server Side Injection:
   https://www.contextis.com/blog/xslt-server-side-injection-attacks

.. _CVE-2012-5357 (Microsoft): https://technet.microsoft.com/library/security/msvr12-016
.. _CVE-2012-1592 CVE-2012-1592 (Struts): http://seclists.org/bugtraq/2012/Mar/110:
.. _CVE-2005-3757 (Google Search Appliance): http://www.cvedetails.com/cve/cve-2005-3757

