.. meta::
    
   :description lang=de: XML-Kurs mit den Themen: DTD, Schema, XSL, XPath
   :keywords: XSL, Tools, Werkzeuge, OER

.. _xml-tools-start:

:ref:`« Kursstart <xml-kurs-start>`

.. index:: Tipps

==============
Tipps & Tricks
==============

.. image:: ./werbung/kornfeld.jpg
   :width: 0px

.. |a| raw:: html

       <div class='hover_img'>
         <a href='#'>Lama
         <span>
           <img src='../_images/kornfeld.jpg'
                alt='kornfeld' />
          </span>
         </a>
        </div>

.. sidebar:: XML-Kurs

	     | Statt der Werbung...
	     | |a|
	     | Serie: Kunst
     
.. toctree::
   :maxdepth: 1
   :glob:
      
   sudilestation
   hxe-xsl-struktur
   hxd-xsl-security/sudilestation
   struktur   
