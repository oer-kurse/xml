.. meta::

   :description lang=de: XML-Kurs mit den Themen: DTD, Schema, XSL, XPath, XQuery
   :keywords: XSL, XML, Projekte, OER

:ref:`« Kursstart <xml-kurs-start>`

.. index:: Anwendungsfälle

=================
 Anwendungsfälle
=================

.. image:: ./images/bovist.jpg
   :width: 0px


.. |a| raw:: html

   <!-- lightbox container hidden with CSS -->
   <a href="#" class="lightbox" id="img1">
   <span style="background-image: url('../../_images/bovist.jpg')"></span>
   </a>

.. |b| raw:: html

   <a href="#img1">
   <img width="250px" src="../../_images/bovist.jpg">
   </a>

.. sidebar:: Pilze

   |b|

   Bovist

|a|

XML ist in vielen Branchen und Szenarien im Einsatz. Die hier
genannten Beispiele erheben keinen Anspruch auf Vollständigkeit.

.. toctree::
   :maxdepth: 1
   :glob:

   *
