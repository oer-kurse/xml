.. meta::

   :description lang=de: Office-Dokumente werden in XML-Format gespeichert
   :keywords: odt, xml, standard

.. INDEX:: Office; XML-Formate
.. INDEX:: XML-Formate; Office
	      
	      
================
Textverarbeitung
================

.. image:: ./images/tintling.jpg
   :width: 0px


.. |a| raw:: html

   <!-- lightbox container hidden with CSS -->
   <a href="#" class="lightbox" id="img1">
   <span style="background-image: url('../../_images/tintling.jpg')"></span>
   </a>

.. |b| raw:: html

   <a href="#img1">
   <img width="250px" src="../../_images/tintling.jpg">
   </a>

.. sidebar:: Pilze

   |b|

   Tintlinge

|a|


Die Dateiformate docx und odt
-----------------------------

Sowohl LibreOffice als auch Word speichern die Daten im XML-Format!

- benennen Sie die Datei in ein zip um

- entpacken Sie die ZIP-Datei

- im neuen Ordner können Sie die einzelnen XML-Dateien öffnen

Beispiel einer Ordnerstruktur (odt/xml)
---------------------------------------

.. image:: ./files/odt-als-zip.png
