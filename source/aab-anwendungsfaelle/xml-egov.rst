.. meta::

   :description lang=de: XML-Kurs mit den Themen: DTD, Schema, XSL, XPath
   :keywords: XSL, eGov, XÖV

.. INDEX:: XML-Formate; XÖV (eGov)
.. INDEX:: XÖV (eGov); XML-Formate

	      
=======================
XML in den Verwaltungen
=======================

.. image:: ./images/schirmpilz.jpg
   :width: 0px


.. |a| raw:: html

   <!-- lightbox container hidden with CSS -->
   <a href="#" class="lightbox" id="img1">
   <span style="background-image: url('../_images/schirmpilz.jpg')"></span>
   </a>

.. |b| raw:: html

   <a href="#img1">
   <img width="250px" src="../_images/schirmpilz.jpg">
   </a>

.. sidebar:: Pilze

   |b|

   Schirmpilz

|a|


Lernziel
========

In den Verwaltungen wird viel mit XML verwaltet, abgesehen von
den vielen Word- und OpenOffice-Dateien, die im Innern auch nur
XML-Dokumente sind. Für den allgemeinen Datenaustausch in und zwischen den
Behörden, entwickelt der IT-Planungsrat und die Koordinierungsstelle
für IT-Standards, ganz spezielle XML-Dokumente. Das ist schon mal ein
lobenswerter Ansatz.

Handlungsanweisungen
====================

:Aufgaben:

- Folgen Sie den Links aus der Liste und informieren Sie sich.
 

Linkliste
=========

- `LegalDocML
  <https://plattform.egesetzgebung.bund.de/cockpit/#/ueberDasProjekt/legaldoc>`_

- `XUnternehmen
  <https://xunternehmen.de/>`_

- `XAusländer (XÖV-zertifiziert)
  <https://www.xoev.de/die_standards/xoev_standards_und__vorhaben/xauslaender_xoev_zertifiziert-11287>`_

- `XDomea (XÖV-zertifiziert)
  <https://www.xoev.de/die_standards/xoev_standards_und__vorhaben/xdomea_xoev_zertifiziert-11406>`_

- `XRechnung <https://github.com/itplr-kosit/xrechnung-artefacts>`_

- `XFall (XÖV-zertifiziert)
  <https://www.xoev.de/die_standards/xoev_standards_und__vorhaben/xfall_xoev_zertifiziert-11365>`_

- `XFinanz (XÖV-zertifiziert)
  <https://www.xoev.de/die_standards/xoev_standards_und__vorhaben/xfinanz_xoev_zertifiziert-11262>`_

- `XHamsterzucht (beispielhafte Umsetzung eines XÖV-Standards) <https://www.xoev.de/xoev_produkte/xhamsterzucht-2188>`_

- `Xhoheitliche Dokumente (XÖV-zertifiziert)
  <https://www.xoev.de/die_standards/xoev_standards_und__vorhaben/xhoheitliche_dokumente_xoev_zertifiziert-11334>`_

- `XAmtshilfe
  <https://www.xoev.de/xamtshilfe-14638>`_

- `XInneres
  <https://www.xoev.de/die_standards/xoev_standards_und__vorhaben/xinneres-11386>`_

- `XJustiz
  <https://www.xoev.de/die_standards/xoev_standards_und__vorhaben/xjustiz-11258>`_
  - `Browseranwendung <https://xjustiz.justiz.de/browseranwendungen/index.php>`_

- `XKatastrophenhilfe
  <https://www.xoev.de/die_standards/xoev_standards_und__vorhaben/xkatastrophenhilfe-11342>`_

- `XKfz (XÖV-zertifiziert)
  <https://www.xoev.de/die_standards/xoev_standards_und__vorhaben/xkfz_xoev_zertifiziert-11274>`_

- `XKind
  <https://www.xoev.de/die_standards/xoev_standards_und__vorhaben/xkind-11391>`_

- `XLeistung
  <https://www.xoev.de/die_standards/xoev_standards_und__vorhaben/xleistung-11352>`_

- `XMeld (XÖV-zertifiziert)
  <https://www.xoev.de/die_standards/xoev_standards_und__vorhaben/xmeld_xoev_zertifiziert-11251>`_

- `XNorm
  <https://www.xoev.de/die_standards/xoev_standards_und__vorhaben/xnorm-11380>`_

- `XÖGD
  <https://www.xoev.de/die_standards/xoev_standards_und__vorhaben/xoegd-11396>`_

- `XPersonenstand (XÖV-zertifiziert)
  <https://www.xoev.de/die_standards/xoev_standards_und__vorhaben/xpersonenstand_xoev_zertifiziert-11292>`_

- `XPersonenstandsregister
  <https://www.xoev.de/die_standards/xoev_standards_und__vorhaben/xpersonenstandsregister_xpsr-11401>`_

- `XPolizei
  <https://www.xoev.de/die_standards/xoev_standards_und__vorhaben/xpolizei-11268>`_

- `XStatistik (XÖV-zertifiziert)
  <https://www.xoev.de/die_standards/xoev_standards_und__vorhaben/xstatistik_xoev_zertifiziert-11280>`_

- `XUBetrieb (XÖV-zertifiziert)
  <https://www.xoev.de/die_standards/xoev_standards_und__vorhaben/xubetrieb_xoev_zertifiziert-11375>`_

- `XUKommunalabwasser (XÖV-zertifiziert)
  <https://www.xoev.de/die_standards/xoev_standards_und__vorhaben/xukommunalabwasser_xoev_zertifiziert-11370>`_

- `XWaffe (XÖV-zertifiziert)
  <https://www.xoev.de/die_standards/xoev_standards_und__vorhaben/xwaffe_xoev_zertifiziert-11347>`_

- `XZUFI (XÖV-zertifiziert)
  <https://www.xoev.de/die_standards/xoev_standards_und__vorhaben/xzufi_xoev_zertifiziert-11357>`_

- `XGewerbeanzeige (XÖV-Zertifizierung beantragt <http://www.xgewerbeanzeige.de/>`_

- `XPlanung
  <http://www.xplanungwiki.de/index.php?title=Xplanung_Wiki>`_

- `XBau
  <https://www.xoev.de/die_agenda/aktuelle_bedarfe/austauschstandards_im_bau__und_planungsbereich-12192>`_
