.. meta::

   :description lang=de: Notenstandards MNX und MusicXML
   :keywords: MNS und MusicXML

============================
Musik: Strukturen für  Noten
============================

.. image:: ./images/mnx-beispiel.png
   :width: 0px


.. |a| raw:: html

   <!-- lightbox container hidden with CSS -->
   <a href="#" class="lightbox" id="img1">
   <span style="background-image: url('../_images/mnx-beispiel.png')"></span>
   </a>

.. |b| raw:: html

   <a href="#img1">
   <img width="250px" src="../_images/mnx-beispiel.png">
   </a>

.. sidebar:: Beispiel

   |b|


|a|


MNX und MusicXML ein Vergleich
------------------------------
Musiker und Musikprogramme benötigen Noten für die Verarbeitung und/oder Aufführung.
Zwei auf XML basierende Notationen stehen zur die Speicherung und den Austausch zur
Verfügung.

- https://w3c.github.io/mnx/docs/
- https://w3c.github.io/mnx/docs/comparisons/musicxml/
