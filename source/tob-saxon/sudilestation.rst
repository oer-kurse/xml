.. _saxonhe:

.. meta::
    
   :description lang=de: XML-Kurs mit den Themen: DTD, Schema, XSL, XPath
   :keywords: XML, Werkzeuge, Tools, Saxon, XML-Parser, Java, OER

:ref:`« Übersicht Werkzeuge <xml-tools-start>`
     
.. index:: Werkzeuge; Saxon (java), Saxon
.. index:: Tools; Saxon (java)
.. index:: Parser; Saxon (xml/java)
.. index:: Saxon (xml/java); Parser
	   
============
Saxon (java)
============

.. image:: ./images/flechtwerk.jpg
   :width: 0px

.. |a| raw:: html

       <div class='hover_img'>
         <a href='#'>Flechtwerk
         <span>
           <img src='../../_images/flechtwerk.jpg'
                alt='Flechtwerk' />
          </span>
         </a>
        </div>

.. sidebar:: SQL-Kurs

	     | Statt der Werbung...
	     | |a|
	     | Serie: Dinge


Lernziel
========

Wenn Sie die Station durchgearbeitet haben, wissen Sie, wie Sie mit
einem externen Prozessor arbeiten.


Handlungsanweisungen
====================

:Aufgaben:

   1. Prüfen Sie, ob Java installiert und verfügbar ist!
   2. Laden sie den Saxon-Parser (java-Version) herunter.
   3. Entpacken Sie die Dateien und kopieren Sie die jar-Dateien in den Übungsordner.
 

.. index: Saxon, XSL-Prozessor

Transformation mit Saxon
~~~~~~~~~~~~~~~~~~~~~~~~

In den letzten Stationen haben Sie kennengelernt, wie Sie mithilfe des in den Browser 
eingebauten XSL-Prozessors HTML-Dateien erzeugen. Das Gleiche können Sie auch mit 
Saxon tun. Saxon ist ein XML-Prozessor für die Kommandozeile.

Download und Installation
~~~~~~~~~~~~~~~~~~~~~~~~~

Download/Entpacken/Installieren:  

`Download saxonHE (Github)
<https://github.com/Saxonica/Saxon-HE/tree/main/12/Java>`_

:Anmerkung: Für die einfache Handhabung, verschieben sie die 
            jar-Dateien der java-Version in den Übungsordner, 
            denn damit verkürzt sich der Aufruf und eine Konfiguration 
            des Systems ist überflüssig. 

Beispieldateien, mit denen Sie die unten stehenden Kommandos
ausprobieren können.

.. index:: Download; saxon-test.zip

:download:`Download: Übungsdateien für *saxonhe.jar*<files/saxon-test.zip>`

	  
Aufrufvarianten mit Ausgabe in der Shell
========================================
Java prüfen: Shell öffnen...
::

  cd  mein-ordner-mit-dem-xml-kram
  java -version
  
Wenn Sie keine Versionummer angezeigt bekommen:

- entweder Javaprogamm im System suchen/Systempfad prüfen/setzen
- oder Neuinstallation der Java-Runtime (JRE) 
  
::

  cd mein-ordner-mit-dem-xml-kram
  java -jar saxon9he.jar adressen.xml adressen.xsl

oder
::
   
  java -jar saxon9he.jar -o:test.html adressen.xml adressen.xsl

oder
::
   
  java -jar saxon9he.jar -o:test.html -s:adressen.xml -xsl:adressen.xsl


