.. meta::
    
   :description lang=de: XML-Kurs mit den Themen: DTD, Schema, XSL, XPath
   :keywords: XML, Python, OER

:ref:`« Übersicht Projekte <xsl-projekt-start>`
   
.. index:: Python

===========
XML: Python
===========

.. image:: ../images/warnung-vor-dem-hunde.jpg
   :width: 0px


.. |a| raw:: html

   <!-- lightbox container hidden with CSS -->
   <a href="#" class="lightbox" id="img1">
   <span style="background-image: url('../../_images/warnung-vor-dem-hunde.jpg')"></span>
   </a>

.. |b| raw:: html

   <a href="#img1">
   <img width="250px"
        alt='Warnung vor dem Hunde'
        src="../../_images/warnung-vor-dem-hunde.jpg">
   </a>

.. sidebar:: XML-Kurs

   |b|

   Serie: Schilder

|a|


Lernziel
========

Sie kennen schon den Aufbau von XML-Dateien und auch die Technik,
mittels XSL, Daten zu extrahieren und in neue Strukturen zu
überführen.

Leider lassen sich nicht alle Probleme mit XSL lösen, so liegt es nahe eine 
Programmiersprache zur Hilfe zu nehmen. Python bietet hier eine ganze Reihe 
von Möglichkeiten, von denen einige vorgestellt werden. Für den Einstieg 
benutzen wir die in jeder Python-Installation vorhandenen SAX und DOM-Module.

Handlungsanweisungen
====================

:Aufgaben:

Suchen Sie in Ihrer lokalen Dokumentation die Einstiegsseiten zum SAX und DOM-Parser. 
Erstellen Sie für Ihren Browser einen Lesezeicheneintrag zu diesen Seiten.

.. index:: SAX

SAX
===
SAX definiert  ein ereignisgesteuertes (event-driven) Interface zum Parsen von 
XML-Daten. Für die Benutzung von SAX muß eine Python-Klasse erstellt werden, 
welche ein spezielles Interface implementiert. Dieses Interface stellt dann 
Methoden zum Zugriff auf die Objekte in der XML-Datei zur Verfügung.

.. index:: DOM

DOM
===
Das Document Object Model (DOM)  definiert eine Baumstruktur (tree-based representation)
 für ein XML-Dokument.

.. index:: Tools (Python)

Übersicht verfügbarer XML-Tools
===============================

Folgende Liste zeigt eine Auswahl verfügbarer XML-Tools, fall die eingebauten XML-Module von Python
nicht ausreichen sollten.

+-----------------------+-----------------------------------+--------------------------------------+
| Name                  | Anwendungsgebiet                  | Autor				   |
+=======================+===================================+======================================+
| ElementTree           | XML middleware                    | Fredrik Lundh			   |
+-----------------------+-----------------------------------+--------------------------------------+
| GNOWSYS               | Topic map engines                 | GNOWSYS				   |
+-----------------------+-----------------------------------+--------------------------------------+
| GPS                   | XML middleware                    | Geir Ove Grønmo			   |
+-----------------------+-----------------------------------+--------------------------------------+
| LT PyXML              | XML middleware                    | Edinburgh Language Technology Group  |
+-----------------------+-----------------------------------+--------------------------------------+
| PyTREX                | XML validators                    | James Tauber			   |
+-----------------------+-----------------------------------+--------------------------------------+
| Pyana                 | XSLT engines                      | Brian Quinlan			   |
+-----------------------+-----------------------------------+--------------------------------------+
| Python XML package    | XML middleware                    | The Python XML-SIG		   |
+-----------------------+-----------------------------------+--------------------------------------+
| Pyxie                 | XML middleware                    | Sean McGrath			   |
+-----------------------+-----------------------------------+--------------------------------------+
| Redfoot               | RDF parsers                       | The Redfoot Team			   |
|                       | XML document DBS, 		    |					   |
+-----------------------+-----------------------------------+--------------------------------------+
| Sablotron             | XSLT engines                      | Ginger Alliance			   |
+-----------------------+-----------------------------------+--------------------------------------+
| Schematron            | XML validators                    | Rick Jelliffe			   |
+-----------------------+-----------------------------------+--------------------------------------+
| SemanText             | Topic map engines                 | Eric Freese			   |
+-----------------------+-----------------------------------+--------------------------------------+
| Skyron                | Data binding engines              | John Wilson			   |
+-----------------------+-----------------------------------+--------------------------------------+
| TmTk                  | Topic map engines                 | Jan Algermissen			   |
+-----------------------+-----------------------------------+--------------------------------------+
| XML Schema Validator  | XML validators                    | Henry Thompson, Richard Tobin	   |
+-----------------------+-----------------------------------+--------------------------------------+
| dtddoc                | DTD documenters                   | Lars Marius Garshol		   |
+-----------------------+-----------------------------------+--------------------------------------+
| lxml                  | XML parsers          		    |					   |
+-----------------------+-----------------------------------+--------------------------------------+
| maki                  | Web publishing                    | Sam Brauer			   |
+-----------------------+-----------------------------------+--------------------------------------+
| pysp                  | SGML/XML parsers                  | Lars Marius Garshol		   |
+-----------------------+-----------------------------------+--------------------------------------+
| tmproc                | Topic map engines                 | Geir Ove Grønmo			   |
+-----------------------+-----------------------------------+--------------------------------------+
| xmlarch               | Architectural forms engines       | Geir Ove Grønmo			   |
+-----------------------+-----------------------------------+--------------------------------------+
| xmldiff               | XML document management utilities | Alexandre Fayolle			   |
+-----------------------+-----------------------------------+--------------------------------------+
| xmlproc               | XML parsers, DTD parsers          | Lars Marius Garshol		   |
+-----------------------+-----------------------------------+--------------------------------------+
| xmltools              | XML editors                       | Alexandre Fayolle			   |
+-----------------------+-----------------------------------+--------------------------------------+
  			
