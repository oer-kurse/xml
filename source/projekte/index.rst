.. meta::

   :description lang=de: XML-Kurs mit den Themen: DTD, Schema, XSL, XPath, XQuery
   :keywords: XSL, XML, Projekte, OER

.. _xsl-projekt-start:

:ref:`« Kursstart <xml-kurs-start>`

.. index:: Projekte; Übersicht

==========
 Projekte
==========

.. image:: ./images/heissluft-ballon.jpg
   :width: 0px


.. |a| raw:: html

   <!-- lightbox container hidden with CSS -->
   <a href="#" class="lightbox" id="img1">
   <span style="background-image: url('../_images/heissluft-ballon.jpg')"></span>
   </a>

.. |b| raw:: html

   <a href="#img1">
   <img width="250px" src="../_images/heissluft-ballon.jpg">
   </a>

.. sidebar:: Heißluftballon

   |b|

|a|

Sie können mit den folgenden Projekten und Beispielen das Wissen festigen.

Handlungsanweisungen
====================

:Aufgaben:

  1. Suchen Sie sich bitte eines der Projekte aus!
  2. Variieren Sie die Aufgabenstellung.
  3. Entwickeln Sie eine eigene Aufgabe, wenn Sie eine eigenen
     XML-Struktur verarbeiten wollen.
  4. Alternativ zu Punkt 3 können Sie sich eine XML-Struktur im
     Internet suchen und damit experimentieren.
  5. ...


.. toctree::
   :maxdepth: 1
   :glob:

   */*
