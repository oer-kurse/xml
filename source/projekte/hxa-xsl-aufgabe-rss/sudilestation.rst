.. meta::
    
   :description lang=de: XML-Kurs mit den Themen: DTD, Schema, XSL, XPath
   :keywords: XSL, RSS, OER

:ref:`« Übersicht Projekte <xsl-projekt-start>`

.. index:: Projekte; RSS transformieren
	   
=======================
XSL: RSS transformieren
=======================



.. image:: ../images/heisse-kohlen.jpg
   :width: 0px

.. |a| raw:: html

   <!-- lightbox container hidden with CSS -->
   <a href="#" class="lightbox" id="img1">
   <span style="background-image: url('../../_images/heisse-kohlen.jpg')"></span>
   </a>

.. |b| raw:: html

   <a href="#img1">
   <img width="250px"
        alt="Heiße Kohlen"
	src="../../_images/heisse-kohlen.jpg">
   </a>

.. sidebar:: XML-Kurs

   |b|

   Heiße Kohlen

|a|


Lernziel
========

An einem konkreten Beispiel, soll das bisher erworbene Wissen noch
einmal angewendet werden.


Handlungsanweisungen
====================

:Aufgaben:  RSS und Podcasting:

  .. image:: ./files/rss.jpg
     :width: 60 px

  1. Informieren Sie sich bitte zum Thema unter dem Link:
     http://de.wikipedia.org/wiki/Rss
  2. Viele Seiten bieten eine Übersicht zu den neuesten Artikeln als RSS-Feed an.
  3. Deutschland-Radio bietet den Hörern die Möglichkeit, Sendungen im
     Internet nach zuhören.
     
     Dazu werden die Sendungen in Form einer RSS-Datei bereitgestellt. 
     Diese kann man in einem geeigneten RSS-Reader abonieren und anschauen. 
     Dort findet man auch einen Download-Link zu einer mp3-Datei, durch die man sich 
     die Sendung anhören kann.
     Schauen Sie sich bitte die RSS-Datei von Deutschland- Radio an.  
     Lassen Sie sich dazu den Quellcode der folgenden Internetseite anzeigen: 
     http://www.dradio.de/rss/podcast/sendungen/hiwi/
  4. Installieren Sie einen RSS-Reader (ein Programm zum Anzeigen der RSS-Dateien) 
     und lassen Sie sich die o.g. RSS-Datei im RSS-Reader anzeigen. 
     Einen geeigneten RSS-Reader finden Sie hier: http://www.rssowl.org/download
     Auch der Browser kann für das Abonnement verwendet werden.

  5. Schreiben Sie nun eine XSL-Datei, welche die RSS-Datei von Deutschland-Radio 
     einließt und nach HTML transformiert. 
     Lassen Sie also das Ergebnis im Browser darstellen. 
  6. Durch einen Klick soll man zur entsprechenden MP3-Datei gelangen.
  7. Benutzen Sie den Saxon-Transformator, um die XML-Datei in eine lokal gespeicherte 
     HTML-Seite zu transformieren. 

:Hinweise:

  1. Sie können die RSS-XML-Datei auf den Seiten von Dradio mit Saxon direkt angeben:
     ::

       saxon http://www.dradio.de/rss/podcast/sendungen/hiwi/ meine_transformationen.xsl

  2. Um den CDATA-Bereich zu transformieren benutzen Sie bitte:
     ::

       <xsl:value-of select="description" disable-output-escaping="yes"/>     

     Durch disable-output-escaping="yes" werden die HTML-Tags nicht umschrieben 
     (z.B. &lt;hr /&gt;), sondern direkt ausgegeben (z.B.: <hr />).

  3. Wenn die Umlaute evtl. nicht richtig dargestellt werden, können Sie dem Browser
     einen Hinweis auf die verwendete Codierung geben und auf  UTF-8 umzustellen:
     ::

       <meta http-equiv="content-type" content="text/html;charset=utf-8" />
     
     Tragen sie die Zeile bitte im *header* der HTML-Seite ein.
