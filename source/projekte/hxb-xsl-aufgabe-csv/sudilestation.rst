.. meta::
    
   :description lang=de: XML-Kurs mit den Themen: DTD, Schema, XSL, XPath
   :keywords: XSL, CSV, OER

:ref:`« Übersicht Projekte <xsl-projekt-start>`

.. index:: Projekte; CSV

=================
XSL: CSV erzeugen
=================

.. image:: ../images/hund-aegypten.jpg
   :width: 0px


.. |a| raw:: html

   <!-- lightbox container hidden with CSS -->
   <a href="#" class="lightbox" id="img1">
   <span style="background-image: url('../../_images/hund-aegypten.jpg')"></span>
   </a>

.. |b| raw:: html

   <a href="#img1">
   <img width="250px"
        alt="Hund aus der Unterwelt"
        src="../../_images/hund-aegypten.jpg">
   </a>

.. sidebar:: XML-Kurs

   |b|

   Hund aus der
   Unterwelt 

|a|


Lernziel
========

An einem konkreten Beispiel, soll das bisher erworbene Wissen noch
einmal angewendet werden.


Handlungsanweisungen
====================
Konvertieren Sie ausgewählte Inhalte einer XML-Datei in das
CSV-Format.

:Aufgaben:  CSV-Datei erzeugen

  1. Machen sie sich mit der XML-Datei vertraut.
  2. Konvertieren Sie die XML-Struktur in eine csv-Datei, damit sie in
     Excel oder LibreOffice importiert werden kann.
  3. Selektieren Sie nach unterschiedlichen Kriterien 


