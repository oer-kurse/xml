==========
XML zu PDF
==========


.. index:: PDF; Speedata

.. index:: Speedata; PDF

.. index:: Projekte; PDF (Speedata)

.. index:: PDF (Speedata); Projekte

Speedata
--------

Es gibt viele Möglichkeiten ein PDF zu generieren. Die bekannteste
OpenSource-Lösung ist Latex oder der Export über LibreOffice. Was
diesen Lösungen oft fehlt sind die kleinen Dinge:

- Anbindung einer Datenbank (Trennung Daten vom Layout)

- manuelles justieren nach dem Einlesen der Daten

- erlernen einer neuen Auszeichnungssprache

Weil in diesem Kurs die vielfältigen Möglichkeiten für XML gezeigt
werden, soll hier die Transformation von XML zum einem PDF
demonstriert werden.

Wir zeigen das an folgenedne Beispielen:

- ☑ die PC-Liste, die für die Transformationsübungen benutzt
  wurde, soll direkt in ein PDF verwandelt werden

  - ☑ Das geht noch bunter und schöner...

  - ☑ Pro Raum eine Tabelle/Seite

- ☐ Ein Rechnungsbeispiel 

  - ☐ normale Rechung

  - ☐ Rechnung als barrierefreies PDF

  - ☐ Rechnung für die automatische Verarbeitung nach dem
    Zugpferd-Standard (XRechnung)
