===========================
Eine Seite/Tabelle pro Raum
===========================


.. index:: Speedata; Tabelle

.. index:: Tabelle; Speedata

Ausgabe einer Seite für jeden Raum
----------------------------------

Nun brauchen wir eine Seite für jeden Raum und nutzen dafür eine
Technik die wir mit XSL ähnlich nutzen würden.
Zusätzlich wird die Raumbezeichnung als Überschrift ausgegeben.
Die Formatierungen wie z.B. Farbe und Schriftgröße muss natürlich
definiert werden. Alle Angaben sind dem Handbuch entnommen.

.. code-block:: xml


    <Layout xmlns="urn:speedata.de:2009/publisher/en"
    	xmlns:sd="urn:speedata:2009/publisher/functions/en">

      <DefineTextformat name="keeptogether" break-below="no"/>

      <DefineFontfamily name="h1" fontsize="18" leading="20">
        <Regular fontface="sans-bold" />
      </DefineFontfamily>

      <Record element="pcliste">
        <ProcessNode select="*"/>
      </Record>

      <Record element="raum">
        <Output>
          <Text>
    	<Paragraph fontfamily="h1"
    		   textformat="keeptogether"
    		   color="blue">
    	  <Value select="@bezeichnung"/>
    	</Paragraph>
          </Text>
        </Output>
        <PlaceObject>
          <Table stretch="max"> 
    	<Tablehead> 
    	  <Tr backgroundcolor="lightgray">
    	    <Td>
    	      <Paragraph><Value>Rechner-Name</Value></Paragraph>
    	    </Td>
    	    <Td>
    	      <Paragraph><Value>Rechner-IP</Value></Paragraph>
    	    </Td>
    	    <Td>
    	      <Paragraph><Value>Anmerkung</Value></Paragraph>
    	    </Td>
    	  </Tr>
    	</Tablehead>
    	<ProcessNode select="*"/>
    	<ForAll select="rechner">
    	  <Tr>
    	    <Td>
    	      <Paragraph><Value select="rechnername"/></Paragraph>
    	    </Td>
    	    <Td>
    	      <Paragraph><Value select="rechnerip"/></Paragraph>
    	    </Td>
    	    <Td>
    	      <Paragraph><Value select="anmerkung"/></Paragraph>
    	    </Td>
    	  </Tr>
    	</ForAll>
          </Table>
        </PlaceObject>
        <NewPage />
      </Record>
    </Layout>

Das PDF mit der ersten Seite und weiteren Vorschaubildern. 

.. image:: ./seiten-mit-ueberschrift.png
