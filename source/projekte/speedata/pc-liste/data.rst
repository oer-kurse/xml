===================
Daten bereitstellen
===================




Daten für Speedata
------------------

.. index:: Speedata; Rohdaten

.. index:: Rohdaten; Speedata


Weil xml-Dateien selbst als textbasierte Datenbank betrachtet
werden können, ist der erste Schritt zum PDF schnell erledigt.

.. code-block:: xml


    <?xml version="1.0" encoding="utf-8" ?>
    <?xml-stylesheet href="pcliste7.xsl" type="text/xsl" ?>
    <!DOCTYPE pcliste SYSTEM "pcliste.dtd">
    <pcliste>
      <raum bezeichnung="Schulungsraum 1">
        <!-- Reihe 1 in Blickrichtung Dozentenplatz gesehen -->
        <!-- R1 = Reihe Fx gleich Platz vom Fenster gezaehlt -->
        <rechner>
          <anmerkung status="Schrott">Dozentenrechner</anmerkung>
          <rechnername>R2C19.ffm.e-cademy.de</rechnername>
          <rechnerip>192.168.64.37</rechnerip>
        </rechner>

        viele Zeilen und das Ende der Datei mit dem letzte Rechner...

        <rechner>
          <anmerkung>IHK3C14W98</anmerkung>
          <rechnername>IHK3C14W98.e-cademy.de</rechnername>
          <rechnerip>192.168.4.12</rechnerip>
        </rechner>
      </raum>
    </pcliste>

Die einzige Anpassung, die gemachte werden muss, ist das Löschen
der ersten drei Zeilen, so daß das Root-Element die erste Zeile
in der Datei ist. 
Damit speedata die Daten auch findet muss die Datei zwingend
*data.xml* benannt werden.

Ausgabeformatierung
-------------------

.. index:: Speedata; Tabellenausgabe

.. index:: Tabellenausgabe; Speedata


Neben den neuen Schlüsselworten, benötigen wir noch die passen
XPath-Ausdrücke und die erste tabellarische Ausgabe ist vollendet.

.. code-block:: xml


    <Layout xmlns="urn:speedata.de:2009/publisher/en"
      xmlns:sd="urn:speedata:2009/publisher/functions/en">

      <Record element="pcliste">
        <PlaceObject>
          <Table stretch="max"> ①
    	<Tablehead> ②
    	  <Tr backgroundcolor="gray">
    	    <Td>
    	      <Paragraph><Value>Rechner-Name</Value></Paragraph>
    	    </Td>
    	    <Td>
    	      <Paragraph><Value>Rechner-IP</Value></Paragraph>
    	    </Td>
    	    <Td>
    	      <Paragraph><Value>Anmerkung</Value></Paragraph>
    	    </Td>
    	  </Tr>
    	</Tablehead>
    	<ForAll select="raum/rechner"> ③
    	  <Tr>
    	    <Td>
    	      <Paragraph><Value select="rechnername"/></Paragraph>
    	    </Td>
    	    <Td>
    	      <Paragraph><Value select="rechnerip"/></Paragraph>
    	    </Td>
    	    <Td>
    	      <Paragraph><Value select="anmerkung"/></Paragraph>
    	    </Td>
    	  </Tr>
    	</ForAll>
          </Table>
        </PlaceObject>
      </Record>
    </Layout>

Übersetzungslauf
----------------

Für das generieren des PDF genügen folgende Schritte,
vorausgesetzt sp (das Kürzel für das Programm speedata) ist im
Suchpfad zu finden.

.. code-block:: bash


    cd projektordner
    ls
    data.xml layout.xml
    sp
    ls
    data.xml    publisher-aux.xml   publisher.pdf       publisher.status
    layout.xml  publisher.finished  publisher.protocol  publisher.vars

Noch nicht sehr schön, aber die grundsätzliche Funktion ist damit
schon mal gegeben.

.. image:: ./speedata.png
