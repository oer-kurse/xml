.. meta::

   :description lang=de: XML, Publishing, Speedata
   :keywords: Speedata, XML, PDF

:ref:`« Kursstart <xml-kurs-start>`

.. _xsl-start:

.. index:: XSL; Übersicht

.. image:: ./werbung/muehlstein.webp
   :width: 0px

.. |a| raw:: html

       <div class='hover_img'>
         <a href='#'>Verschneiter Mühlstein
          <span>
           <img src='../../_images/muehlstein.webp'
                alt='Mühlstein verschneit' />
          </span>
         </a>
        </div>

.. sidebar:: XML-Kurs

	     | Statt der Werbung...
	     | |a|
	     | Serie: Kunst

	     
====================
Mit Speedata zum PDF
====================

     
.. toctree::
   :maxdepth: 1
   :glob:

   *   
   */*
