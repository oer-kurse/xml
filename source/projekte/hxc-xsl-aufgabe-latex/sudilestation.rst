.. meta::
    
   :description lang=de: XML-Kurs mit den Themen: DTD, Schema, XSL, XPath
   :keywords: XSL, Latex, PDF, OER

:ref:`« Übersicht Projekte <xsl-projekt-start>`

.. index:: Projekte; PDF (Latex)
.. index:: PDF (Latex)

==================
XSL: PDF  erzeugen
==================

.. image:: ../images/latex-beispiele.png
   :width: 0px


.. |a| raw:: html

   <!-- lightbox container hidden with CSS -->
   <a href="#" class="lightbox" id="img1">
   <span style="background-image: url('../../_images/latex-beispiele.png')"></span>
   </a>

.. |b| raw:: html

   <a href="#img1">
   <img width="250px"
        alt="Latex Beispiele"
	src="../../_images/latex-beispiele.png">
   </a>

.. sidebar:: XML-Kurs

   |b|

   Beispiele: Latex

|a|


	     
Lernziel
========

An einem konkreten Beispiel, soll das bisher erworbene Wissen noch
einmal angewendet werden.

Handlungsanweisungen
====================

:Aufgaben:  PDF-Datei erzeugen

1. Machen sie sich mit der XML-Datei vertraut.
   
   :download:`Download der Messdaten (re. Mausklick und mit der Dateiendung xml speichern!)  <files/mdkurz.txt>`

2. Konvertieren Sie die XML-Struktur in eine Latex-Datei.
3. Erzeugen Sie anschließend aus der Latex-Datei ein PDF-Dokument
4. Selektieren Sie nach unterschiedlichen Kriterien, um nur Teile der XML-Datei 
   auszugeben.
5. Wenden Sie die Transformation auf die "lange Version" der Messdaten an. 
   Die Daten finden sie im Index über den Begriff: *Beispieldatei: Messdaten (4.6 MB)*
 
Latex Grundlagen - kürzestmögliche Fassung!
===========================================
Latex ist ein Textformat, aus dem PDF-Dateien generiert werden können.

- Zeilen die mit % beginnen, sind Kommentare
- Zeilen die mit \ beginnen, sind Steueranweisungen/Befehle 
- Parameter werden in {} gesetzt
- Optionen werden in [] eingeschlossen
- die Dateierweiterung ist .tex

Grundgerüst für eine Leere Seite
================================

Mit vielen Kommentaren.

.. warning:: Auf einem Mac-Rechner gibt es andere Anweisungen für die Konfiguration der
          dt. Umlaute. Beachten Sie die entsprechenden Kommentare im Text.

.. code-block:: latex
   :linenos:

   %%%================%%%
   %%%  LaTeX-Gerüst  %%%  
   %%%================%%%
   
   %%
   %%  Nach Bedarf auskommentieren oder Kommentare entfernen
   %%
   
   
   
   %%
   %% Genau eine Dokumentenklasse wählen: 
   %%
   \documentclass[12pt,a4paper]{article}
   %\documentclass[11pt,a4paper]{article}   
   %\documentclass[10pt,a4paper]{article}
   %
   %\documentclass[12pt,a4paper]{book}   
   % 
   %\documentclass{letter}
   %...
   
   % Windows/Linux 
   %\usepackage{german}      % Deutsche TeX-Eigenheiten
   %\usepackage{isolatin1}   % Eingabekodierung mit Umlauten windows/linux...
   %\usepackage{isolatin1}   % Eingabekodierung mit Umlauten 
   
   %% MAC-spezifische Konfiguration für die dt. Sprache 
   \usepackage[applemac]{inputenc} 
   \usepackage[T1]{fontenc}
   \usepackage[ngerman]{babel}
   \usepackage{eurosym}
   
   %%
   %% Index-Erstellung
   %%
   \usepackage{makeidx}
   \makeindex            % damit eine Indexdatei angelegt wird
   
   
   %%
   %% AMS-LaTeX Pakete:
   %%
   \usepackage{amsmath}  % allgemeine Mathe-Erweiterungen
   \usepackage{amssymb}  % Symbole und Schriftarten
   \usepackage{amsthm}   % erweiterte Theorem-Umgebungen
   
   
   %%
   %% XY-Pic für Diagramme etc
   %%
   %\usepackage[all]{xy}      % Das Paket mit allem, was man so braucht
   %\UseComputerModernTips    % Pfeilspitzen wie im normalen Mathe-Modus
   %\CompileMatrices          % Damit geht es etwas schneller.
   
   
   \usepackage{mathrsfs}  % gibt den Befehl "\mathscr{}" für schöne
                          % Mathe-Skript-Buchstaben 
   
   
   
   
   %%
   %% Für den Titel:
   %%
   %\author{G. Harder}
   %\title{}
   %\date{}   % Standard: Datum der Kompilierung ("\today").
   
   %%
   %% Für lange Tabellen, die über mehrere Seiten gehen können.
   %%
   %\usepackage{longtable}

   
   %%%--------------------------%%%
   %%%  Gleich geht es los...   %%%
   %%%--------------------------%%%
   \begin{document}
   %%
   %% Titel:
   %%
   %% * Entweder Standardtitel mit obigen "\title" und "\author"
   %\maketitle
   %% * oder alternativ eine frei formatierte Titelseite:
   %\begin{titlepage}
   %\end{titlepage}
   
   %%
   %% Zusammenfassung:
   %%
   %\begin{abstract}
   %\end{abstract}
   
   %%
   %% Inhaltsverzeichnis:
   %%
   \tableofcontents
   
   %%%-------------------------------%%%
   %%%  Jetzt geht es richtig los:   %%%
   %%%-------------------------------%%%
   
   Inhalt, ohne Ende ...
   
   und eine Mustertabelle für die Ausgabe...
   \begin{longtable}{|l|r|c|p{2cm}|}
   \hline
   Linksbündige Spalte. & Rechtsbündige Spalte & Zentrierte Spalte & Parbox\\
   \hline
   Kurzer Text. & Kurzer Text. & Kurzer Text. & Kurzer Text.\\
   \hline
   Text. & Text. & Text. & Text.\\
   \hline
   Text. & Text. & Text. & Text.\\
   \hline
   \end{longtable}

   %%%----------------------------%%%
   %%%  Jetzt geht es zu Ende...  %%%
   %%%----------------------------%%%
   
   %%
   %%  Bibliographie
   %%
   
   
   
   
   %%
   %% Stichwortverzeichnis:
   %%
   %\renewcommand{\indexname}{Stichworte}  % Soll der "Index" anders heißen?
   %\printindex                            % Stichwortverzeichnis ausgeben.
   
   \end{document}
   %%%%%%%%%%%%%%
   %%%  Ende  %%%
   %%%%%%%%%%%%%%
   
   
Die fertige xml-to-latex.xsl
============================

Weil es eine lange Datei ist, hier zum
Download:

:download:`Das Stylesheet xml-to-latex.xsl <files/xml-to-latex.txt>` (Speichern mit der Endung XSL)

Der Aufruf könnte wie folgt aussehen:
::

    java -jar saxon9he -o: messdaten.tex mdkurz.xml xml-to-latex.xls 

Im Erfolgsfall könne Sie mit einem Latexprogramm (Mac/Techshop, Windows/Miktex, 
Linux/latex-Paket) ein PDf erzeugen. Falls es nicht gelingt, finden
Sie hier eine fertige Version zum

Download:

:download:`messdaten.pdf <files/messdaten.pdf>`

.. index: Namensräume

Erläuterungen zum xsl-Quelltext
===============================
In der der Datei mdkurz.xml werden vier Namensräume definiert: s,dt,
rs, z davon werden aber nur zwei:  *rs* für das Element data und z für
das Element *row* verwendet.
Um den Inhalt eines Elementes mit Namensraumangabe verwenden zu können
müssen auch  in der XSL-Datei, die Namensräume der XML-Datei bekannt
gemacht werden (Zeilen: 3-4).

.. code-block:: xml
   :linenos:

   <xsl:stylesheet 
        xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
        xmlns:rs='urn:schemas-microsoft-com:rowset'
        xmlns:z='#RowsetSchema'> 

Das Ausgabeformat muss für Latex auf *text* umgestellt werden.
::

  <xsl:output method="text"/>

Im Anschluss an diese Anpassungen folgen viele Zeilen Latex-Quellcode ...
Zu guter Letzt werden in einer Template-Rule alle Zeilen aus der
XML-Struktur in eine Tabellen-Zeile für das Satzsystem Latex
gewandelt. Die Besonderheit ist hier das Ampersand oder Kaufmanns-Und,
welches im XML-/XSL-Standard eine Sonderfunktion hat.
Es muss also kodiert ausgegeben werden.
::

  <xsl:text>&#38;</xsl:text> 

Ein Zeilenende wird wie folgt erzeugt:
::

  <xsl:text>&#xA;</xsl:text>

Wer mehr zum Thema Latex wissen möchte, findet über seine
Lieblings-Suchmaschine, viele Tutorials.




 

   
   
   
   
