.. meta::
    
   :description lang=de: XML-Kurs mit den Themen: DTD, Schema, XSL, XPath
   :keywords: XSL, XML-Dom, OER

:ref:`« Übersicht Projekte <xsl-projekt-start>`
     
.. index:: Python; DOM
   
===============
Python: XML-Dom
===============

.. image:: ../images/beeren-im-herbst.jpg
   :width: 0px


.. |a| raw:: html

   <!-- lightbox container hidden with CSS -->
   <a href="#" class="lightbox" id="img1">
   <span style="background-image: url('../../_images/beeren-im-herbst.jpg')"></span>
   </a>

.. |b| raw:: html

   <a href="#img1">
   <img width="250px"
        alt='Beeren im Herbst'
	src="../../_images/beeren-im-herbst.jpg">
   </a>

.. sidebar:: XML-Kurs

   |b|

   Serie: Pflanzen 

|a|

Lernziel
========

Neben dem SAX-Parser können Sie auch einen DOM-Parser zur Bearbeitung
von XML-Dateien einsetzen. Das XML-Dokument wird als Baum-Struktur im
Speicher gehalten. Im Unterschied zum SAX-Parser kann der DOM-Baum
manipuliert werden, z.B. durch hinzufügen, entfernen oder ändern von
Elementen und  Attributen.

Aufgaben
========

1. Erstellen Sie mit Hilfe des minidom-Modules eine eigene XML-Datei.
2.  Speichern Sie diese nach der Erstellung in einer Datei ab.

Beispiele zum Öffnen einer Datei
================================
Für die Bearbeitung einer XML-Datei muß diese geöffnet und eingelesen werden.
Die Beispiele mit *dom1* und *dom2* benutzen Dateien, *dom3* erhält ein neues 
XML-Dokument aus einer Zeichenkette.

.. code-block:: python
   :linenos:

   from xml.dom.minidom import parse, parseString
   
   dom1 = parse('c:\\temp\\mydata.xml') 
   
   datasource = open('c:\\temp\\mydata.xml')
   dom2 = parse(datasource)   
   
   dom3 = parseString('<myxml>Some data<empty/> some more data</myxml>')
   

Ausgabe der Teile eines DOM
===========================
Hier ein Beispiel, wie ein XML-Baum ausgegeben werden kann.

.. code-block:: python
   :linenos:


   import xml.dom.minidom
   
   document = """\
   <slideshow>
   <title>Demo slideshow</title>
   <slide><title>Slide title</title>
   <point>This is a demo</point>
   <point>Of a program for processing slides</point>
   </slide>
   
   <slide><title>Another demo slide</title>
   <point>It is important</point>
   <point>To have more than</point>
   <point>one slide</point>
   </slide>
   </slideshow>
   """
   
   dom = xml.dom.minidom.parseString(document)
   
   def getText(nodelist):
       rc = ""
       for node in nodelist:
           if node.nodeType == node.TEXT_NODE:
               rc = rc + node.data
       return rc
   
   def handleSlideshow(slideshow):
       print "<html>"
       handleSlideshowTitle(slideshow.getElementsByTagName("title")[0])
       slides = slideshow.getElementsByTagName("slide")
       handleToc(slides)
       handleSlides(slides)
       print "</html>"
   
   def handleSlides(slides):
       for slide in slides:
           handleSlide(slide)
   
   def handleSlide(slide):
       handleSlideTitle(slide.getElementsByTagName("title")[0])
       handlePoints(slide.getElementsByTagName("point"))
   
   def handleSlideshowTitle(title):
       print "<title>%s</title>" % getText(title.childNodes)
   
   def handleSlideTitle(title):
       print "<h2>%s</h2>" % getText(title.childNodes)
   
   def handlePoints(points):
       print "<ul>"
       for point in points:
           handlePoint(point)
       print "</ul>"
   
   def handlePoint(point):
       print "<li>%s</li>" % getText(point.childNodes)
   
   def handleToc(slides):
       for slide in slides:
           title = slide.getElementsByTagName("title")[0]
           print "<p>%s</p>" % getText(title.childNodes)
   
   handleSlideshow(dom)
   
   
   
   
