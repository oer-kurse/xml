.. meta::
    
   :description lang=de: XML-Kurs mit den Themen: DTD, Schema, XSL, XPath
   :keywords: XSL, MindMap, OER

:ref:`« Übersicht Projekte <xsl-projekt-start>`

.. index:: Projekte; Mindmap
	  
==================
MindMap generieren
==================

.. image:: ../images/wasseramsel.jpg
   :width: 0px


.. |a| raw:: html

   <!-- lightbox container hidden with CSS -->
   <a href="#" class="lightbox" id="img1">
   <span style="background-image: url('../../_images/wasseramsel.jpg')"></span>
   </a>

.. |b| raw:: html

   <a href="#img1">
   <img width="250px"
        alt='Wasseramsel'
	src="../../_images/wasseramsel.jpg">
   </a>

.. sidebar:: XML-Kurs

   |b|

   Serie: Vögel

|a|

Lernziel
========

Sie können nun durch die Projekte Ihr Wissen festigen.

Handlungsanweisungen
====================

:Aufgaben:

1. Entwickeln Sie nun aus einer Freemind-XML-Datei eine hierarchische Liste in HTML. 
   Hier können Sie sich die Freemind-Datei (XML) herunterladen: 

   :download:`Download Mindmap <./files/sonnensystem-mindmap.mm>`

   Achten Sie bitte auf die Links, die in der Freemind-XML-Datei enthalten sind. 
   Diese sollen auch in der Liste anklickbar sein.
2. Entwickeln Sie bitte zu der Freemind-XML-Datei
   (sonnenssystem-mindmap.mm) eine DTD oder ein Schema.
     

Freemind-Mindmapping
====================

Freemind ist ein Programm zum Erzeugen von MindMaps
(`Wikipedia: FreeMind <http://de.wikipedia.org/wiki/FreeMind>`_).

Das Programm ist kostenlos und speichert die MindMaps als XML-Datei. 
Die Grafik einer MindMap könnte so aussehen:

.. image:: ./files/mindmap.png

Der XML-Code sieht so aus (Ausschnitt):
::

   <map version="0.8.0">
   <!-- To view this file, download 
        free mind mapping software FreeMind 
        from http://freemind.sourceforge.net 
   -->
   <node CREATED="1163083123203" 
         ID="Freemind_Link_1590057021" 
         MODIFIED="1163083213296" 
         TEXT="Sonne">
   <node CREATED="1163083271125" 
         ID="Freemind_Link_1243080833" 
         MODIFIED="1163083328890" 
         POSITION="right" TEXT="Merkur" VSHIFT="-14"/>
   <node CREATED="1163083271125" HGAP="22" 
         ID="Freemind_Link_1862849771" MODIFIED="1163083272218" 
         POSITION="right" TEXT="Venus" VSHIFT="-2"/>
   <node CREATED="1163083271125" 
         ID="Freemind_Link_939750799" MODIFIED="1163083330140" 
         POSITION="right" TEXT="Erde" VSHIFT="12">
   <node CREATED="1163083347421" 
         ID="_" MODIFIED="1163083348796" TEXT="Mond"/>
   </node>
   <node CREATED="1163083271125" 
         ID="Freemind_Link_234604380" MODIFIED="1163083331031" 
         POSITION="right" TEXT="Mars" VSHIFT="9">
   <node CREATED="1163083378734" 
         MODIFIED="1163083378734" TEXT="Phobos"/>
   <node CREATED="1163083378734" 
         MODIFIED="1163083378734" TEXT="Deimos"/>
   </node>

   -------Abgeschnitten---------
   	
   
   
