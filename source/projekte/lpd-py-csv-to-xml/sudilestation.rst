.. meta::
    
   :description lang=de: XML-Kurs mit den Themen: DTD, Schema, XSL, XPath
   :keywords: XML, CSV,  OER

:ref:`« Übersicht Projekte <xsl-projekt-start>`

.. index:: Python; CSV, CSV zu XML konvertieren

   
==========================
Python: CSV zu XML wandeln
==========================

.. image:: ../images/spiegelung.jpg
   :width: 0px


.. |a| raw:: html

   <!-- lightbox container hidden with CSS -->
   <a href="#" class="lightbox" id="img1">
   <span style="background-image: url('../../_images/spiegelung.jpg')"></span>
   </a>

.. |b| raw:: html

   <a href="#img1">
   <img width="250px"
        alt='Spiegelungen'
        src="../../_images/spiegelung.jpg">
   </a>

.. sidebar:: XML-Kurs

   |b|

   Serie: Spiegelbilder

|a|

Lernziel
========

Wenn ich eine CSV-Datei erhalte und diese über XML weiterverarbeiten muss, 
ist die Konvertierung mit dem csv-Modul aus Python kein Problem.

Aufgaben
========

1. Laden Sie sich die Datei
   :download:`Download </files/csvtoxml.py>`
   herunter.
2. Legen Sie ihre csv-Dateien im gleichen Ordner oder einen Unterordner ab.
3. Starten Sie die Transformation.
4. Konvertieren Sie zwei unterschiedliche CSV-Dateien mit
   XSL-Transformation zu einer neuen XML-Struktur.
   
Beispiel einer Datei csv-Datei
==============================

.. code-block:: python
   :linenos:

   a,b,c,d
   22,222,222,222
   111,111,111,111
   333,333,333,333


Das Python-Script
=================

Quelle:
`Python recipe auf ActiveState <http://code.activestate.com/recipes/577423-convert-csv-to-xml/>`_

.. code-block:: python
   :linenos:

   # csv2xml.py
   # Convert all CSV files in a given (using command line argument) folder to XML.
   # FB - 20120523
   # First row of the csv files must be the header!
   
   # example CSV file: myData.csv
   # id,code name,value
   # 36,abc,7.6
   # 40,def,3.6
   # 9,ghi,6.3
   # 76,def,99
   
   import sys
   import os
   import csv
   if len(sys.argv) != 2:
       os._exit(1)
   path=sys.argv[1] # get folder as a command line argument
   os.chdir(path)
   csvFiles = [f for f in os.listdir('.') if f.endswith('.csv') or f.endswith('.CSV')]
   for csvFile in csvFiles:
       xmlFile = csvFile[:-4] + '.xml'
       csvData = csv.reader(open(csvFile))
       xmlData = open(xmlFile, 'w')
       xmlData.write('<?xml version="1.0"?>' + "\n")
       # there must be only one top-level tag
       xmlData.write('<csv_data>' + "\n")
       rowNum = 0
       for row in csvData:
           if rowNum == 0:
               tags = row
               # replace spaces w/ underscores in tag names
               for i in range(len(tags)):
                   tags[i] = tags[i].replace(' ', '_')
           else: 
               xmlData.write('<row>' + "\n")
               for i in range(len(tags)):
                   xmlData.write('    ' + '<' + tags[i] + '>' \
                                 + row[i] + '</' + tags[i] + '>' + "\n")
               xmlData.write('</row>' + "\n")                
           rowNum +=1
       xmlData.write('</csv_data>' + "\n")
       xmlData.close()
      
Aufruf des Skriptes
===================
Wenn alle Dateien in einem Ordner liegen.

::

  python csvtoxml.py .

Wenn die csv-Dateien in einem Unterordner liegen:
::

  python csvtoxml.py meinOrdner

      
Das Ergebnis der Transformation
===============================
.. code-block:: xml
   :linenos:

   <?xml version="1.0"?>
   <csv_data>
   <row>
       <a>22</a>
       <b>222</b>
       <c>222</c>
       <d>222</d>
   </row>
   <row>
       <a>111</a>
       <b>111</b>
       <c>111</c>
       <d>111</d>
   </row>
   <row>
       <a>333</a>
       <b>333</b>
       <c>333</c>
       <d>333</d>
   </row>
   </csv_data>
   
      
