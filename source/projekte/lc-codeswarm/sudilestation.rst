.. meta::
    
   :description lang=de: XML-Kurs mit den Themen: DTD, Schema, XSL, XPath
   :keywords: XSL, code_swarm, OER

:ref:`« Übersicht Projekte <xsl-projekt-start>`
     
.. index:: Projekt; code_swarm, codeswarm
	   
==========================
Visualisierung: code_swarm
==========================

.. image:: ../images/code_swarm-screenshot.jpg
   :width: 0px


.. |a| raw:: html

   <!-- lightbox container hidden with CSS -->
   <a href="#" class="lightbox" id="img1">
   <span style="background-image: url('../../_images/code_swarm-screenshot.jpg')"></span>
   </a>

.. |b| raw:: html

   <a href="#img1">
   <img width="250px"
        alt='Screenshot Codeswarm'
        src="../../_images/code_swarm-screenshot.jpg">
   </a>

.. sidebar:: XML-Kurs

   |b|

   Serie: 

|a|

	     
Lernziel
========

Das Google-Projekt *code_swarm* dient der Visualisierung von Einträgen in log-Dateien 
(git, svn, cvs...). Wenn Sie Vorgänge in einer Zeitleiste visualisieren wollen, können 
Sie die Ereignisse in einer eigenen XML-Struktur definieren und anschließend mit dem 
code_swarm-Programm ein Video generieren.

:Beispeilvideo:

.. raw:: html
	 
   <iframe src="https://player.vimeo.com/video/1093745"
   width="640" height="483" frameborder="0"
   webkitallowfullscreen mozallowfullscreen allowfullscreen>
   </iframe>
   <p><a href="https://vimeo.com/1093745">code_swarm - Python</a>
   from <a href="https://vimeo.com/michaelogawa">Michael Ogawa</a> on
   <a href="https://vimeo.com">Vimeo</a>.</p>  
	   
Handlungsanweisungen
====================

:Aufgaben:

  - Installieren Sie ein aktuelles JSDK
  - Installieren Sie Ant
  -  Holen Sie sich eine Kopie von code_swarm
  - erstellen Sie eine Datei myEvents.xml
  - Erstellen Sie das Video

Viel Spaß beim experimenteren ...

Java-SDK  installieren
======================

Prüfen Sie ob das Programm javac verfügbar ist 
(verwenden cmd über den Dialog Ausführen..:

::

  javac -version

Wenn das Programm javac[.exe] nicht vorhanden ist, dann:
herunterladen von:

   `Download java, falls notwendig
   <http://www.oracle.com/technetwork/java/javase/downloads/index.html>`_

Umgebungsvariable Path  erweitern (siehe Einstellungen | Umgebungsvariablen):
::

  %SystemRoot%\... weitere Einträge...;C:\Programme\Java\jdkxxxxx\bin

Ant installieren
================
Herunterladen von:
::

  http://ant.apache.org/

Entpacken und in den Programm-Ordner packen.
Umgebungsvariable *JAVA_HOME* festlegen (siehe Einstellungen | Umgebungsvariablen):
::

  C:\Programme\Java\jdkxxx


Umgebungsvariable Pfad erweitern(siehe Einstellungen | Umgebungsvariablen):

  %SystemRoot%\... weitere Einträge...;C:\Programme\apache-ant-xxxx\bin


code_swarm installieren
=======================
Herunterladen mit Subversion:
::

  svn checkout http://codeswarm.googlecode.com/svn/trunk/ codeswarm-read-only 


Projekt konfigurieren
======================
In der Datei *project.config* legen Sie diverse Parameter für Ihr
Projekt fest. Verwenden Sie die Beispieldatei im Ordner *data* und
passen Sie die Parameter entsprechend an.

Video erzeugen
==============
Testaufruf mit den vorhandenen Beispieldateien:
::

  cd code_swarm-read-only
  ./run.bat data/project.config


