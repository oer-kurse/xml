.. meta::
    
   :description lang=de: XML-Kurs mit den Themen: DTD, Schema, XSL, XPath
   :keywords: XML, Programmierung, OER

:ref:`« Übersicht Projekte <xsl-projekt-start>`

.. index:: Programmierung
	   
=================================
XML und Programmierschnittstellen
=================================

.. image:: ../images/gesicht.jpg
   :width: 0px

.. |a| raw:: html

   <!-- lightbox container hidden with CSS -->
   <a href="#" class="lightbox" id="img1">
   <span style="background-image: url('../../_images/gesicht.jpg')"></span>
   </a>

.. |b| raw:: html

   <a href="#img1">
   <img width="250px"
        src="../../_images/gesicht.jpg"
	alt='Gesicht' />
   </a>

   
.. sidebar:: XML-Kurs

   |b|

   Serie: Kunst 

|a|

Lernziel
========

Hier soll kurz gezeigt werden, welche Moeglichkeiten es
gibt, mit Programmiersprachen auf XML-Dateien zuzugreifen.


Handlungsanweisungen
====================

:Aufgaben:

  1. Informieren Sie sich kurz über die Möglichkeiten, XML über
     Programmierschnittstellen zu bearbeiten.
     

Zugriff auf XML durch Programmiersprachen
=========================================

Fast jede moderne Programmiersprache hat Schnittstellen zu XML. Hier
seien einige Sprachen mit sehr guter Unterstützung genannt: Java,
Perl, PHP, Python, JavaScript.

Es gibt unterschiedliche Schnittstellen und Methoden, um auf
XML-Datein zuzugreifen, diese sollen nun kurz vorgestellt werden.

Document Object Modell (DOM)
============================

Mit dem DOM greift man hierachisch auf einen XML-Datei zu, wie die
folgende Grafik verdeutlichen soll:

.. image:: img/dom.png

„person“ ist das root-Element, mit den Kindelemente alter, wohnort und
beruf. Klar zu erkennen ist die hierachische Anordnung.

.. index:: Perl

Hier ein Beispiel in Perl:
==========================
::
   #!/usr/bin/perl
   use XML::DOM;

   #neues Instanz erstellen
   my $parser = new XML::DOM::Parser;

   #zu parsenden File angeben (das XML-Dokument oben)
   my $doc = $parser->parsefile('test.xml');

   #Elementname angeben (person), zurück bekommen wir eine Liste der 
   #Knoten (in diesem Fall ein Knoten zu den Attributen).

   my $nodes = $doc->getElementsByTagName("person");

   #jetzt wählen wir den 0. Knoten (Zählung beginnt mit 0) aus
   my $node = $nodes->item(0);

   #und greifen auf die beiden Attribute zu, mit getNodeValue bekommen wir 
   #den entsprechenden Wert. Die Struktur ist folgende: 
   #Attributknoten -> Attribut -> Attributwert 

   print $node->getAttributes->item(0)->getNodeValue;
   print $node->getAttributes->item(1)->getNodeValue;
   
.. index:: Python

Beispiele für die Programmiersprache Python
===========================================
   
- Python: (RSS-Feeds lesen) http://effbot.org/zone/element-rss-wrapper.htm

Programmiersprachen und XML?
============================

Die Anwendungsbereiche sind vielfältig, hier eine Übersicht:

 - Ein- und Auslesen von XML-Dateien sowie deren Transformation in andere Formate
 - Erstellen von XML-Dateien (z.B. auf Formularen und Benutzereingaben)
 - Automatisieren von Transformationen (XML -> HTML, XML -> Latex -> PDF, usw.)
 - Erstellen von SVG-Grafiken on-the-fly
 - Transformation von XML-Dokumenten in relationale Datenbanken und umgekehrt 

