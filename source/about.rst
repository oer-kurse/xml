.. orphan:
   
.. meta::
   
   :description lang=de: Über den XML-Kurs, was sind OER
   :keywords: XML, Open Educational Resources, OER

===============
 Über den Kurs
===============

Das Schulungsmaterial kann zum Nachschlagen, für das Selbstlernen und
andere Schulungskonzepte verwendet werden. Die OER-Lizenz bedeutet,
dass mit den Inhalten folgende Aktionen durchgeführt werden dürfen:

- ungefragt verwenden
- ungefragt verändern
- ungefragt mit anderen Inhalten neu vermischen

Es gelten auch die Regeln der Creative Commons Lizenz: 

.. raw:: html

   <a rel="license"
      href="http://creativecommons.org/licenses/by/4.0/">
      <img alt="Creative Commons Lizenzvertrag" style="border-width:0"
      src="https://i.creativecommons.org/l/by/4.0/88x31.png" /></a><br />
      Dieses Werk ist lizenziert unter der <br />
      <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">
      Creative Commons Namensnennung 4.0 International Lizenz</a>.

Sollten diese Lizenzen immer noch für Unsicherheiten bezüglich der
Nutzung hervorrufen, erkläre ich hiermit einen Nichtangriffspackt und
verzichte auf alle rechtlichen Mittel, da es nichts durchzusetzen gibt.

Wie Komme ich an den Quellcode?
===============================

Das verwendete Theme (haiku) unterstützt leider nicht die Verlinkung zum
Sourcecode, er ist aber präsent und kann durch die Manipulation der URL
abgerufen werden. Beispiel:
::
   
  .../xml/html/index.html

- füge hinter *html* den namen *_sources* ein
- ersetze die Dateierweiterung *html* durch *rst.txt*:
  
::

   xml/html/_sources/index.rst.txt

Eine zweite Möglichkeit besteht im Download des kompletten Quellcodes,
der unter folgender URL zur Verfügung gestellt wird:

`Kursmaterial auf Bitbucket.org <https://bitbucket.org/pkoppatz/xml-kurs-oer>`_


Was ist rst und Sphinx?
=======================
Sphinx ist eine Software, die für die Dokumentation von
Python-Projekten verwendet wird. Das Standard-Datei-Format ist rst,
es handelt sich um ein einfaches Textformat, welches durch Markup
ergänzt, aus einer Quelle, diverse Ausgabe-Formate generieren kann z.B
HTML, Latex (als Vorstufe für die Generierung eines PDF). Siehe auch
die Dokumentation zum Projekt:

`Sphinx-Dokumentation <http://www.sphinx-doc.org/en/stable/>`_ 

==============
Über den Autor
==============

Ich bin seit vielen Jahren als Dozent tätig. Für meine Kurse stelle ich
Materialien zur Verfügung, nun auch als OER. Neben der Entwicklung von 
Kursmaterialien, interessiere ich mich für die Programmiersprache Python,
lese gern und halte mich mit Sport fit.

.. image:: _static/peter-koppatz.jpg

	   

