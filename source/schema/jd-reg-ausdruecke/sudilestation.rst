.. meta::

   :description lang=de: XML-Kurs mit den Themen: DTD, Schema, XSL, XPath
   :keywords: XML, Schema, Reguläre Ausdrücke, OER

:ref:`« Übersicht: Schema <xml-schema-start>`

.. index:: Schema; Reguläre Ausdrücke
.. index:: Reguläre Ausdrücke

==================
Reguläre Ausdrücke
==================

.. image:: ../images/mosaik005-potsdam.jpg
   :width: 0px


.. |a| raw:: html

   <!-- lightbox container hidden with CSS -->
   <a href="#" class="lightbox" id="img1">
   <span style="background-image: url('../../_images/mosaik005-potsdam.jpg')"></span>
   </a>

.. |b| raw:: html

   <a href="#img1">
   <img width="250px"
	alt="Mosaik Potsdam"
        src="../../_images/mosaik005-potsdam.jpg">
   </a>

.. sidebar:: Mosaik

   |b|

   Potsdam Dortustraße

|a|


Lernziel
========

Reguläre Ausdrücke sind eine entscheidende Erweiterung gegenüber DTDs,
es wird möglich, Inhalte von Elemente zu prüfen.
Denken Sie an die Struktur von Telefonnummern, eMail-Adressen oder Postleitzahlen.

Handlungsanweisungen
====================

:Aufgaben: 1. Definieren Sie bitte für die Attribute telefonnummer und plz
              innerhalb der XSD-Datei mit regulären Ausdrücken.

              a) Die Telefonnummer sollte immer das Format „Vorwahl-Nummer“ haben. 
              b) Die PLZ sollten genau 5 Zahlen enthalten.

              Die beiden regulären Ausdrücke sind:
              für die PLZ:
              ::

                 [0-9]{5}

              für die Telefonnummer:
              ::

                [0-9]{3,6}-[0-9]{4,8}

              Ein XML-Beispiel, welches die obigen Anforderungen erfüllt.
              ::

                <?xml version="1.0" encoding="ISO-8859-1"?>
                <reise-datenbank xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
                                 xsi:noNamespaceSchemaLocation="reise.xsd">
                  <reise telefonnummer="0331-34723423" plz="12344">
                    <veranstalter>Fahr-away</veranstalter>
                    <ziel>Polen</ziel>
                  </reise>
                   <reise telefonnummer="0331-43723432" plz="14482">
                    <veranstalter>BleibHier</veranstalter>
                    <ziel>Deutschland</ziel>
                  </reise>
                </reise-datenbank>


Wie definieren wir Reguläre Ausdrücke?
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

In unserer XML-Datei befindet sich das Attribut *format*.
Dieses enthält den Buchstaben a gefolgt von einer einstelligen Zahl.
Also: a1, a2, a3, a4, a5 usw.

Nun sollten wir in unserer XSD-Datei prüfen, ob dieses Struktur auch wirklich
eingehalten wird. Die Bezeichnungen ab, a11 oder a# sollten als Fehler gemeldet
werden. Wie geht das nun? Wir definieren für das Attribut *format* einen Typ
*din_typ*, der den Test enthalten soll.

.. code-block:: xml
   :linenos:

   <?xml version="1.0" encoding="ISO-8859-1"?>
   <artikel_db xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
   xsi:noNamespaceSchemaLocation="artikel_db.xsd">
   <artikel format="a4">
       <titel>Der Titel</titel>
       <autor>Heinz Becker</autor>
       <inhalt>Hier irgendein Inhalt.</inhalt>
     </artikel>
     <artikel format="a3">
       <titel>Tanzen</titel>
       <autor>Susanne Martens</autor>
       <inhalt>Hier irgendein Inhalt.</inhalt>
     </artikel>
   </artikel_db>

Der Typ *din_typ* enthält die entscheidende Zeile:

.. code-block:: xml
   :linenos:
   :emphasize-lines: 16, 19-23

   <?xml version="1.0" encoding="UTF-8"?>
   <xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema">
     <xs:element name="artikel_db" type="artikel_db_type"/>
     <xs:complexType name="artikel_db_type">
       <xs:sequence>
         <xs:element name="artikel" type="artikel_typ"
           minOccurs="1" maxOccurs="unbounded"/>
       </xs:sequence>
     </xs:complexType>
     <xs:complexType name="artikel_typ">
       <xs:sequence>
         <xs:element name="titel" type="xs:string"/>
         <xs:element name="autor" type="xs:string"/>
         <xs:element name="inhalt" type="xs:string"/>
       </xs:sequence>
       <xs:attribute name="format" type="din_typ"/>
     </xs:complexType>

     <xs:simpleType name="din_typ">
       <xs:restriction base="xs:string">
      <xs:pattern value="a[0-9]"/>
       </xs:restriction>
     </xs:simpleType>

   </xs:schema>

Der Reguläre Ausdruck prüft, ob dem a eine beliebige Zahl zwischen 0 und 9 folgt.

Den Inhalt von Elementen prüfen
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Nun lernen Sie, wie Sie den Inhalt von Elementen mit regulären
Ausdrücken prüfen können. Im Element autor sollen folgende Zeichen
zugelassen werden:

- Buchstaben
- Leerzeichen
- Bindestrich

.. code-block:: xml
   :linenos:
   :emphasize-lines: 13, 25-29

   <?xml version="1.0" encoding="UTF-8"?>
   <xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema">
     <xs:element name="artikel_db" type="artikel_db_type"/>
     <xs:complexType name="artikel_db_type">
       <xs:sequence>
         <xs:element name="artikel" type="artikel_typ"
           minOccurs="1" maxOccurs="unbounded"/>
       </xs:sequence>
     </xs:complexType>
     <xs:complexType name="artikel_typ">
       <xs:sequence>
         <xs:element name="titel" type="xs:string"/>
         <xs:element name="autor" type="nur_text"/>
         <xs:element name="inhalt" type="xs:string"/>
       </xs:sequence>
       <xs:attribute name="format" type="din_typ"/>
     </xs:complexType>

     <xs:simpleType name="din_typ">
       <xs:restriction base="xs:string">
         <xs:pattern value="a[0-9]"/>
       </xs:restriction>
     </xs:simpleType>

     <xs:simpleType name="nur_text">
       <xs:restriction base="xs:string">
         <xs:pattern value="[a-zA-Z -]{1,}"/>
       </xs:restriction>
     </xs:simpleType>

   </xs:schema>

Durch hervogehobenen Zeilen wird die Typ-Regel durchgesetzt.
Die Elemente des Regurären Ausdruck bedeuten:

+----------------------+-------------------------------------------------+
| Regulärere Ausdruck  | Kommentar                                       |
+======================+=================================================+
| [a-zA-Z -]           | Alle Buchstaben (groß und klein), Leerzeichen   |
|                      | und der Bindestrich                             |
+----------------------+-------------------------------------------------+
| {1,40}               | Minimal ein Zeichen, maximal 40                 |
+----------------------+-------------------------------------------------+
| {1,}                 | Minimal ein Zeichen, maximal: unendlich         |
+----------------------+-------------------------------------------------+

Weiterführenden Links zu Regulären Ausdrücken
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

- `Reguläre Ausdrücke für einzelne Zeichen (SelfHTML) <http://de.selfhtml.org/perl/sprache/regexpr.htm#zeichen>`_

- `Belles Lettres -- Deutsch für Dichter und Denker
  <http://www.belleslettres.eu/content/technik/wth-02-regex-markdown-xml.php>`_:

  Ein ausführliches Video von Daniel Scholten, das zeigt, wie die
  Überarbeitung literarischer Texte mit Hilfe regulärer Ausdrücke
  möglich ist und warum er keine klassische Textverarbeitung
  verwendet. Gleichzeitig gibt er Hinweise zum Editor Notepad++ (Windows).
  Interessant sind auch der erste Teil, mit Anmerkungen zu Sinn und Unsinn
  diverser Dateiformate. Weitere Filme zum Thema Konvertierung werden
  folgen.

- In der Kursübersicht der Startseite finden Sie auch einen Link, zu einer
  Online-Übungsseite für reguläre Ausdrücke.
