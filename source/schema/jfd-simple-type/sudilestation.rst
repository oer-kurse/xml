.. meta::

   :description lang=de: XML-Kurs mit den Themen: DTD, Schema, XSL, XPath
   :keywords: XML, Schema, Datentyp, OER

:ref:`« Übersicht: Schema <xml-schema-start>`

.. index:: Schema; Datentyp

============
Wiederholung
============

.. image:: ../images/mosaik009-potsdam.jpg
   :width: 0px


.. |a| raw:: html

   <!-- lightbox container hidden with CSS -->
   <a href="#" class="lightbox" id="img1">
   <span style="background-image: url('../../_images/mosaik009-potsdam.jpg')"></span>
   </a>

.. |b| raw:: html

   <a href="#img1">
   <img width="250px"
	alt="Mosaik Potsdam"
        src="../../_images/mosaik009-potsdam.jpg">
   </a>

.. sidebar:: Mosaik

   |b|

   Potsdam Dortustraße

|a|

=====================
Einfache Datentypen I
=====================

Lernziel
========

Aus dem Element *adresse* wird ein komplexer Datentyp, der als Container dient.

Handlungsanweisungen
====================

:Aufgaben:

  1. Erweitern Sie das Schema aus der vorherigen Station.
  2. Fügen Sie ein zweites Element *anrede* ein.

Die DTD als Vorlage
===================
Zeile: 4

.. code-block:: xml
   :linenos:

   <?xml version="1.0" encoding="UTF-8"?>
   <!ELEMENT adressdb (adresse+)>
   <!ENTITY % sichtbarkeit "einsicht (privat|public|proteced) #REQUIRED">
   <!ELEMENT adresse (anrede, name, strasse, postanschrift, email?, bild)>
   <!ELEMENT anrede (#PCDATA)>
   <!ELEMENT name (nachname, vorname)>
   <!ELEMENT postanschrift (plz, wohnort)>
   <!ELEMENT nachname (#PCDATA)>
   <!ELEMENT vorname (#PCDATA)>
   <!ELEMENT plz (#PCDATA)>
   <!ELEMENT wohnort (#PCDATA)>
   <!ELEMENT strasse (#PCDATA) >
   <!ELEMENT email (#PCDATA)>
   <!ATTLIST strasse %sichtbarkeit;>
   <!ATTLIST postanschrift land
             (Deutschland | Schweiz | Oesterreich) #REQUIRED %sichtbarkeit; >
   <!ATTLIST adresse nr CDATA #REQUIRED>
   <!ELEMENT bild EMPTY>
   <!ATTLIST bild bezeichnung CDATA #REQUIRED>
   <!ENTITY email_default "keine E-Mail angegeben" %sichtbarkeit; >

Kind-Element in der DTD
=======================

.. code-block:: xml

   <!ELEMENT adresse (anrede, name, strasse, postanschrift, email?, bild)>

ComplexType als Container
=========================
Siehe Zeile: 6

.. code-block:: xml
   :linenos:

   <?xml version="1.0" encoding="UTF-8"?>
    <xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema">
    <xs:element name="adressdb">
       <xs:annotation>
         <xs:documentation>Meine Adressammlung</xs:documentation>
       </xs:annotation>
       <xs:complexType>
         <xs:sequence>
           <xs:element ref="adresse" minOccurs="1" maxOccurs="unbounded"/>
         </xs:sequence>
       </xs:complexType>
     </xs:element>
     <xs:element name="adresse">
       <xs:complexType>
         <xs:sequence>
           <xs:element ref="anrede" minOccurs="0"/>
         </xs:sequence>
       </xs:complexType>
     </xs:element>
     <xs:element name="anrede" type="xs:string"/>
   </xs:schema>


Einfache Datentypen
===================

- Das Element *adresse* ist nun wie das Element *adressdb*, ein Container,
  der weitere Elemente zusammenfassen soll. Es enthält als einziges Element
  das Kindelement *anrede*. Der Datentyp ist *xs:string*.

- Elemente können von einem einfachen Typ sein oder setzen sich in einem komplexen 
  Typ aus einfachen Typen zusammen.

- Eine Übersicht zu allen einfachen Datentypen finden Sie unter folgender Adresse:

  http://www.w3.org/TR/xmlschema-0/#simpleTypesTable


Das fertige XML-Dokument
========================

.. code-block:: xml

   <?xml version="1.0" encoding="UTF-8"?>
   <adressdb xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
             xsi:noNamespaceSchemaLocation="art03.xsd">
     <adresse>
      <anrede />
     </adresse>
   </adressdb>
