.. meta::

   :description lang=de: XML-Kurs mit den Themen: DTD, Schema, XSL, XPath
   :keywords: XML, Schema, Standardeingaben (default-Werte), OER

:ref:`« Übersicht: Schema <xml-schema-start>`

.. index:: Schema; Standardeingaben (default-Werte)

================
Standardeingaben
================


.. image:: ../images/mosaik012-potsdam.jpg
   :width: 0px


.. |a| raw:: html

   <!-- lightbox container hidden with CSS -->
   <a href="#" class="lightbox" id="img1">
   <span style="background-image: url('../../_images/mosaik012-potsdam.jpg')"></span>
   </a>

.. |b| raw:: html

   <a href="#img1">
   <img width="250px"
	alt="Mosaik Potsdam"
        src="../../_images/mosaik012-potsdam.jpg">
   </a>

.. sidebar:: Mosaik

   |b|

   Potsdam Dortustraße

|a|


Lernziel
========

Die Standardwerte für Elemente definieren. Entspricht einer
Entity-Definition in der DTD.


Handlungsanweisungen
====================

:Aufgaben:

  1. Erweitern Sie das Schema aus der vorherigen Station
  2. Für ein Element kann ein default-Wert definiert werden,
     dies funktioniert dann wie mit einer Entity in der DTD.

Die DTD als Vorlage
===================
Zeile 20

.. code-block:: xml
   :linenos:


   <?xml version="1.0" encoding="UTF-8"?>
   <!ELEMENT adressdb (adresse+)>
   <!ENTITY % sichtbarkeit "einsicht (privat|public|proteced) #REQUIRED">
   <!ELEMENT adresse (anrede, name, strasse, postanschrift, email?, bild)>
   <!ELEMENT anrede (#PCDATA)>
   <!ELEMENT name (nachname, vorname)>
   <!ELEMENT postanschrift (plz, wohnort)>
   <!ELEMENT nachname (#PCDATA)>
   <!ELEMENT vorname (#PCDATA)>
   <!ELEMENT plz (#PCDATA)>
   <!ELEMENT wohnort (#PCDATA)>
   <!ELEMENT strasse (#PCDATA) >
   <!ELEMENT email (#PCDATA)>
   <!ATTLIST strasse %sichtbarkeit;>
   <!ATTLIST postanschrift land
             (Deutschland | Schweiz | Oesterreich) #REQUIRED %sichtbarkeit; >
   <!ATTLIST adresse nr CDATA #REQUIRED>
   <!ELEMENT bild EMPTY>
   <!ATTLIST bild bezeichnung CDATA #REQUIRED>
   <!ENTITY email_default "keine E-Mail angegeben" %sichtbarkeit; >


Standard-Eingaben definieren
============================
Zeile 56

.. code-block:: xml
   :linenos:

   <xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema">
     <xs:element name="adressdb">
       <xs:annotation>
         <xs:documentation>Meine Adressammlung</xs:documentation>
       </xs:annotation>
       <xs:complexType>
         <xs:sequence>
           <xs:element ref="adresse" minOccurs="1" maxOccurs="unbounded"/>
         </xs:sequence>
       </xs:complexType>
     </xs:element>
     <xs:element name="adresse">
       <xs:complexType>
         <xs:sequence>
           <xs:element ref="anrede" minOccurs="1"/>
           <xs:element ref="name" minOccurs="1"/>
           <xs:element ref="strasse" minOccurs="1"/>
           <xs:element ref="postanschrift" minOccurs="1"/>
           <xs:element ref="email" minOccurs="0"/>
           <xs:element ref="bild" minOccurs="1"/>
         </xs:sequence>
       </xs:complexType>
     </xs:element>
     <xs:element name="anrede" type="xs:string"/>
     <xs:element name="name">
       <xs:complexType>
         <xs:sequence>
           <xs:element ref="nachname"/>
           <xs:element ref="vorname"/>
         </xs:sequence>
       </xs:complexType>
     </xs:element>
    <xs:element name="postanschrift">
       <xs:complexType>
         <xs:sequence>
           <xs:element ref="plz"/>
           <xs:element ref="wohnort"/>
         </xs:sequence>
         <xs:attribute name="land" use="required">
           <xs:simpleType>
             <xs:restriction base="xs:string">
               <xs:enumeration value="Deutschland"/>
               <xs:enumeration value="Schweiz"/>
               <xs:enumeration value="Östereich"/>
             </xs:restriction>
           </xs:simpleType>
         </xs:attribute>
         <xs:attribute ref="einsicht" use="required"/>
       </xs:complexType>
     </xs:element>
     <xs:element name="strasse">
       <xs:complexType>
         <xs:attribute ref="einsicht"/>
       </xs:complexType>
     </xs:element>
     <xs:element name="email" type="xs:string" default="Keine E-Mail angegeben"/>
     <xs:element name="bild" type="xs:string"/>
     <xs:element name="nachname" type="xs:string"/>
     <xs:element name="vorname" type="xs:string"/>
     <xs:attribute name="einsicht">
       <xs:simpleType>
         <xs:restriction base="xs:string">
           <xs:enumeration value="privat"/>
           <xs:enumeration value="public"/>
         </xs:restriction>
       </xs:simpleType>
     </xs:attribute>
   </xs:schema>

Das fertige XML-Dokument
========================

.. code-block:: xml

   <adressdb xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                   xsi:noNamespaceSchemaLocation="artikel05.xsd">
      <adresse>
       <anrede/>
       <name>
         <nachname/>
         <vorname/>
       </name>
       <strasse einsicht="privat"/>
       <postanschrift einsicht="privat" land="Deutschland">
         <plz/>
         <wohnort/>
       </postanschrift>
       <email>Keine E-Mail angegeben</email>
       <bild/>
     </adresse>
   </adressdb>
   
