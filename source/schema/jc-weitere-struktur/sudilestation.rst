.. meta::

   :description lang=de: XML-Kurs mit den Themen: DTD, Schema, XSL, XPath
   :keywords: XML, Schema, Verschachtelung, Wiederholung, Attribute, OER

:ref:`« Übersicht: Schema <xml-schema-start>`

.. index:: Schema; Verschachtelungen
.. index:: Schema; Wiederholung
.. index:: Schema; Attribute

==========================================
Verschachtelungen, Wiederholung, Attribute
==========================================

.. image:: ../images/mosaik004-potsdam.jpg
   :width: 0px


.. |a| raw:: html

   <!-- lightbox container hidden with CSS -->
   <a href="#" class="lightbox" id="img1">
   <span style="background-image: url('../../_images/mosaik004-potsdam.jpg')"></span>
   </a>

.. |b| raw:: html

   <a href="#img1">
   <img width="250px"
	alt="Mosaik Potsdam"
        src="../../_images/mosaik004-potsdam.jpg">
   </a>

.. sidebar:: Mosaik

   |b|

   Potsdam Dortustraße

|a|

Lernziel
========

Sie lernen nun wichtige Strukturen für die Schema-Definitionen kennen:
Verschachtelte Elemente, Wiederholungen und Attribute.


Handlungsanweisungen
====================

:Aufgaben: 1. Erstellen Sie eine Schema-Datei für folgende XML-Datei:
           ::

              <?xml version="1.0" encoding="ISO-8859-1"?>
              <reise-datenbank xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
                               xsi:noNamespaceSchemaLocation="reise.xsd">
                <reise art="Fahrrad-Tour">
                  <veranstalter>Fahr-away</veranstalter>
                  <ziel>Polen</ziel>
                </reise>
                <reise art="Wanderung">
                  <veranstalter>BleibHier</veranstalter>
                  <ziel>Deutschland</ziel>
                </reise>
              </reise-datenbank>

.. index: XSD: Verschachtelte Elemente

Verschachtelte Elemente
~~~~~~~~~~~~~~~~~~~~~~~~

Sie lernen nun, wie Sie verschachtelte Elemente prüfen können.
Schauen wir uns die verschachtelte XML-Datei an:

.. code-block:: xml
   :linenos:

   <?xml version="1.0" encoding="ISO-8859-1"?>
   <artikel_db xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
               xsi:noNamespaceSchemaLocation="artikel_db.xsd">
     <artikel>
       <titel>Der Titel</titel>
       <autor>Heinz Becker</autor>
       <inhalt>Hier irgendein Inhalt.</inhalt>
      </artikel>
    </artikel_db>

Wie können wir nun die XSD-Datei dazu erstellen?

.. code-block:: xml
   :linenos:

   <?xml version="1.0" encoding="UTF-8"?>
   <xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema">
     <xs:element name="artikel_db" type="artikel_db_type" />
   </xs:schema>

**Schritt 1:** Das Wurzel-Element *artikel_db* kann mehrere Kind-Elemente
 *artikel* enthalten.

Diesen Sachverhalt können wir nun definieren. Mit Schema-Definitionen ist
es nicht so einfach wie in einer DTD. Wie kann das Element *artikel*
als Kindelement von *artikel_db* definiert werden?
Wir definieren dafür einen neuen Typ *artikel_db_type* als Datentyp
(Zeilen 4-8). Darin wieder enthalten ein Abschnitt für das
Kind-Element *artikel* (Zeile 6).

.. code-block:: xml
   :linenos:

   <?xml version="1.0" encoding="UTF-8"?>
   <xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema">
              <xs:element name="artikel_db" type="artikel_db_type" />
     <xs:complexType name="artikel_db_type">
       <xs:sequence>
         <xs:element name="artikel" type="artikel_typ" />
       </xs:sequence>
     </xs:complexType>
    </xs:schema>

**2. Schritt:** Wir definieren nun den Typ artikel_db_type:
Dieser Typ beinhaltet das Element artikel.
Das Element artikel enthält wiederum Unterelemente, die in einem extra
Typ definiert werden müssen. Der Name wird im Attribut *type* festglegt.

.. code-block:: xml
   :linenos:

   <?xml version="1.0" encoding="UTF-8"?>
   <xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema">
              <xs:element name="artikel_db" type="artikel_db_type" />
     <xs:complexType name="artikel_db_type">
       <xs:sequence>
         <xs:element name="artikel" type="artikel_typ" />
       </xs:sequence>
     </xs:complexType>

     <xs:complexType name="artikel_typ">
       <xs:sequence>
         <xs:element name="titel"  type="xs:string" />
         <xs:element name="autor"  type="xs:string" />
         <xs:element name="inhalt" type="xs:string" />
       </xs:sequence>
     </xs:complexType>
   </xs:schema>

**3. Schritt:** Nun muss noch der Aufbau eines *artikel_typ* definiert
werden. Ein Artikel soll ja aus den drei Kind-Elementen *titel*, *autor* und *inhalt* 
bestehen. Diese sind im *artikel_typ* aufgeführt und bestehen aus
dem einfachen Datentyp einer Zeichenkette.

.. index: XSD: Wiederholungen


Wiederholungen
~~~~~~~~~~~~~~

Wenn die XML-Datei mehrere Artikel enthalten soll, so muss das
auch in der Schema-Datei definiert werden, da die obige
Schema-Definition nur genau ein artikel-Element zulässt.
Die modifizierte XSD-Datei, sollten sie weitere Datensätze einfügen
können.

.. code-block:: xml
   :linenos:

   <?xml version="1.0" encoding="UTF-8"?>
   <xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema">
              <xs:element name="artikel_db" type="artikel_db_type" />
     <xs:complexType name="artikel_db_type">
       <xs:sequence>
         <xs:element name="artikel" type="artikel_typ"
                     minOccurs="1" maxOccurs="10"  />
       </xs:sequence>
     </xs:complexType>

     <xs:complexType name="artikel_typ">
       <xs:sequence>
         <xs:element name="titel"  type="xs:string" />
         <xs:element name="autor"  type="xs:string" />
         <xs:element name="inhalt" type="xs:string" />
       </xs:sequence>
     </xs:complexType>
   </xs:schema>

In der Schema-Datei müssen wir angeben, ob und wie viele Wiederholungen
des *artikel*-Elements zugelassen sind. Das geht mit:
*minOccurs="1"* und  *maxOccurs="10"* (Zeile 7).
Für beliebig viele Einträge verwenden Sie *maxOccurs="unbounded"*

.. index: XSD: Attribute

Attribute
=========

Die XML-Struktur wird nun ein Attribut *format* erhalten.
Wie definieren wir nun Attribute in der XSD (Siehe Zeile 17)?

.. code-block:: xml
   :linenos:

   <?xml version="1.0" encoding="UTF-8"?>
   <xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema">
              <xs:element name="artikel_db" type="artikel_db_type" />
     <xs:complexType name="artikel_db_type">
       <xs:sequence>
         <xs:element name="artikel" type="artikel_typ"
                     minOccurs="1" maxOccurs="10"  />
       </xs:sequence>
     </xs:complexType>

     <xs:complexType name="artikel_typ">
       <xs:sequence>
         <xs:element name="titel"  type="xs:string" />
         <xs:element name="autor"  type="xs:string" />
         <xs:element name="inhalt" type="xs:string" />
       </xs:sequence>
       <xs:attribute name="format" type="xs:string"/>
     </xs:complexType>
   </xs:schema>

