.. meta::

   :description lang=de: XML-Kurs mit den Themen: DTD, Schema, XSL, XPath
   :keywords: XML, Schema, Datentyp, OER

:ref:`« Übersicht: Schema <xml-schema-start>`

.. index:: Schema; Datentyp

======================
Einfache Datentypen II
======================


.. image:: ../images/mosaik010-potsdam.jpg
   :width: 0px


.. |a| raw:: html

   <!-- lightbox container hidden with CSS -->
   <a href="#" class="lightbox" id="img1">
   <span style="background-image: url('../../_images/mosaik010-potsdam.jpg')"></span>
   </a>

.. |b| raw:: html

   <a href="#img1">
   <img width="250px"
	alt="Mosaik Potsdam"
        src="../../_images/mosaik010-potsdam.jpg">
   </a>

.. sidebar:: Mosaik

   |b|

   Potsdam Dortustraße

|a|


Lernziel
========

Aus dem Element adresse wird ein komplexer Datentyp, der als Container dient.

Handlungsanweisungen
====================

:Aufgaben:

  1. Erweitern Sie das Schema aus der vorherigen Station
  2. Alle Kindelemente aus Adresse sollen als einfache Datentypen definiert werden.

Die DTD als Vorlage
===================
Siehe: Zeile 4

.. code-block:: xml
   :linenos:

   <?xml version="1.0" encoding="UTF-8"?>
   <!ELEMENT adressdb (adresse+)>
   <!ENTITY % sichtbarkeit "einsicht (privat|public|proteced) #REQUIRED">
   <!ELEMENT adresse (anrede, name, strasse, postanschrift, email?, bild)>
   <!ELEMENT anrede (#PCDATA)>
   <!ELEMENT name (nachname, vorname)>
   <!ELEMENT postanschrift (plz, wohnort)>
   <!ELEMENT nachname (#PCDATA)>
   <!ELEMENT vorname (#PCDATA)>
   <!ELEMENT plz (#PCDATA)>
   <!ELEMENT wohnort (#PCDATA)>
   <!ELEMENT strasse (#PCDATA) >
   <!ELEMENT email (#PCDATA)>
   <!ATTLIST strasse %sichtbarkeit;>
   <!ATTLIST postanschrift land
             (Deutschland | Schweiz | Oesterreich) #REQUIRED %sichtbarkeit; >
   <!ATTLIST adresse nr CDATA #REQUIRED>
   <!ELEMENT bild EMPTY>
   <!ATTLIST bild bezeichnung CDATA #REQUIRED>
   <!ENTITY email_default "keine E-Mail angegeben" %sichtbarkeit; >

Kind-Element in der DTD
=======================

.. code-block:: xml

   <!ELEMENT adresse (anrede, name, strasse, postanschrift, email?, bild)>

ComplexType als Container
=========================
Siehe Zeile: 6

.. code-block:: xml
   :linenos:


   <?xml version="1.0" encoding="UTF-8"?>
     <xs:element name="adressdb">
        <xs:annotation>
          <xs:documentation>Meine Adressammlung</xs:documentation>
        </xs:annotation>
        <xs:complexType>
          <xs:sequence>
            <xs:element ref="adresse" minOccurs="1" maxOccurs="unbounded"/>
          </xs:sequence>
        </xs:complexType>
      </xs:element>
     <xs:element name="adresse">
       <xs:complexType>
         <xs:sequence>
           <xs:element ref="anrede" minOccurs="1"/>
           <xs:element ref="name" minOccurs="1"/>
           <xs:element ref="strasse" minOccurs="1"/>
           <xs:element ref="postanschrift" minOccurs="1"/>
           <xs:element ref="email" minOccurs="0"/>
           <xs:element ref="bild" minOccurs="1"/>
         </xs:sequence>
       </xs:complexType>
     </xs:element>
     <xs:element name="anrede" type="xs:string"/>
     <xs:element name="name" type="xs:string"/>
     <xs:element name="strasse" type="xs:string"/>
     <xs:element name="postanschrift" type="xs:string"/>
     <xs:element name="email" type="xs:string"/>
     <xs:element name="bild" type="xs:string"/>
   </xs:schema>


Das fertige XML-Dokument
========================

.. code-block:: xml

   <?xml version="1.0" encoding="UTF-8"?>
   <adressdb xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
             xsi:noNamespaceSchemaLocation="art03.xsd">
     <adresse>
       <anrede />
       <name />
       <strasse />
       <postanschrift />
       <email/>
       <bild/>
     </adresse>
   </adressdb>

