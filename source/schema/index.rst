.. meta::

   :description lang=de: XML-Kurs mit den Themen: DTD, Schema, XSL, XPath
   :keywords: XML, Schema, OER

:ref:`« Kursstart <xml-kurs-start>`

.. _xml-schema-start:

===
XSD
===

.. image:: ./images/mosaik001-potsdam.jpg
   :width: 0px


.. |a| raw:: html

   <!-- lightbox container hidden with CSS -->
   <a href="#" class="lightbox" id="img1">
   <span style="background-image: url('../../_images/mosaik001-potsdam.jpg')"></span>
   </a>

.. |b| raw:: html

   <a href="#img1">
   <img width="250px" src="../../_images/mosaik001-potsdam.jpg">
   </a>

.. sidebar:: Mosaik

   |b|

   Potsdam Dortustraße

|a|



Mit dem Schema-Standard wird der Nachfolger für DTD definiert.
In einer Serie wird die Migration der DTD in eine Schema-Definition gezeigt.

.. toctree::
   :maxdepth: 1
   :glob:

   */*
