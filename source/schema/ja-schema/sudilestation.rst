.. meta::

   :description lang=de: XML-Kurs mit den Themen: DTD, Schema, XSL, XPath
   :keywords: XML, Schema, OER

:ref:`« Übersicht: Schema <xml-schema-start>`

.. index:: Schema; Einleitung

===========================
Schema: Was ist ein Schema?
===========================

.. image:: ../images/mosaik002-potsdam.jpg
   :width: 0px


.. |a| raw:: html

   <!-- lightbox container hidden with CSS -->
   <a href="#" class="lightbox" id="img1">
   <span style="background-image: url('../../_images/mosaik002-potsdam.jpg')"></span>
   </a>

.. |b| raw:: html

   <a href="#img1">
   <img width="250px"
	alt="Mosaik Potsdam"
        src="../../_images/mosaik002-potsdam.jpg">
   </a>

.. sidebar:: Mosaik

   |b|

   Potsdam Dortustraße

|a|

Lernziel
========

Sie lernen nun, warum Schema eine Alternative zu DTDs sind.

Handlungsanweisungen
====================

:Aufgaben: Informieren Sie sich im Internet!

  1. Was sind Schemata?
  2. Wer entwickelt diesen Nachfolger der DTD?

Worum geht es?
==============

Die Nachteile von DTDs haben wir schon kennen gelernt.
DTDs sind mit ihren vier Datentypen zum vollständige Beschreibungen
von Daten nur eingeschränkt geeeignet.

Zudem ist es recht ungeschickt, dass die DTDs eine eigene Syntax
haben. Mit Schemata können Sie flexibler und genauer arbeiten. So lassen sich
z.B. Postleitzahlen genau definieren z.B mit folgenden Regeln:
nur Zahlen und davon maximal fünf.

In einer DTD kann das nicht definiert werden!

DTDs und Schema
===============

Es ist gut, beides zu beherrschen, da DTDs weit verbreitet sind und
man diese Syntax auch lesen können muss. Zudem kommt es auch auf das
Einsatzgebiet an, in dem Standards entwickelt werden und wie streng die
Struktur der zu erfassenden Daten validiert werden muss.
