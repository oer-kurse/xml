.. meta::

   :description lang=de: XML-Kurs mit den Themen: DTD, Schema, XSL, XPath
   :keywords: XML, Schema, Wiederholungen, OER

:ref:`« Übersicht: Schema <xml-schema-start>`

.. index:: Schema; Wiederholungen

============
Wiederholung
============

.. image:: ../images/mosaik008-potsdam.jpg
   :width: 0px


.. |a| raw:: html

   <!-- lightbox container hidden with CSS -->
   <a href="#" class="lightbox" id="img1">
   <span style="background-image: url('../../_images/mosaik008-potsdam.jpg')"></span>
   </a>

.. |b| raw:: html

   <a href="#img1">
   <img width="250px"
	alt="Mosaik Potsdam"
        src="../../_images/mosaik008-potsdam.jpg">
   </a>

.. sidebar:: Mosaik

   |b|

   Potsdam Dortustraße

|a|

Lernziel
========

Die wiederholte Eingabe eines Kindelementes wird definiert.

Handlungsanweisungen
====================

:Aufgaben:

  1. Erweitern Sie das Schema aus der vorherigen Station
  2. Fügen Sie ein zweites Element *adresse* ein.

Die DTD als Vorlage
===================

.. code-block:: xml
   :linenos:

   <?xml version="1.0" encoding="UTF-8"?>
   <!ELEMENT adressdb (adresse+)>
   <!ENTITY % sichtbarkeit "einsicht (privat|public|proteced) #REQUIRED">
   <!ELEMENT adresse (anrede, name, strasse, postanschrift, email?, bild)>
   <!ELEMENT anrede (#PCDATA)>
   <!ELEMENT name (nachname, vorname)>
   <!ELEMENT postanschrift (plz, wohnort)>
   <!ELEMENT nachname (#PCDATA)>
   <!ELEMENT vorname (#PCDATA)>
   <!ELEMENT plz (#PCDATA)>
   <!ELEMENT wohnort (#PCDATA)>
   <!ELEMENT strasse (#PCDATA) >
   <!ELEMENT email (#PCDATA)>
   <!ATTLIST strasse %sichtbarkeit;>
   <!ATTLIST postanschrift land
             (Deutschland | Schweiz | Oesterreich) #REQUIRED %sichtbarkeit; >
   <!ATTLIST adresse nr CDATA #REQUIRED>
   <!ELEMENT bild EMPTY>
   <!ATTLIST bild bezeichnung CDATA #REQUIRED>
   <!ENTITY email_default "keine E-Mail angegeben" %sichtbarkeit; >

Kind-Element in der DTD
=======================

.. code-block:: xml

  <!ELEMENT adressdb (adresse+)>


ComplexType als Container
=========================

.. code-block:: xml
   :linenos:

   <?xml version="1.0" encoding="UTF-8"?>
   <xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema">
     <xs:element name="adressdb">
       <xs:complexType>
         <xs:sequence>
           <xs:element ref="adresse" maxOccurs="unbounded"/>
         </xs:sequence>
       </xs:complexType>
     </xs:element>
     <xs:element name="adresse"/>
   </xs:schema>

Mit dem neuen Konstrukt werden gleich mehrere Dinge gesteuert:

- Die Kombination complexType und sequence erlauben das Zusammenfügen
  unterschiedlichster Elemente.
- Mit ref="adresse" referenzieren wir das adress-Element und damit
  wird addressdb wieder zum Root-Element
- mit *maxOccurs* wird die Entsprechung des Wiederholungzeichen (+)
  in einer DTD umgesetzt. Es ist nun definiert, dass beliebig viele
  Elemente *adressen* in der *adressdb* untergebracht werden können.

Das fertige XML-Dokument
========================

.. code-block:: xml

   <?xml version="1.0" encoding="UTF-8"?>
   <adressdb xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
             xsi:noNamespaceSchemaLocation="art03.xsd">
     <adresse />
     <adresse />
   </adressdb>
