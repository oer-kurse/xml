.. meta::

   :description lang=de: XML-Kurs mit den Themen: DTD, Schema, XSL, XPath
   :keywords: XML, Schema, Root-Element, OER

:ref:`« Übersicht: Schema <xml-schema-start>`

.. index:: Schema; Root-Element

============
Root-Element
============

.. image:: ../images/mosaik006-potsdam.jpg
   :width: 0px


.. |a| raw:: html

   <!-- lightbox container hidden with CSS -->
   <a href="#" class="lightbox" id="img1">
   <span style="background-image: url('../../_images/mosaik006-potsdam.jpg')"></span>
   </a>

.. |b| raw:: html

   <a href="#img1">
   <img width="250px"
	alt="Mosaik Potsdam"
        src="../../_images/mosaik006-potsdam.jpg">
   </a>

.. sidebar:: Mosaik

   |b|

   Potsdam Dortustraße

|a|


Lernziel
========

In der folgenden Übungsserie wird in aufeinanderfolgenden Schritten eine
Schemadatei für das Adress-XML-Beispiel entwickelt. Sie haben damit den
direkten Vergleich zur schon vorhandenen DTD.

Handlungsanweisungen
====================

:Aufgaben:

  1. Erstellen Sie das Grundgerüst mit dem Wurzelelement.
  2. Erzeugen Sie eine xml-Datei basierend auf der neuen Schema-Datei.

Die DTD als Vorlage
===================

.. code-block:: xml
   :linenos:

   <?xml version="1.0" encoding="UTF-8"?>
   <!ELEMENT adressdb (adresse+)>
   <!ENTITY % sichtbarkeit "einsicht (privat|public|proteced) #REQUIRED">
   <!ELEMENT adresse (anrede, name, strasse, postanschrift, email?, bild)>
   <!ELEMENT anrede (#PCDATA)>
   <!ELEMENT name (nachname, vorname)>
   <!ELEMENT postanschrift (plz, wohnort)>
   <!ELEMENT nachname (#PCDATA)>
   <!ELEMENT vorname (#PCDATA)>
   <!ELEMENT plz (#PCDATA)>
   <!ELEMENT wohnort (#PCDATA)>
   <!ELEMENT strasse (#PCDATA) >
   <!ELEMENT email (#PCDATA)>
   <!ATTLIST strasse %sichtbarkeit;>
   <!ATTLIST postanschrift land
             (Deutschland | Schweiz | Oesterreich) #REQUIRED %sichtbarkeit; >
   <!ATTLIST adresse nr CDATA #REQUIRED>
   <!ELEMENT bild EMPTY>
   <!ATTLIST bild bezeichnung CDATA #REQUIRED>
   <!ENTITY email_default "keine E-Mail angegeben" %sichtbarkeit; >

Das Root-Element in der DTD
===========================

.. code-block:: xml

  <!ELEMENT adressdb (adresse+)>

Das Root-Element in einem Schema
================================

.. code-block:: xml

   <?xml version="1.0" encoding="UTF-8"?>
   <xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema">
     <xs:element name="adressdb">
       <xs:annotation>
         <xs:documentation>
            Comment describing your root element
         </xs:documentation>
       </xs:annotation>
     </xs:element>
   </xs:schema>

Kommentare
==========

Kommentare werden wie im XML Standard verwendet, für die Dokumentation
haben Sie zusätzlich die Möglichkeit die Elemente *xs:annotation* und
*xs:documentation* zu verwenden.

Elemente definieren
===================

Die Elementdefinition selbst erfolgt mit:

.. code-block:: xml

   <xs:element name="adressdb">
   ...
   </xs:element>




Das fertige XML-Dokument
========================
Wenn Sie nun mit der XSD-Datei ein neues XML-Dokument anlegen, können Sie den folgenden 
Inhalt erzeugen (achten Sie auf den Namen der XSD-Datei *art01.xsd*):

.. code-block:: xml

   <?xml version="1.0" encoding="UTF-8"?>
   <adressdb xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
             xsi:noNamespaceSchemaLocation="art01.xsd">
   </adressdb>


