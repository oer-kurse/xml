.. meta::

   :description lang=de: XML-Kurs mit den Themen: DTD, Schema, XSL, XPath
   :keywords: XML, Schema, Datentypen, OER

:ref:`« Übersicht: Schema <xml-schema-start>`

.. index:: Schema; eingebaute Datentypen, Datentypen (Schema)

================
Erste Definition
================

.. image:: ../images/mosaik003-potsdam.jpg
   :width: 0px


.. |a| raw:: html

   <!-- lightbox container hidden with CSS -->
   <a href="#" class="lightbox" id="img1">
   <span style="background-image: url('../../_images/mosaik003-potsdam.jpg')"></span>
   </a>

.. |b| raw:: html

   <a href="#img1">
   <img width="250px"
	alt="Mosaik Potsdam"
        src="../../_images/mosaik003-potsdam.jpg">
   </a>

.. sidebar:: Mosaik

   |b|

   Potsdam Dortustraße

|a|

Lernziel
=========

Entwickeln Sie nun Ihre erste Schema-Definition.

Handlungsanweisungen
====================

:Aufgaben:

   1. Entwickeln Sie bitte für folgende XML-Datei eine Schema-Datei:

      ::
         <?xml version="1.0" encoding="ISO-8859-1"?>
           <reise>
             <veranstalter>Fahr-Away</veranstalter>
             <ziel>Japan</ziel>
             <beginn>2007-02-09</beginn>
             <ende>2007-02-20</ende>
             <preis>1020.99</preis>
           </reise>

   2. Testen Sie die XML-Datei gegen die Schema-Datei. Ist sie
      valide? Prüfen Sie die Schema-Datei durch bewusste
      Falsch-Eingaben (z.B. als Preis eine Zeichenkette
      eingeben).


Die Schema-Datei
~~~~~~~~~~~~~~~~
Die Dateiendung für Schema-Dateien ist: xsd

.. code-block:: xml
   :linenos:


   <?xml version="1.0" encoding="ISO-8859-1"?>
   <xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema">
     <xs:element name="artikel">
       <xs:complexType>
          <xs:sequence>
            <xs:element name="titel" type="xs:string"/>
            <xs:element name="autor" type="xs:string"/>
            <xs:element name="inhalt" type="xs:string"/>
          </xs:sequence>
       </xs:complexType>
     </xs:element>
   </xs:schema>

Artikel ist ein komplexer Typ -- *xs:complexType*.
Dieser enthält eine Sequenz möglicher Kindelemente (Zeilen 6-8).
Entscheidend für die Kindelemente ist der verwendete Datentyp.
Es sind fünf vordefinierte Standard-Datentypen vorhanden:

.. index: XSD: einfache Datentypen, xs:string, xs:integer, xs:decimal
.. Index: xs:boolean, xs:date

+------------+-------------------------------------------------------+
| Datentyp   |	Anmerkung                                            |
+============+=======================================================+
| xs:string  | Für normale Zeichenketten, also Texte                 |
+------------+-------------------------------------------------------+
| xs:integer | Für Ganzzahlen wie 1234, keine Fließkommazahlen.      |
+------------+-------------------------------------------------------+
| xs:decimal | Datentyp für Dezimalzahlen, also Fließkommazahlen,    |
|            | wie 0.123                                             |
+------------+-------------------------------------------------------+
| xs:boolean | Ein Boolscher Wert, d.h. es gibt nur "wahr" oder      |
|            | "falsch" bzw. "0" oder "1".                           |
+------------+-------------------------------------------------------+
| xs:date    | Ein Datum im Format Jahr-Monat-Tag                    |
+------------+-------------------------------------------------------+

Die XML-Datei
~~~~~~~~~~~~~
.. code-block:: xml
   :linenos:

   <?xml version="1.0" encoding="ISO-8859-1"?>
   <artikel xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
            xsi:noNamespaceSchemaLocation="artikel.xsd">
     <titel>Der Titel</titel>
     <autor>Heinz Becker</autor>
     <inhalt>Hier irgendein Inhalt.</inhalt>
   </artikel>

Die Schema-Datei wird über die Zeile 3 angesprochen und mit der
XML-Datei verbunden.
