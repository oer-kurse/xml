.. meta::

   :description lang=de: XML-Kurs mit den Themen: DTD, Schema, XSL, XPath
   :keywords: XML, Schema, Element, Kind-Elemente, OER

:ref:`« Übersicht: Schema <xml-schema-start>`

.. index:: Schema; Kind-Elemente

============
Kindelemente
============


.. image:: ../images/mosaik007-potsdam.jpg
   :width: 0px


.. |a| raw:: html

   <!-- lightbox container hidden with CSS -->
   <a href="#" class="lightbox" id="img1">
   <span style="background-image: url('../../_images/mosaik007-potsdam.jpg')"></span>
   </a>

.. |b| raw:: html

   <a href="#img1">
   <img width="250px"
	alt="Mosaik Potsdam"
        src="../../_images/mosaik007-potsdam.jpg">
   </a>

.. sidebar:: Mosaik

   |b|

   Potsdam Dortustraße

|a|


Lernziel
========

Ein weiteres Element wird definiert.

Handlungsanweisungen
====================

:Aufgaben:

  1. Erweitern Sie das Schema aus der vorherigen Station
  2. Fügen Sie ein zweites Element *adresse* ein.

Die DTD als Vorlage
===================

.. code-block:: xml
   :linenos:

   <?xml version="1.0" encoding="UTF-8"?>
   <!ELEMENT adressdb (adresse+)>
   <!ENTITY % sichtbarkeit "einsicht (privat|public|proteced) #REQUIRED">
   <!ELEMENT adresse (anrede, name, strasse, postanschrift, email?, bild)>
   <!ELEMENT anrede (#PCDATA)>
   <!ELEMENT name (nachname, vorname)>
   <!ELEMENT postanschrift (plz, wohnort)>
   <!ELEMENT nachname (#PCDATA)>
   <!ELEMENT vorname (#PCDATA)>
   <!ELEMENT plz (#PCDATA)>
   <!ELEMENT wohnort (#PCDATA)>
   <!ELEMENT strasse (#PCDATA) >
   <!ELEMENT email (#PCDATA)>
   <!ATTLIST strasse %sichtbarkeit;>
   <!ATTLIST postanschrift land
             (Deutschland | Schweiz | Oesterreich) #REQUIRED %sichtbarkeit; >
   <!ATTLIST adresse nr CDATA #REQUIRED>
   <!ELEMENT bild EMPTY>
   <!ATTLIST bild bezeichnung CDATA #REQUIRED>
   <!ENTITY email_default "keine E-Mail angegeben" %sichtbarkeit; >

Kind-Element in der DTD
=======================

.. code-block:: xml

  <!ELEMENT adressdb (adresse+)>

Kind-Element in einem Schema
============================
Siehe Zeile 4 ...

.. code-block:: xml
   :linenos:


   <?xml version="1.0" encoding="UTF-8"?>
   <xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema">
     <xs:element name="adressdb"></xs:element>
      <xs:element name="adresse"/>
   </xs:schema>

Das fertige XML-Dokument
========================
Wenn Sie nun mit der XSD-Datei ein neues XML-Dokument anlegen, können Sie den folgenden 
Inhalt erzeugen (achten Sie auf den Namen der XSD-Datei *art03.xsd*):

.. code-block:: xml

   <?xml version="1.0" encoding="UTF-8"?>
   <adressdb xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
             xsi:noNamespaceSchemaLocation="art03.xsd">
     <adresse />
   </adressdb>
