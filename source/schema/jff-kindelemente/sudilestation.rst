.. meta::

   :description lang=de: XML-Kurs mit den Themen: DTD, Schema, XSL, XPath
   :keywords: XML, Schema, Kindelemente, OER

:ref:`« Übersicht: Schema <xml-schema-start>`

.. index:: Schema; Kindelemente

============
Kindelemente
============


.. image:: ../images/mosaik010-potsdam.jpg
   :width: 0px


.. |a| raw:: html

   <!-- lightbox container hidden with CSS -->
   <a href="#" class="lightbox" id="img1">
   <span style="background-image: url('../../_images/mosaik010-potsdam.jpg')"></span>
   </a>

.. |b| raw:: html

   <a href="#img1">
   <img width="250px"
	alt="Mosaik Potsdam"
        src="../../_images/mosaik010-potsdam.jpg">
   </a>

.. sidebar:: Mosaik

   |b|

   Potsdam Dortustraße

|a|

Lernziel
========

Weitere Kindelemente werden hinzugefügt.

Handlungsanweisungen
====================

:Aufgaben:

  1. Erweitern Sie das Schema aus der vorherigen Station
  2. Fügen Sie ein zweites Element *anrede* ein.
  3. Wiederholen Sie die Arbeitsschritte für das Element *postanschrift*, so wie es
     für das Element *name* demonstriert ist.
   
Die DTD als Vorlage
===================
Zeilen:  6, 8, 9

.. code-block:: xml
   :linenos:

   <?xml version="1.0" encoding="UTF-8"?>
   <!ELEMENT adressdb (adresse+)>
   <!ENTITY % sichtbarkeit "einsicht (privat|public|proteced) #REQUIRED">
   <!ELEMENT adresse (anrede, name, strasse, postanschrift, email?, bild)>
   <!ELEMENT anrede (#PCDATA)>
   <!ELEMENT name (nachname, vorname)>
   <!ELEMENT postanschrift (plz, wohnort)>
   <!ELEMENT nachname (#PCDATA)>
   <!ELEMENT vorname (#PCDATA)>
   <!ELEMENT plz (#PCDATA)>
   <!ELEMENT wohnort (#PCDATA)>
   <!ELEMENT strasse (#PCDATA) >
   <!ELEMENT email (#PCDATA)>
   <!ATTLIST strasse %sichtbarkeit;>
   <!ATTLIST postanschrift land
             (Deutschland | Schweiz | Oesterreich) #REQUIRED %sichtbarkeit; >
   <!ATTLIST adresse nr CDATA #REQUIRED>
   <!ELEMENT bild EMPTY>
   <!ATTLIST bild bezeichnung CDATA #REQUIRED>
   <!ENTITY email_default "keine E-Mail angegeben" %sichtbarkeit; >

Kind-Elemente in der DTD
========================

.. code-block:: xml

   <!ELEMENT name (nachname, vorname)>
   <!ELEMENT nachname (#PCDATA)>
   <!ELEMENT vorname (#PCDATA)>


ComplexType als Container
=========================
Zeilen:  25-32, 37-38

.. code-block:: xml
   :linenos:

   <xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema">
     <xs:element name="adressdb">
       <xs:annotation>
         <xs:documentation>Meine Adressammlung</xs:documentation>
       </xs:annotation>
       <xs:complexType>
         <xs:sequence>
           <xs:element ref="adresse" minOccurs="1" maxOccurs="unbounded"/>
         </xs:sequence>
       </xs:complexType>
     </xs:element>
     <xs:element name="adresse">
       <xs:complexType>
         <xs:sequence>
           <xs:element ref="anrede" minOccurs="1"/>
           <xs:element ref="name" minOccurs="1"/>
           <xs:element ref="strasse" minOccurs="1"/>
           <xs:element ref="postanschrift" minOccurs="1"/>
           <xs:element ref="email" minOccurs="0"/>
           <xs:element ref="bild" minOccurs="1"/>
         </xs:sequence>
       </xs:complexType>
     </xs:element>
     <xs:element name="anrede" type="xs:string"/>
     <xs:element name="name">
       <xs:complexType>
         <xs:sequence>
           <xs:element ref="nachname"/>
           <xs:element ref="vorname"/>
         </xs:sequence>
       </xs:complexType>
     </xs:element>
     <xs:element name="strasse" type="xs:string"/>
     <xs:element name="postanschrift" type="xs:string"/>
     <xs:element name="email" type="xs:string"/>
     <xs:element name="bild" type="xs:string"/>
     <xs:element name="nachname" type="xs:string"/>
     <xs:element name="vorname" type="xs:string"/>
   </xs:schema>

Aus einem einfachen Datentyp kann ein complexType erzeugt werden, wie hier am Beispiel
von *name* mit den neuen Kindelementen *vorname* und *nachname* demonstriert wird.


Das fertige XML-Dokument
========================

.. code-block:: xml

   <?xml version="1.0" encoding="UTF-8"?>
   <adressdb xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
             xsi:noNamespaceSchemaLocation="art03.xsd">
     <adresse>
       <anrede/>
       <name>
         <nachname/>
         <vorname/>
       </name>
       <strasse/>
       <postanschrift/>
       <bild/>
     </adresse>
   </adressdb>

