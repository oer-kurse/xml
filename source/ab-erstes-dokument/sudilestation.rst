.. meta::

   :description lang=de: XML-Kurs mit den Themen: DTD, Schema, XSL, XPath
   :keywords: XML, Regeln, XSL, OER, DTD, Schema, XPath

:ref:`« Kursstart <xml-kurs-start>`

======================
Das erste XML Dokument
======================

.. image:: ./images/globus.jpg
   :width: 0px


.. |a| raw:: html

   <!-- lightbox container hidden with CSS -->
   <a href="#" class="lightbox" id="img1">
   <span style="background-image: url('../../_images/globus.jpg')"></span>
   </a>

.. |b| raw:: html

   <a href="#img1">
   <img width="250px" src="../../_images/globus.jpg">
   </a>

.. sidebar:: Globus

   |b|

|a|

Lernziel
========

In dieser Station erstellen Sie Ihr erstes XML-Dokument und lernen so
die Syntax von XML kennen. Außerdem wissen Sie nach der Station mehr
darüber, wie Sie einen Syntaxcheck durchführen.


Handlungsanweisungen
====================
:Aufgaben:
  1. Erstellen Sie ein XML-Dokument für eine CD-Datenbank. Enthalten
     sein sollen: Interpret, Titel, Verlag, Genre, Preis.

  2. Tragen Sie drei CDs in das XML-Dokument ein. Testen Sie, ob die
     Syntax richtig ist.

  3. Erweitern Sie das XML-Dokument um folgendes Element: Bandinformationen.
     Diese soll wiederum zwei Informationen enthalten: Gründungsjahr
     und Anzahl der Bandmitglieder.

  4. Eine neue CD wird in die Datenbank aufgenommen: Die "Österreicher
     Schürzenjäger" mit dem Song

     "Schöne Küchenmaid" aus dem Verlag "A Stückl Heimat", Genre
     "HipHop". Das gute Stück soll 20 € kosten.

     Was ist zu beachten?
  5. Optional: Entwickeln Sie ein XML-Dokument für einen
     Buchbestand. Welche Elemente könnten

     für ein Buch mit in die Datenbank hineingenommen werden. Füllen
     Sie die Datenbank dann mit drei Büchern.

  6. Fehler im Quellcode! Finden und beheben Sie die Fehler!

     ::

       <?xml version="1.0" encoding="UTF-8" ?>
       <adresse>
        <name>
         <nachname>Schulze</nachname
         <vorname>Herbert</vorname>
        <strasse>Rundweg 10</strasse>
        <postanschrift>
         <plz>12345</plz>
         <wohnort>Stadthäuschen/<wohnort>
        <postanschrift>
        <email >herbert@schulze.de</email>
       </adresse>

Der Aufbau eines XML-Dokuments
==============================
|

.. image:: img/prolog.png

Ein XML-Dokument besteht aus einem sogenannten Prolog und dem XML-Inhalt.


Ein Adressenbeispiel
====================

Ziel ist es, eine kleine Adressdatenbank zu bauen. Also die Adressen in einer
XML-Datei zu speichern. Die Struktur einer Adresse könnte so aussehen:

.. image:: img/brief.png


Adressen und XML
Ein möglicher Code für die Adressen wäre:
::

  <?xml version="1.0" encoding="UTF-8"?>
  <adresse>
    <!-- Jetzt kommt der Name-->
    <name>
      <nachname>Schulze</nachname>
      <vorname>Herbert</vorname>
    </name>
    <!-- Jetzt kommt die Strasse-->
    <strasse>Rundweg 10</strasse>
    <postanschrift>
    <plz>12345</plz>
    <wohnort>Stadthausen</wohnort>
    </postanschrift>
    <email>herbert@schulze.de</email>
  </adresse>

Zur Erklärung:
::

  <?xml version="1.0" encoding="UTF-8" ?>

Der Prolog definiert hier in seiner einfachsten Form,
welche XML-Version verwendet wird und welcher
Zeichensatz für dieses Dokument gilt.
::

  <adresse> ... </adresse>

Das Wurzelelement umschließt alle anderen Elemente.
::

  <!-- Kommentar -->

Wird von jeder Art Parser ignoriert...

Adressbuch
==========

Gut, wir haben jetzt eine Adresse in eine XML-Form gebracht.
Wie sieht es mit mehreren Adressen aus?

Sie sehen jetzt das Element *adressensammlung*, das mehrere Adressen
einschließt.
So können Sie beliebig viele Adressen in *adressensammlung* speichern.
*adressensammlung* ist nun das Rootelement.

::

  <?xml version="1.0" encoding="UTF-8"?>
  <adressensammlung>
  <adresse>
    <!-- Jetzt kommt der Name-->
    <name>
      <nachname>Schulze</nachname>
      <vorname>Herbert</vorname>
    </name>
    <!-- Jetzt kommt die Strasse-->
    <strasse>Rundweg 10</strasse>
    <postanschrift>
    <plz>12345</plz>
    <wohnort>Stadthausen</wohnort>
    </postanschrift>
    <email>herbert@schulze.de</email>
  </adresse>
  <adresse>
  ...
  </adresse>
  </adressensammlung>

Allgemeine Regeln (FAQ)
=======================

:Frage: Wie werden die XML-Dokumente gespeichert?
:Antwort:  Mit der Endung .xml!

:Frage: Wird Groß-/Kleinschreibung unterschieden?
:Antwort: Ja.

:Frage: Muss jedes Element geschlossen werden?
:Antwort:  Ja (Leere Elemente haben eine besondere Syntax).

Beipiel:
::

  <start>Inhalt</start>.

::

  <name />

Für Elemente ohne Inhalt gilt die verkürzte Schreibweise:
Attributwerte sind in Anführungszeichen zu setzen.


Verschachteln von Elementen
===========================

Wie verschachtelte ich Elemente?
::

   FALSCH:

   <start><ende>Inhalt</start></ende>

   RICHTIG:

   <start><ende>Inhalt</ende></start>

Was sind wohlgeformte XML-Dokumente?

Werden diese Regeln eingehalten, nennt man das Dokument wohlgeformt
(well formed).

Was bedeutet nun Encoding?
==========================

Schauen wir uns die folgende Zeile etwas genauer an:
::

  <?xml version="1.0" encoding="UTF-8" ?>


Mit dem Attribut encoding legen wir den Zeichensatz für das XML-Dokument fest.
"UTF-8" bedeutet, dass ein Zeichensatz verwendet wird, der alle Zeichen aller
Sprachen enthält (ca. 65000). Deutsche Umlaute wie "ö" oder "ü" werden in
"UTF-8" besonders kodiert.

Nehmen wir also wieder unseren Jörg Schütze.
XML-Codiert müsste sein Name nun so aussehen:

J&#246;rg Sch&#252;tze

Einige andere Kodierungen:
::

  Ä = &#196;
  Ö = &#214;
  Ü = &#220;
  ä = &#228;
  ö = &#246;
  ü = &#252;
  ß= &#223;
  € = &#8364;
  [Leerzeichen] = &#160;

Eine andere Möglichkeit besteht darin, einen anderen Zeichensatz zu
wählen: "ISO-8859-1".
In diesem sind die Sonderzeichen westeuropäischer Sprachen enthalten,
die nicht kodiert werden müssen Beachte, dieser Zeichensatz steht
nicht auf allen Rechnern zur Verfügung, dort erscheint dann ein
Fragezeichen, wenn das Zeichen nicht im Zeichenvorrat der Schriftarten
vorhanden ist.

::

  <?xml version="1.0" encoding="ISO-8859-1" ?>

.. note:: Der einfachste Weg:

   Speichern Sie das XML-Document mit der Codierung UTF-8. Dann können
   Sie auch normale Umlaute benutzen, ohne sie kodieren zu müssen.

   Wenn Sie den XML-Spy als Editor nutzen, ist das Encoding für UTF-8
   schon eingestellt.

Syntax-Check
============

Wie prüfe ich nun die Syntax des Dokuments?

1. Browser der aktuellen Generation können XML-Dokumente darstellen.
   Sie müssen das XML-Dokument mit dem Browser öffnen. Es wird dann der XML-Baum
   dargestellt, weil es noch keine Transformationsanweisungen gibt.

2. Testen Sie zunächst nur ob die XML-Instanz *wohlgeformt*
   ist. Gültig werden die Dokumente erst mit einer korrekten DTD, die
   Sie im nächsten Kapitel kennenlernen werden, bzw. einem Schema, das
   zum Kursende behandelt wird. Nachfolgend die Icons aus XML-Spy:

   .. image:: img/pruefung.png

3. Alternativ kann ein online verfügbarer XML-Validator verwendet
   werden. z.B. http://www.utilities-online.info/xsdvalidation

4. Ein XML-Dokument im Browser geöffnet zeigt ebenfalls Fehler an,
   wenn das Dokument nicht wohlgeformt ist.
