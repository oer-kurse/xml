.. meta::

   :description lang=de: Invisible XML, implizite Strukturen in explizites XML überführen
   :keywords: XML, ixml, Struktur, konvert 



===============
 Invisible XML
===============

.. INDEX:: Invisible XML; ixml
.. INDEX:: ixml; Invisible XML


Sie haben Daten die strukturiert sind und wollen es in eine XML-Struktur wandeln.
Damit wird aus einer Beschreibung mit ixml eine sichtbare, strukturierte Information.

Im Tutorial wird gezeigt, wie aus:

::

   4 November 2021

dieses XML wird:

::

   <date>
     <day>
       <digit>4</digit>
     </day> 
     <month>November</month> 
     <year>
       <digit>2</digit>
       <digit>0</digit>
       <digit>2</digit>
       <digit>1</digit>
     </year>
   </date>

oder dieses:

::

   <date>
     <day>4</day> 
     <month>November</month> 
     <year>2021</year>
   </date>

oder auch dieses:

::

   <date day='4' month='November' year='2021'/>

 Weiterführende Links
======================

- Blogartikel: https://www.xml.com/articles/2022/03/01/invisible-xml/
- Tutorial: https://homepages.cwi.nl/~steven/ixml/tutorial/tutorial.xhtml
- Testinstanz: http://45.83.242.201/ixml/tutorial/run.html
  
