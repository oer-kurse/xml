# XML

* Kurs-Material für einen XML-Kurs
* Mit vielen Übungen zu den Themen: XPATH, DTD, Schema und XSL

Konzipiert als Kurs und Nachschlagewerk.

Die aktuelle Build-Version ist direkt abrufbar unter:

https://oer-kurse.gitlab.io/xml/

Peter Koppatz (2022)
